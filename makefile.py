import csv
import math
from pathlib import Path

s = source('autopill')

pilldata = {}
with Path(__file__).with_name('pilldata.csv').open('r', newline='') as fp:
    rdr = csv.reader(fp)
    next(rdr) # skip header
    for row in rdr:
        if len(row) >= 6:
            name, enable, length, width, thick, pincher = row[:6]
            if enable.lower() == 'y':
                pilldata[name] = (float(width), float(thick), float(length), pincher.lower() == 'y')

s.part('complete', stl=False)

s.part('gear-animate', stl=False)

# keep as part but don't generate STLs
s.part('lower-hopper-base-test', stl=False)

pill_widths = sorted({d[0] for d in pilldata.values()})

common_parts = [
    s.part('lower-hopper-base', out_xform='rz(-90)', stl=False),
    s.part('lower-hopper-base-shim', stl=False),
    s.part('lower-hopper-lid', out_xform='rz(90)', stl=False),
    s.part('dispenser-tube-lower', out_xform='ry(pincher ? -90 : 0)', stl=False),
    s.part('dispenser-tube-upper', out_xform='ry(pincher ? -90 : 180)', stl=False),
]

full_parts = common_parts + [
    s.part('dispenser-slider-upper', out_xform='ry(180)', stl=False)
]

full_parts_pincher = common_parts + [
    s.part('pincher', out_xform='rx(90)', stl=False)
]

for pill, data in pilldata.items():
    pill_width, pill_thick, pill_len, pincher = pilldata[pill]
    parts = full_parts_pincher if pincher else full_parts
    for part in parts:
        part.subpart(
            pill,
            pill_width=pill_width,
            pill_thick=pill_thick,
            pill_len=pill_len,
            pincher=pincher,
            stl=f'autopill-psz-{pill}-{part.name}.stl'
        )

s.part('beltwheel')
s.part('beltwheel-ext', mod='beltwheel', args='beltwheel_offset')

pwgl = s.part('pillwheel-gear-l', out_xform='rx(planetgear_hgr ? 180 : 0)', stl=False)
pwgr = s.part('pillwheel-gear-r', out_xform='rx(planetgear_hgr ? 180 : 0)', stl=False)
#pwglb = s.part('pillwheel-gear-l-belt', stl=False)

for width in pill_widths:
    pwgl.subpart(f'w{width}mm', pill_width=width, pill_thick=width)
    pwgr.subpart(f'w{width}mm', pill_width=width, pill_thick=width)
    pwgl.subpart(f'old-w{width}mm', pill_width=width, pill_thick=width, pillgear_old_interface=True)
    pwgr.subpart(f'hgr-w{width}mm', pill_width=width, pill_thick=width, planetgear_hgr=True)
    pwgl.subpart(f'hgr-w{width}mm', pill_width=width, pill_thick=width, planetgear_hgr=True)
    #pwg.subpart(f'w{width}mm', pill_width=width, pill_thick=width)
    #pwgr.subpart(f'hgr-w{width}mm', pill_width=width, pill_thick=width, planetgear_hgr=True)
    #pwglb.subpart(f'w{width}mm', pill_width=width, pill_thick=width)


pg = s.part('planetgear', stl=False)
pgs = s.part('planetgear-sun')
pgp = s.part('planetgear-planet')
pgr = s.part('planetgear-ring')

pgsh = pgs.subpart('hgr', planetgear_hgr=True)
pgph = pgp.subpart('hgr', planetgear_hgr=True)
pgrh = pgr.subpart('hgr', planetgear_hgr=True)

pg_parts = [
    (pgs, None),
    (pgr, None),
]

pg_hgr_parts = [
    (pgsh, None),
    (pgrh, None),
]

for i in range(5):
    ang = i * 2 * math.pi / 5
    sv = math.sin(ang)
    cv = math.cos(ang)
    pg_parts.append((pgp, [20 * cv, 20 * sv, 0]))
    pg_hgr_parts.append((pgph, [18 * cv, 18 * sv, 0]))

mergepart('autopill-planetgear.stl', pg_parts)
mergepart('autopill-planetgear-hgr.stl', pg_hgr_parts)

#pg.subpart('hgr', planetgear_hgr=True, stl=False)

s.part('planetgear-carrier', planetgear_hgr=True)
s.part('planetgear-shim')
s.part('planetgear-beltwheel')

s.part('pillwheel', out_xform='rx(180)')
s.part('pillwheel-holder')
s.part('motorbeltwheel')

s.part('pillwheel-flex-cover')

s.part('pillwheel-poker', out_xform='rx(180)')
s.part('pillwheel-poker-cam-follower')
s.part('pillwheel-insert')

s.part('pillwheel-gear-spacer', out_xform='rx(180)')

s.part('disptopmount', preview_xform='ty(20) tz(3) rx(-preview_wheel_rot) ty(-20) tz(-3)')
s.part('disptopmount-pg-l')
s.part('disptopmount-pg-r')

p = s.part('dispenser-wirebox')
p.subpart('vc', dispenser_connector_vertical=True)

p = s.part('dispenser')
p.subpart('pincher', pincher=1)

p = s.part('dispenser-slider')
p.subpart('pincher', pincher=1)

s.part('dispenser-pcb-spacer-front', out_xform='rx(180)')
s.part('dispenser-pcb-spacer-rear')

s.part('dispenser-button', out_xform='rx(180)')

s.part('led-holder', out_xform='rx(-90)')
s.part('pincher-slider', pincher=1)
s.part('servo-arm')

r = s.part('riser', args='riser_height * module_rise', stl=False)
rdr = s.part('riser-dbl-r', args='riser_height', stl=False)
rdl = s.part('riser-dbl-l', args='riser_height', stl=False)
for n in range(1, 4):
    for p in (r, rdr, rdl):
        p.subpart(f'{n}', riser_height=n)

rdr.subpart('inner', riser_height=0)
rdl.subpart('inner', riser_height=0)

s.part('stackable-spacer')

s.part('basemodule-r')
s.part('basemodule-l')
p = s.part('slide-insert', mod='slide-insert')
for mult in [2, 3, 4]:
    p.subpart(f'x{mult}', slide_insert_length=mult)

s.part('idc-holder-shim', out_xform='rx(180)' )

s.part('pilltray')
s.part('pilltray-holder')
s.part('pilltray-holder-top')
s.part('pilltray-holder-top-aligntool-1')
s.part('pilltray-holder-top-aligntool-2')
s.part('pilltray-holder-rear-slide', out_xform='pilltray_holder_rear_slide_xform()')

s.part('hopper', out_xform='ry(180)')
s.part('hopper-lid', out_xform='ry(180)')
s.part('spiral-l', mod='wheel_spiral')
s.part('spiral-r', mod='wheel_spiral', out_xform='sx(-1)')
s.part('pcb-model', args='traces=preview_pcb_traces')
s.part('pcb-model-main', args='traces=preview_pcb_traces')
s.part('pcb-model-led', args='traces=preview_pcb_traces')
s.part('mainbox')

s.part('mainbox-pcb-spacer')
s.part('mainbox-lid', out_xform='rx(180)')
s.part('mainbox-lid-inlay-front', out_xform='rx(180)')
s.part('mainbox-lid-inlay-back', out_xform='rx(180)')

s.part('pi-mount', preview_xform='rz(-90)')
s.part('pi-mount-lid', out_xform='rx(180)')
s.part('pi-mount-lid-inlay-led', out_xform='rx(180)')
s.part('pi-mount-lid-inlay-front', out_xform='rx(180)')
s.part('pi-mount-lid-inlay-back', out_xform='rx(180)')

s.part('pi-button-dpad', out_xform='rx(180)')
s.part('pi-button-dpad-inlay', out_xform='rx(180)')

s.part('button-pcb-cover', out_xform='rx(180)')

s.part('display-spacer', out_xform='rx(180)')

s.part('interface-spacer')
s.part('motor-pin-spacer', mod='interface-spacer', args='n=4, r=1, hh=1.2')

s.part('md-spacer')
s.part('md-solder-jig')
s.part('wire-holder-r')
s.part('wire-holder-l')

s.automain()

s = source('motortool')
s.part('main', default=True)
