include <common.scad>

$fn = $preview ? 30 : 80;

bearing_thick = 6;
bearing_outer = 24;
bearing_inner = 12;

motormount_l = 23;
motormount_w = 26;

wheel_rad = 25;

motor_x = wheel_rad + 4;

module motormount_base(motorh=15, r1=0, r2=0, r3=0, r4=0) {
    difference() {
        cubex(
            x1=0, x2=motormount_l,
            yc=0, ys=motormount_w,
            z1=0, z2=motorh,
            r1=r1, r2=r2, r3=r3, r4=r4
        );
        tz(-eps)
        tx(12) {
            intersection() {
                cylinder(h=motorh-2, d=20);
                cubex(
                    xc=0, xs=15,
                    yc=0, ys=20,
                    z1=0, z2=motorh-2
                );
            }
            cylx(z1=motorh-2-eps2, z2=motorh+eps2, d=8.6);
        }
        cubex(
            x1=12, x2=24,
            yc=0, ys=4,
            z1=-eps, z2=motorh-2
        );
    }
}


difference() {
    union() {
        cylx(-1, bearing_thick, d=bearing_outer + 12);
        cubex(x1=0, x2=motor_x, z1=-1, z2=3, yc=0, ys=bearing_outer + 12);
    }
    cylx(-eps, bearing_thick + eps, d=bearing_outer);
    cylx(z1=-1-eps, z2=eps, d=bearing_inner + 5, ext=eps);
}

tx(motor_x)
tz(15-1)
sz(-1)
motormount_base();
