module mainbox_mount() {
    rfx() tx(30) ty([5, 20]) {
        rz(30) {
            countersink_m2(10, expand=1, nut_length=threaded_inserts ? 0 : 2.5);
            if (threaded_inserts) {
                cylx(0, 4, d=insert_dia_m2_5);
            }
        }
    }
}


module mainbox(screws=true) {
    wall_thick = 4;
    color("green") cube(1, center=true);
    difference() {
        union() {
            //if (!preview_cutout)
            wirebox_base(mainbox_h, 0, 0, mainbox_w, mainbox_l, ti_height=4.0);

            for (pt = mount_holes_m2) translate(pt) {
                cylx(0, 3, d=insert_dia_m2_5 + 2 * 1.8);
            }
            *for (pt = mount_holes_m3) translate(pt) {
                cylx(0, 3, d=insert_dia_m3 + 2 * 1.8);
            }
        }

        for (pt = mount_holes_m2) translate(pt) {
            if (threaded_inserts) {
                cylx(3 - 6.0, 3+eps, d=insert_dia_m2_5);
            } else {
                cylx(-10, 5, d=screw_diameter_m2+.3);
                tz(-4-eps)
                hexnut_hole(h=3, d=nut_diameter_m2);
            }
        }
        for (pt = mount_holes_m3) translate(pt) {
            if (threaded_inserts) {
                cylx(3 - 6.0, 3+eps, d=insert_dia_m3);
            } else {
                cylx(-10, 5, d=screw_diameter_m3+.3);
                tz(-4-eps)
                hexnut_hole(h=3, d=nut_diameter_m3);
            }
        }

        // power plug
        cubex(y1=mainbox_l - eps, y2=mainbox_l + 4 + eps, xc=main_pcb_pos.x + autopill_main_pos_PWR1.x, xs=9.6, z1=pcb_top_z - 1.6 - 11.6, z2=mainbox_h+eps);

        // space for original power plug on mega
        //cubex(x1=-2-eps, x2=eps, yc=main_pcb_pos.y + autopill_main_pos_J13.y, ys=9.6, z1=pcb_top_z - 1.6 - 11.2, z2=mainbox_h+eps);

        // usb
        cubex(x1=-4-eps, x2=eps, yc=mega_pos.y + 38.1, ys=15, z1=mega_mount_z-1, z2=mainbox_h+eps);

        // pi power
        xzy()
        cubex(z1=-4-eps, zs=4+eps2, xc=mainbox_w - 30, xs=6, y1=mainbox_h - 6, y2=mainbox_h+eps, r2=3, r1=3);

        // main mount holes
        if (screws)
        tx(mainbox_xcenter) xzy() sz(-1) mainbox_mount();

        // riser mounts
        rfx(mainbox_w/2)
        translate([-4, pilltray_holder_x1 + dispbase_row_spacing + 26, -4])
        rz(90) riser_interface_out_inner([module_interface_center]);

        tx(mainbox_w - 32) ty(mainbox_l + 4) tz(-4) {
            riser_interface_out_inner([0]);
        }

    }
    if (!threaded_inserts) {
        for (pt = [each mount_holes_m3, each mount_holes_m2]) translate(pt) {
            tz(-4+3) bridge_support(8, h=0.3);
        }
    }

}

module preview_mainbox() {
    tz(0) {

        if (preview_pcb)
        translate(main_pcb_pos)
        tz(pcb_top_z - 1.6) {
            pcb_model_main();
            *tz(1.6) mainbox_pcb_spacer();

        }

        translate(mega_pos)
        tz(mega_mount_z)

        color("green", 0.5)
        translate([50.796, 26.67, 0.8])
        import("arduino-mega.stl");

        color("green", 0.5)
        translate(converter_6v_pos)
        tz(0) cube(converter_6v_size);

        color("red", 0.5)
        translate(converter_5v_pos)
        tz(0) cube(converter_5v_size);

        if (preview_lid)
        tz(mainbox_h) {
            preview_mainbox_lid(preview_pcb = false);

            color("#333333")
            render(10)
            mainbox_lid();
        }
    }
}

module mainbox_pcb_spacer() {
    difference() {
        union() {
            color("aqua")
            hull()
            for (pt = mainbox_ports)  translate(pt + 2.54 * [-.5, 2]) {
                cubex(
                    xc = 0, xs = 10,
                    yc = 0, ys = 20,
                    z1=0, z2=.8

                );
            }

            for (pt = mainbox_ports)  translate(pt + 2.54 * [-.5, 2]) {
                color("#363636")
                cubex(
                    xc = 0, xs = 6.4,
                    yc = 0, ys = 19,
                    z1=0, z2=2.4
                );
            }
        }
        for (pt = mainbox_ports)  translate(pt + 2.54 * [-.5, 2]) {
            color("hotpink")
            cubex(
                xc = 0, xs = 2.54 * 2 + .2,
                yc = 0, ys = 2.54 * 5 + .2,
                z1=-eps, z2=3
            );
        }

        tz(-1.6)
        iter_objects(autopill_main_objects) {
            if ($cpackage == "smd_r") {
                cubex(
                    xc = 0, xs = 4.6,
                    yc = 0, ys = 2.5,
                    z1 = -10, z2 = 3
                );

            }
            if ($ref == "PWR1") {
                sz(-1)
                tz(-1.6)
                cubex(
                    x1 = -10, x2 = 2.5,
                    yc = 0, ys = 6,
                    z1=-10, z2=10
                );
            }
        }
    }
}

module mainbox_lid() {
    wall_thick = 4;
    pcb_z = -mainbox_h + pcb_top_z;
    difference() {
        union() {
            if(!preview_cutout)
            color("#333333")
            wirebox_lid_base(
                x1=-wall_thick, x2=mainbox_w+wall_thick,
                y1=-wall_thick, y2=mainbox_l+wall_thick, lt=preview_cutout ? .5 : 4
            );

            *hull()
            translate(main_pcb_pos)
            for (pt = mainbox_ports)  translate(pt + 2.54 * [-.5, 2]) {
                cubex(
                    xc = 0, xs = 12.5,
                    yc = 0, ys = 2.54 * 5 + 8,
                    z1=pcb_z + 2.4, z2=0
                );
            }

            translate(main_pcb_pos) {
                difference() {
                    cubex(x1=-1.6, x2=autopill_main_size.x + 2, y1=-2, y2=autopill_main_size.y + 2, z1=pcb_z - 1.6, z2=0, r=3);
                    color("red")
                    cubex(x1=-.2, x2=autopill_main_size.x + .2, y1=-.2, y2=autopill_main_size.y + .2, z1=pcb_z - 1.6 - eps, z2=pcb_z, r=2.2);
                    let(ofs=1.6)
                    cubex(x1=ofs, x2=autopill_main_size.x - ofs, y1=ofs, y2=autopill_main_size.y - ofs, z1=pcb_z - 1.6 - eps, z2=pcb_z + 2.4);
                    cubex(x1=-4, x2=4, y1=4, y2=40, z1=pcb_z - 1.6 - eps, z2=pcb_z + 2.4);
                }
            }


        }

        let(plugl = 2.54 * 5 + .2, plugw = 2.54 * 2 + .2, zofs=2.2)
        translate(main_pcb_pos) {
            for (pt = mainbox_ports) translate(pt) {
                tz(pcb_z) rz(180)
                render(5)
                idc_cutout();
            }
            *for (pt = mainbox_ports)  translate(pt + 2.54 * [-2, 2]) {
                //tx(13.1 - 2.5) ty(24.2 + 2.54*2)

                z1 = pcb_z + zofs;
                z2 = 3;
                z3 = 4+eps;
                x1 = 0.5;
                //x2 = 0.5;
                x3 = 2.54 + 6.5;
                xzy() linear_extrude(9+2.54*4, center=true, convexity=2)
                polygon([
                    [x1, z3],
                    [x3, z3],
                    [x3, z2],
                    [x3-2, z2-2],
                    [x3-2, z1],
                    [x1, z1],
                    //[x1, z2],
                ]);

                cubex(
                    xc=2.54 * 1.5, xs=plugw,
                    yc=0, ys=plugl,
                    z1=pcb_z - eps, z2=4+eps
                );


                tz(pcb_z)
                yzx() tz(x1) lxpoly(x3 - x1 - 2, points = [
                    [-plugl/2 + zofs, -eps],
                    [-plugl/2, zofs + eps],
                    [plugl/2, zofs + eps],
                    [plugl/2 - zofs, -eps],
                ]);

                cubex(
                    x1=1, x2=2.54 + 5.4,
                    yc=0, ys=4.4,
                    z1=pcb_z-eps, z2=4+eps
                );
            }

            tz(pcb_z - 1.6)
            iter_objects(autopill_main_objects) {
                if ($cpackage == "smd_r") {
                    cubex(
                        xc = 0, xs = 4.6,
                        yc = 0, ys = 2.5,
                        z1 = -10, z2 = 1
                    );

                }
                if ($ref == "PWR1") {
                    tz(-1.6)
                    sz(-1)
                    cubex(
                        x1 = -7.8, x2 = 2,
                        y1 = -7, y2 = 2.5,
                        z1=-10, z2=3,
                        r=2.5
                    );
                }
            }

        }

        *cubex(y1=mainbox_l + 4 + eps, y2=mainbox_l - 10.8, xc=main_pcb_pos.x + autopill_main_pos_PWR1.x, xs=10, z1=-10, z2=1);

    }


    cubex(y1=mainbox_l - 2, y2=mainbox_l + 4 + eps, xc=main_pcb_pos.x + autopill_main_pos_PWR1.x, xs=9.6, z1=-mainbox_h + pcb_top_z - 1.6, z2=0);
    cubex(x1=-4-eps, x2=eps, yc=mega_pos.y + 38.1, ys=14.6, z1=-7.6, z2=0);
}

module preview_mainbox_lid(preview_pcb=preview_pcb) {
    tz(eps)
    mainbox_lid_inlay_front();

    if (preview_pcb) {
        tz(-mainbox_h)
        translate(main_pcb_pos)
        tz(pcb_top_z - 1.6) {
            pcb_model_main();
            *tz(1.6) color("#363636") render(10) mainbox_pcb_spacer();
        }
    }
}

module mainbox_skirt() {
    rx(180)
    skirt(10, [0, 0, 4 - .02], [mainbox_w, mainbox_l, 0]);
}

module logo_text(txt, size) {
    font = "Quicksand:style=Bold";
    spacing = 1;
    ofs = -.1;

    offset(ofs)
    text(txt, size, font, valign="center", halign="center", script="utf8", spacing=spacing);

}

module logo(full=1) {
    //font = "Corpulent Caps BRK";
    //font = "Kickflip BRK";
    //font = "Mishmash BRK";
    //font = "Upheaval TT BRK";
    //font = "Dyuthi";
    //font = "Noto Sans Display:style=Bold";
    font = "Quicksand:style=Bold";
    spacing = 1;
    ofs = -.1;

    if(full)
    logo_text("Pink Panda", 8);

    ty(-13)
    logo_text("Pill Pusher Pro", 11);

}

module mainbox_lid_inlay_2d() {
    font = "Quicksand:style=Bold";

    tx(mainbox_w - 34)
    ty(18)
    rz(180) {
        logo_text("Pink Panda", 7);

        ty(-13)
        logo_text("Pill Pusher", 10);

        ty(-26)
        logo_text("Pro", 10);

    }

    *logo(1);

    translate(main_pcb_pos)
    for (j = [0 : 15]) {
        translate(mainbox_ports[j] + [-1.27, 2.54 * 1.5 + (j > 7 ? 16 : -13.4)])
        let(spc=(j == 13 ? 0.8 : 1))
        rz(180) {
            ty(j >= 8 ? -1 : 0)
            difference() {
                squarex(xc=0, xs=10.8, y1=-4, y2=5);
                ty(j >= 8 ? 2 : 0)
                squarex(xc=0, xs=9, y1=-5, y2=4.1);

            }
            text(str(j + 1), 5.6, font, valign="center", spacing=spc, halign="center", script="utf8");
        }

    }
}

module mainbox_lid_inlay_front() {
    color("gold")
    tz(4-.4) {
        linear_extrude(.4+eps) {
            mainbox_lid_inlay_2d();
        }
    }
}

module mainbox_lid_inlay_back() {
    color("white")
    tz(4-.6) {
        linear_extrude(.2) {
            offset(-8.5)
            offset(10)
            mainbox_lid_inlay_2d();
        }
    }
}
