module jig_pin(z1=0, z2=10) {
    color("gold")
    cubex(
        xc=0, yc=0,
        xs=0.75, ys=0.75,
        z1=z1, z2=z2
    );
    color("blue")
    for (a = [45, -45]) rz(a) {
        cubex(
            xc=0, yc=0,
            xs=2.2, ys=.25,
            z1=z1, z2=z2
        );

    }
}

module md_solder_jig(h=10.0, straight=true) {
    pin_z1 = h - 9.4;

    expand = 1.2;
    size = [25.1, 21.0];
    origin = [29.4, 29.0] - [1.7, 2];
    shift = [.6, -.5];

    difference() {
        cubex(
            x1=-expand, x2=size.x+expand,
            y1=-expand, y2=size.y+expand,
            z1=0, z2=h
        );
        cubex(
            x1=0, x2=size.x,
            y1=0, y2=size.y,
            z1=h - 1.6, z2=h+eps
        );
        tz(h - 1.6 - eps) corners2(0, 0, size.x, size.y) {
            rz(45) cubex(
                x1=-2, x2=5,
                yc=0, ys=2,
                z1=0, z2=1.6+eps2
            );
        }
        for (i = [0 : 9]) {
            pt1 = md_spacer_layer_2[i] - origin;
            pt1f = [pt1.x, size.y - pt1.y];
            translate(pt1f)
            jig_pin(pin_z1, h+eps);
            *color("green")
            translate(pt1f)
            cubex(
                xc=0, yc=0,
                xs=1.4, ys=1.4,
                z1=pin_z1, z2=h+eps
            );
        }

        cubex(xc=size.x/2 + 1, xs=8, y1=-3, y2=size.y + 2, z1=h - 6, z2 = h + eps);
        *cubex(x1=0, x2=size.x - 4, y1=size.y - 9, y2=size.y, z1=h - 8.4, z2 = h + eps);
    }
    if ($preview)
    for (i = [0 : 9]) {
        pt1 = md_spacer_layer_2[i] - origin;
        pt1f = [pt1.x, size.y - pt1.y];
        translate(pt1f) color("gold") cylx(pin_z1, pin_z1 + 11.2, d=0.8);
    }
}
