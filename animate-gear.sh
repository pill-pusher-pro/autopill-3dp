#!/bin/sh

nframes=480
rm anim/gear*.png
openscad autopill.scad \
         -Dpart='"gear-animate"' \
         -Danim_frames=$nframes \
         -Dplanetgear_hgr=true \
         --colorscheme=Metallic \
         --imgsize=1280,720 \
         --animate $nframes \
         --camera 0,0,5,51,0,319,220 \
         -o anim/gear.png

ffmpeg -r 60 -i anim/gear%05d.png -vf scale=1280:720 -y anim/gear.mkv
