module hinge_transform() {
    tz(-hinge_size/2+1)
    ty(-hopper_length/2 - hopper_wall_thick + .05)
    //tx(pri_width / 2)
    rfx() sz(-1)
    tx(hopper_width*.3)
    children();
    //    tx(pri_width / 2 - 10)
}

module snap_tab(fc=0, ext1=0, ext2=0) {
    ww = 1.2+1;
    hh = 12;
    xl = 1.3;
    xw = 1.5;
    ty(-1)
    yzx() linear_extrude(15 + fc, center=true)
    polygon([[0, 0],
        [ww + xl, 0],
        [ww, -xl],
        [ww, -hh + ext2],
        [ww + xl + ext1 + ext2, -hh + ext2],
        [-xw, -hh - ww - xw - xl - ext1],
        [-xw, -hh],
        [0, -hh+xw],
    ]);
}

module snap_tab_support() {
    bx = hopper_wall_thick;
    ww = 1.2;
    hh = 12.6;
    xl = 1.9-.5;
    xw = 1.5;
    difference() {
        yzx() linear_extrude(15, center=true, convexity=10)
        polygon([[bx, 1],
            [bx+1, 0],
            [bx+4, -6],
            //[bx+1, -hh+bx],
            [ww + xl, -hh+1],
            //[ww + .2 + xl, -hh+.2],

            [ww + xl, -hh + .4],

            [ww + xl, -hh],
            [ww + xl - .5, -hh],

            [ww + xl - .5, -hh+.4],

            [ww + .6, -hh+.4],
            [ww + .6, -hh],
            [ww + .3, -hh],
            [ww + .3, -hh+1.5],


            [bx+2, -6],
            //[bx-.5, -2.5],
            [bx, -2],
            [bx-1, 0],
        ]);
        tz(-6)
        ry(45) cubex(xc=0, zc=0, xs=4, zs=4, y1=0, y2=10);
        tz(-6)
        cubex(xc=0, z1=0, z2=10, xs=4*sqrt(2), y1=0, y2=10);
    }
}

module funnel_to(wfrom, wto) {
    factor = (wfrom - wto) / 2 / hopper_funnel_height;
    mtx = [[1, 0, factor, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
    [0, 0, 0, 1]];
    intersection_for(scl=[-1, 1]) {
        sx(scl)
        multmatrix(mtx)
        children();
    }
}

module hopper() {
    difference() {
        union() {
            cubex(xc=0, xs=hopper_width + 2*hopper_wall_thick,
                yc=0, ys=hopper_length + 2*hopper_wall_thick,
                z1=0, z2=hopper_height,
            r=4);

            funnel_to(hopper_width + 2*hopper_wall_thick, total_height + 2*hopper_wall_thick) {
                cubex(xc=0, xs=hopper_width + 2*hopper_wall_thick,
                    yc=0, ys=hopper_length + 2*hopper_wall_thick,
                    z1=-hopper_funnel_height, z2=0,
                r=4);

            }
            cubex(xc=0, xs=total_height + 2*hopper_wall_thick,
                yc=0, ys=hopper_length + 2*hopper_wall_thick,
                z1=-hopper_funnel_height-20, z2=-hopper_funnel_height,
            r=4);
        }


        difference() {
            cubex(xc=0, xs=hopper_width,
                yc=0, ys=hopper_length,
                z1=-eps, z2=hopper_height+eps,
            r=0);

            cubex(xc=0, xs=22,
                y1=hopper_length/2-2, y2=hopper_length/2,
                z1=hopper_height-15, z2=hopper_height,
            r1=1, r2=1);
        }

        funnel_to(hopper_width, total_height - 2) {
            cubex(xc=0, xs=hopper_width,
                yc=0, ys=hopper_length,
                z1=-hopper_funnel_height-eps, z2=eps,
                r=0
            );
        }

        tz(hopper_height)
        ty(hopper_length/2 + hopper_wall_thick + eps)
        sy(-1)
        color("#666666") snap_tab(.6, ext1=.8, ext2=.8);

        // main fitting
        cubex(xc=0, xs=total_height + .4,
            yc=0, ys=hopper_length + 4.4,
            z1=-hopper_funnel_height-20-eps, z2=-hopper_funnel_height+eps,
        r=0);
    }

    ty(-1.2)
    tz(hopper_height)
    hinge_transform()
    difference() {
        hinge_inner(base_size=hinge_size-2, xlen=1.2+eps2, vofs1=4.5, vofs2=0);
        tz(-hinge_size/2)
        ty(-5)
        rx(-40)
        cubex(xc=0, xs=12, yc=0, ys=12, z1=0, z2=1.8);
    }
}

module hinge_outer(base_size=7.5, ww=12, iw=6, xheight=10, cutofs=0) {
    xw = .6;
    difference() {
        tt = base_size;

        union() {
            yzx() {
                linear_extrude(ww, center=true, convexity=1)
                polygon([
                    [0, -xheight],
                    [0, tt/2],
                    [-tt, tt/2],
                    [-tt, -xheight+tt],
                ]);
            }

            tz(tt/2)
            tx(ww/2)
            ty(-tt/2)
            ry(-90)
            cylinder(h=ww, d=tt);
        }


        tz(tt/2)
        tx(ww/2+eps)
        ty(-tt/2)
        ry(-90) {
            cylinder(h=ww+eps2, d=screw_diameter_m2);
            hexnut_hole(h=2, d=nut_diameter_m2);

            cylx(z1=ww-1.6, z2=ww+eps2, d1=screw_diameter_m2, d2=4.8);
        }

        cubex(
            xc=0, xs=iw,
            y1=eps, ys=-tt - eps2,
            z1=-xheight+tt - cutofs, z2=tt + eps
        );
    }
}

module hinge_inner(base_size=7.5, ww=6, xlen=0, vofs1=0, vofs2=0) {
    tt = base_size;

    ty(-tt/2)
    yzx()
    {
        difference() {
            hull() {
                pts = [
                    [0, -tt/2],
                    [0, tt/2],
                    [tt/2+xlen, tt/2+vofs1],
                    [tt/2+xlen, -tt/2+vofs2]
                ];
                linear_extrude(ww, center=true)
                polygon(pts);
                cylinder(h=ww, d=tt, center=true);
            }
            cylinder(h=ww+eps2, center=true, d=screw_diameter_m2);
        }
    }
}

module preview_hopper() {
    if (preview_lid) {
        rpos = [0, -(hopper_length/2 + hopper_wall_thick + hinge_size/2), hopper_height - hinge_size/2 + 1];
        translate(rpos)
        rx(lerp(0, 90, preview_slider))
        translate(-rpos)
        tz(hopper_height+.4+eps) {
            color("blue", 0.5)
            render(10)
            hopper_lid();
        }
    }
}

module hopper_lid() {
    difference() {
        minkowski() {
            cubex(xc=0, xs=hopper_width+3,
                y1=-hopper_length/2-.2, y2=hopper_length/2+1.5,
                z1=-3, z2=11,
            r=0);
            cylinder(h=hopper_wall_thick, r1=hopper_wall_thick, r2=0);
        }

        // main interior
        cubex(xc=0, xs=hopper_width,
            y1=-hopper_length/2, y2=hopper_length/2,
            z1=-3-eps, z2=8,
        r=0);

        // lip interior
        cubex(xc=0, xs=hopper_width+2*hopper_wall_thick+.6,
            y1=-hopper_length/2-2*hopper_wall_thick, y2=hopper_length/2 + hopper_wall_thick + .3,
            z1=-3-eps, z2=0,
        r3=4, r4=4);
    }

    ty(hopper_length/2 + hopper_wall_thick + eps)
    sy(-1) {
        snap_tab(ext1=.8, ext2=-.6);
        color("#886666")
        snap_tab_support();

    }

    ty(-.2)
    tz(hinge_size/2-.4)
    hinge_transform()
    sx(-1)
    hinge_outer(base_size=hinge_size, ww=16, cutofs=1);
}
