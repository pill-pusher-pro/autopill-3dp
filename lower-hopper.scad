function yint(p1, p2, x=0) = [x, lerp(p1.y, p2.y, -(p1.x - x) / (p2.x - p1.x))];
//function yint(p1, p2, x=0) = -(p1.x - x) / (p2.x - p1.x);

function translate_pts(vec, pts) = [for (pt = pts) vec + pt];
function scale_pts2(vec, pts) = [for (pt = pts) [vec.x * pt.x, vec.y * pt.y]];
function scale_pts3(vec, pts) = [for (pt = pts) [vec.x * pt.x, vec.y * pt.y, vec.z * pt.z]];

function bump(x) = (x >= 0 && x <= 1) ? (sin((x - .25) * 360) / 2 + 0.5) : 0;
function cam_rad(ang) = (cam_rad_base + 7 * bump((ang - (180 - poker_cam_ang_size/2)) / poker_cam_ang_size));

module inner(h) {
    tp1 = tangent_pt;
    tp2 = tp1 + tan_vec * 200;

    color("blue")
    linear_extrude(h + eps, convexity=5)
    rfx() intersection() {
        polygon([
            [-eps, ymax+eps],
            [-eps, ymin2-eps],
            [wheel_spacing/2 + .5, ymin2-eps],
            [wheel_spacing/2 + .5, 0],
            [wheel_tx, 0],
            tp1, tp2
        ]);
        polygon([
            [-eps, ymax+eps],
            [-eps, ymin2-eps],
            [hopper_length/2, ymin2-eps],
            [hopper_length/2, ymax+eps],
        ]);
    }
}

module lower_hopper_lid_tabs(fc, wt=0) {
    rfx() {
        cubex(
            x1=hopper_length/2 - wt + eps2, xs=2+eps + wt,
            yc=ymax - 8 - 3, ys=6 + fc,
            z1=0, z2=lid_thick+eps
        );
        cubex(
            x1=pill_width/2 + .5 - wt - eps, xs=2+eps2 + wt,
            yc=ymin2 + 14, ys=6 + fc,
            z1=0, z2=lid_thick+eps
        );

        tx(wheel_tx)
        rz(-tangent_ang)
        tx(-wheel_rad+tan_offset)
        ty(wheel_rad+10)
        cubex(
            x1=-wt-eps, xs=2+eps2 + wt,
            yc=0, ys=6 + fc,
            z1=0, z2=lid_thick+eps
        );
    }
}

module outer2d(ymax=ymax, wt=4, ofs=0, wheels=true) {
    ww = (hopper_length + wt) / 2;

    p1 = tangent_pt + tan_vec * (wheel_rad+wt-ofs) + tan_pt_vec * -wt/2;
    p2 = yint(p1, p1 + tan_vec, ww);

    cpts = [
        //[0, ymax],
        [(hopper_length + wt)/2, ymax],
        p2,
        p1,
        each wheels ? translate_pts([wheel_tx, 0], reverse(arcpts(wheel_rad + 4, -90, 90 - tangent_ang, $fn))) : [],
        [total_height/2, ymin],
        [total_height/2, ymin2],
    ];

    pts = [
        each cpts,
        each scale_pts2([-1, 1], reverse(cpts))
    ];

    //for (i=pts) translate(i) circle(d=1);
    polygon(pts);
}

module outer(h, ymax=ymax, wt=4, ofs=0, wheels=true) {
    color("gold")
    linear_extrude(h, convexity=5)
    outer2d(ymax, wt, ofs, wheels);

}

module lower_hopper_text() {
    ty(ymax - 5)
    tz(-eps)
    linear_extrude(.4 + eps, convexity=10) {
        sx(-1)
        marker_text(dia_text, halign="center", valign="top");
    }
}


module lower_hopper_lid(wt=.4, ext=0) {
    difference() {
        union() {
            color("#f9ac83")
            outer(lid_thick, ymax=ymax + ext, wt=-wt, ofs=wheel_rad, false);
        }

        rfx()
        cubex(
            x1=pill_width_fc/2 - wt, x2=hopper_length,
            y1=0, y2=ymin2,
            z1=-eps, z2=lid_thick+eps
        );

        tz(-pri_height)
        color("#8491fb") {
            inner(pri_height);
            //lower_hopper_base();
        }

        rfx() tx(wheel_tx) {
            // wheel
            cylx(-eps, lid_thick + eps, r=wheel_rad + wheel_fit_clearance_2);
        }

        if (hopper_window) {

            tz(.4)
            linear_extrude(lid_thick) {
                intersection() {
                    union() {
                        squarex(
                            xc=0, xs=hopper_length,
                            y1 = 0, y2 = ymax - 15
                        );
                        squarex(
                            xc=0, xs=pill_width - 4,
                            y1 = ymin, y2 = 0
                        );
                    }
                    difference() {
                        offset(-3, chamfer=true)
                        outer2d(ymax=ymax + ext, wt=-wt, ofs=wheel_rad);
                        rfx() tx(wheel_tx) circle(r=wheel_rad + 3);
                        for (yy = [0 : 3]) ty(lerp(wheel_rad/2, ymax - 15, yy/4))
                        squarex(
                            xc=0, xs=hopper_length,
                            yc=0, ys=2
                        );
                    }
                }
            }


            *intersection() {
                hole_size = 4.6;
                hole_spacing = 8;
                ty(16)
                rz(45) {
                    for (xx = [0 : 1 : 10]) tx(xx * hole_spacing)
                    for (yy = [0 : 1 : 10]) ty(yy * hole_spacing) {
                        if (xx + yy < 14 && xx - yy > -10 && yy - xx > -10)
                        cubex(x1=0, xs=hole_size, y1=0, ys=hole_size, z1=-eps, z2=lid_thick+eps);
                    }
                }
                y2 = (13 * hole_spacing + hole_size)/sqrt(2);
                xs = (18 * hole_spacing) / sqrt(2);
                ty(16)
                cubex(xc=0, xs=xs, y1=0, y2=y2, z1=-eps, z2=lid_thick+eps);

            }

        }


        //taper
        ty(ymax + ext) {
            tz(lid_thick)
            rx(-45)
            cubex(y1=0, y2=20, z1=0, z2=-pri_depth * 2, xc=0, xs=hopper_length+eps);
        }

        tz(lid_thick)
        ry(180)
        lower_hopper_text();

        *ty(ymax) tz(lid_thick) {
            cubex(xc=0, xs=hopper_length+eps, y1=0, y2=ext + eps, z1=-1.2, z2=eps);
        }
    }
    lower_hopper_lid_tabs(fc=0, wt=.6);
}

module lower_hopper_base(xfc=0, h=pri_height +  lid_thick) {
    difference() {
        color("gold")
        render(10)
        union() {
            outer(h + pri_depth);
            rfx() tx(wheel_tx)
            linear_extrude(6) intersection() {
                circle(r=pg_shroud_inner);
                squarex(
                    x1=-pg_shroud_inner, x2=pg_shroud_inner,
                    y1=ymin, y2=pg_shroud_inner
                );
            }
        }

        tz(pri_depth)
        color("blue")
        render(10)
        inner(h);

        tz(pri_depth + pri_height)
        lower_hopper_lid_tabs(.2);

        rfx() tx(wheel_tx) {
            // wheel
            cylx(4-eps, total_height + eps, r=wheel_rad + wheel_fit_clearance_2);

            // bearing
            cylx(1-eps, pri_depth, d=bearing_outer);

            // shaft passthrough
            cylx(z1=-eps, z2=1+eps, d=bearing_inner + 5);
        }

        //taper
        let(ext=12)
        ty(ymax) {
            yzx() lxpoly(hopper_length-eps, center=true, convexity=2, points=[
                [eps, pri_depth + eps],
                [eps, eps],
                [-1, 1],
                [-1 - ext, 1],
                [-pri_depth - ext - eps, pri_depth + eps],

            ]);
            *rx(45)
            cubex(y1=0, y2=20, z1=0, z2=pri_depth * 2, xc=0, xs=hopper_length-eps);
        }

        rfx() tx(wheel_tx) {
            render(4) tz(-eps) pillwheel_holder_outer(.2);
            pillwheel_holder_screws(undef, [0, 45, 90], pg_shroud_inner+1);
            pillwheel_holder_screws(undef, [-45], wheel_rad + 5);
        }

        // pg mount
        for (ang = [0, 180]) rz(ang)
        tx(wheel_tx)
        for (m=pg_mounts) let(ang=m[0], xx=m[1]) rz(ang + 180)
        tx(xx) cylx(-eps, min(2.6, pri_depth - 1), d=3.5);

        lower_hopper_text();
    }
}

module pillwheel_preview(wheel_color, beltwheel_color, left=false) {
    holder_ang = left ? -90 : 0;
    bwofs = left ? 0 : beltwheel_offset;
    wheel_rot = left ? preview_wheel_rot : -preview_wheel_rot;

    if (preview_pillwheel_holder) {
        color("#66aa66", 0.5)
        rz(holder_ang)
        render(10)
        pillwheel_holder();
    }

    rz(wheel_rot) {

        if (preview_pillwheel) {
            tz(total_height/2 + poker_height/2 - .2)
            for (i = [0 : poker_spokes - 1]) rz(i * 360 / poker_spokes) {
                ang = wheel_rot + holder_ang + i * 360 / poker_spokes - poker_cam_ang;
                tx(cam_rad(ang))
                rx(180)
                render(10)
                pillwheel_poker_cam_follower();
            }

            color(wheel_color, 0.5)
            render(10)
            pillwheel_poker();
        }

        if (preview_bearing) {
            tz(bearing_top_z - bearing_thick)
            color("#444488", 0.5)
            render(10)
            bearing_model();
        }


        if (preview_pillgear) {
            pillwheel_gear_spacer();
            tz(bearing_top_z - bearing_thick - beltwheel_bearing_spacing) {
                color(beltwheel_color, 0.5)
                render(10)
                pillwheel_gear(left);
            }
        } else {
            if (preview_beltwheel) {
                tz(bearing_top_z - bearing_thick - beltwheel_bearing_spacing - bwofs)
                render(10)
                beltwheel(bwofs);
            }
        }

    }

    if (!left && preview_planetgear) {
        *planetgear_shim();

        tz(pg_z) {
            sun_rot = (wheel_rot + 100) * pg_gear_ratio;
            planetgear(rotate=sun_rot);
            if (preview_beltwheel)

            tz(-2) rz(sun_rot)
            color(beltwheel_color, 0.5)
            render(10)
            planetgear_beltwheel();
        }
    }

}

module preview_lower_hopper_base() {
    if (preview_lid) {
        color("#ac62e7", 0.5)
        tz(pri_height + pri_depth) render(3) lower_hopper_lid();
    }

    tx(wheel_tx) {
        pillwheel_preview("green", "#cc00aa", false);
    }

    tx(-wheel_tx) {
        pillwheel_preview("red", "#00aacc", true);
    }

    if (preview_hopper) {
        color("blue", 0.5)
        tz(total_height/2)
        ty(ymax + hopper_funnel_height + .2)
        rx(-90)
        rz(-90)
        render(10)
        hopper();

    }
}

module lower_hopper_base_shim() {
    linear_extrude(4, convexity=4) {
        difference() {
            square(total_height, center=true);
            square([pill_width_fc, pill_thick_fc], center=true);
        }
    }
}

module lower_hopper_base_test() {
    intersection() {
        lower_hopper_base();
        tx(wheel_tx)
        cylx(-10, total_height/2 + 3, r = wheel_rad + 4);
    }
}
