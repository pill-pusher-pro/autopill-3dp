module_interface_xpos = [dispbase_x1 + 20, dispbase_x2 - 20];
module_interface_center = (dispbase_x1 + dispbase_x2) / 2;
module_interface_screws = [
    module_interface_center - 16,
    module_interface_center,
    module_interface_center + 16,
];
module_interface_width = 20;

riser_interface_screws = [
    module_interface_center - 20,
    module_interface_center + 20,
];

stackable_posts = [
    [dispbase_x1 + 4, dispbase_y1 + 4, 180, -1],
    [dispbase_x1 + 4, dispbase_y2 - 4, 0, 1],
    [dispbase_x2 - 4, dispbase_y1 + 4, 180, 1],
    [dispbase_x2 - 4, dispbase_y2 - 4, 0, -1],
];

function vcross(v1, v2) = [
    v1.y * v2.z - v1.z * v2.y,
    v1.z * v2.x - v1.x * v2.z,
    v1.x * v2.y - v1.y * v2.x
];

module cylpt(v1, v2, r=undef, d=undef, r1=undef, r2=undef, d1=undef, d2=undef) {

    translate(v1) {
        dv = v2 - v1;
        lxy2 = dv.x * dv.x + dv.y * dv.y;

        if (lxy2 < 0.0001) {
            cylinder(h=dv.z, r=r, d=d, r1=r1, r2=r2, d1=d1, d2=d2);
        } else {
            length = sqrt(lxy2 + dv.z * dv.z);
            rz = atan2(dv.y, dv.x);
            ry = atan2(sqrt(lxy2), dv.z);
            rz(rz) ry(ry) cylinder(h=length, r=r, d=d, r1=r1, r2=r2, d1=d1, d2=d2);
        }
    }
}

module stackable_clipbox(h) {
    cubex(
        x1=dispbase_x1, x2=dispbase_x2, y1=dispbase_y1, y2=dispbase_y2,
        z1=4-eps, z2=h,
    );

}

module stackable_brace(corner, brace_h, brace_w=undef, ang=0) {
    post = stackable_posts[corner];
    brace_w = is_undef(brace_w) ? brace_h * .75 : brace_w;


    translate([post[0], post[1]])
    rz(ang * post[3] + post[2])
    union() {
        v1 = [0, -brace_w, 0];
        v2 = [0, 0, brace_h];
        *translate(v1) color("blue") sphere(d=6);
        *translate(v2) color("green") sphere(d=6);
        cylpt(v1, v2, d=6);
    }
    *yzx() {
        linear_extrude(6, center=true)
        polygon([[-brace_w + 6, 0], [-brace_w, 0], [0, brace_h], [0, brace_h - 6]]);
    }
}

module stackable_outer(h, top=true, bot=true) {
    difference() {
        union() {
            cubex(
                x1=dispbase_x1, x2=dispbase_x2, y1=dispbase_y1, y2=dispbase_y2,
                z1=0, z2=4,
                r=4
            );

            braces = !is_undef(braces) ? braces : [
                [0, 0, undef],
                [1, 0, undef],
                [2, 0, undef],
                [3, 0, undef],
                each (h < 30 ? [] : [
                    [0, 90, undef],
                    [1, 90, undef],
                    [2, 90, undef],
                    [3, 90, undef]
                ]),
            ];

            for (i = [0 : 3]) translate([stackable_posts[i][0], stackable_posts[i][1]]) {
                cylx(z1=0, z2=h, d=8);
                if (top) rz(stackable_posts[i][2]){
                    let(zofs=15, yofs=7)
                    tz(h - zofs) {
                        difference() {
                            intersection()  {
                                yzx() lxpoly(8, center=true, points=[
                                    [0, 0],
                                    [-yofs - 4, yofs + 4],
                                    [-yofs - 4, zofs],
                                    [0, zofs],

                                ]);
                                union() {
                                    linehull([[0, 0], [0, -yofs]]) cylx(0, zofs, d=6);
                                    ty(-yofs)
                                    cylx(0, zofs+eps, d=8);

                                }
                            }
                            ty(-yofs)
                            cylx(0, zofs+eps, d=insert_dia_m2_5);
                        }
                    }
                    intersection() {
                        union() {
                            cylx(z1=h, z2=h+10, d=5 - .3);
                            cylx(z1=h+10, z2=h+12.6, d1=5 - .3, d2=0);
                        }
                        cubex(xc=0, yc=0, xs=6, ys=6, z1=h, z2=h+11.5);
                    }

                }
            }

        }

    }
}

module stackable_inner(h, bot=true) {
    if (bot)
    corners2(dispbase_x1+4, dispbase_y1+4, dispbase_x2-4, dispbase_y2-4) {
        cylx(z1=-eps2, z2=10, d=5.3);
        cylx(z1=10-eps, z2=13, d1=5.3, d2=0);
        ty(-7) countersink_m2(4);
    }
}

module stackable(h, top=true, bot=true, brace_h=undef, brace_w=undef) {
    brace_h1 = is_undef(brace_h) ? min(h - 4, 74) : brace_h;
    brace_h2 = is_undef(brace_h) ? min(h - 4, 74) : brace_h;

    difference() {
        union() {
            stackable_outer(h, top, bot);
            intersection() {
                rfy() rfx((dispbase_x2 + dispbase_x1) / 2)
                union() {
                    translate([1, 0, -1/.75] * 8)
                    stackable_brace(0, brace_h1, brace_w, 0);
                    stackable_brace(0, brace_h2, brace_w, 90);
                }
                stackable_clipbox(h);
            }
        }
    }
}

stackable_post_size = [9.5, 14];
stackable_post_tab_size = [5, 8, 12];

module stackable_tab(fc=0, r=2) {
    let(tab=stackable_post_tab_size)
    yzx() cubex(
        xc=0, xs=tab.y + 2*fc,
        zc=0, zs=tab.x + 2*fc,
        y1=0, y2=tab.z + 2*fc,
        r3=r, r4=r
    );

}

module stackable_test(h, top=true, bot=true, brace_h=undef, brace_ang=undef) {
    difference() {
        union() {
            cubex(
                x1=dispbase_x1, x2=dispbase_x2, y1=dispbase_y1, y2=dispbase_y2,
                z1=0, z2=4,
                r=4
            );

            // mount posts
            corners2(dispbase_x1, dispbase_y1, dispbase_x2, dispbase_y2) {

                cubex(
                    z1=0, z2=h,
                    x1=0, x2=-stackable_post_size.x,
                    y1=0, y2=-stackable_post_size.y,
                    r=4
                );


                brace_h = is_undef(brace_h) ? min(h - 4, 74) : brace_h;
                brace_w = brace_h * .75;
                translate([-4, -4])
                for (ang = brace_ang) rz(ang)
                ty(-(stackable_post_size.y - 6) * cos(ang) + (stackable_post_size.x - 6) * sin(ang))
                yzx() {
                    linear_extrude(6, center=true)
                    polygon([[-brace_w + 6, 0], [-brace_w, 0], [0, brace_h], [0, brace_h - 6]]);
                }

                if (top) {
                    tz(h)
                    let(tab=stackable_post_tab_size)
                    translate(-stackable_post_size/2)
                    difference() {
                        stackable_tab(0);
                        tz(tab.z - tab.y/2) yzx() {
                            cylx(zc=0, zs=tab.x + eps, d=screw_diameter_m2);
                            linehull([[0, 0], [0, 5]]) rz(30) tz(-2.5/2) hexnut_hole(h=2.5, d=nut_diameter_m3);
                        }
                    }
                }
                *intersection() {
                    union() {
                        cylx(z1=h, z2=h+10, d=5 - .3);
                        cylx(z1=h+10, z2=h+12.6, d1=5 - .3, d2=0);
                    }
                    cubex(xc=0, yc=0, xs=6, ys=6, z1=h, z2=h+11.5);
                }

            }
        }

        if (bot)
        corners2(dispbase_x1, dispbase_y1, dispbase_x2, dispbase_y2) {
            translate(-stackable_post_size/2) tz($preview ? -eps : 0){
                stackable_tab(.2, 0);
                let(tab=stackable_post_tab_size)
                tz(tab.z - tab.y/2) yzx() {
                    rfz() countersink_m2(stackable_post_size.x/2 + .2);
                    *cylx(zc=0, zs=stackable_post_size.x + eps, d=screw_diameter_m2);
                }
            }

        }

        *for (xx = [dispbase_x1+4, dispbase_x2-4], yy=[dispbase_y1+4, dispbase_y2-4]) tx(xx) ty(yy) {
            cylx(z1=-eps2, z2=10, d=5.3);
            cylx(z1=10-eps, z2=13, d1=5.3, d2=0);
        }
    }
}

// spacer to adjust already-printed double risers
module stackable_spacer(h=module_rise_rear - module_rise) {
    difference() {
        union() {
            cylx(z1=0, z2=h, d=8);
            intersection() {
                union() {
                    cylx(z1=h, z2=h+10, d=5 - .3);
                    cylx(z1=h+10, z2=h+12.6, d1=5 - .3, d2=0);
                }
                cubex(xc=0, yc=0, xs=6, ys=6, z1=h, z2=h+11.5);
            }
        }
        cylx(z1=-eps2, z2=10, d=5.3);
        cylx(z1=10-eps, z2=13, d1=5.3, d2=0);
    }
}

module riser_interface_out_outer(screws=riser_interface_screws) {
    ty(-8) riser_interface_in_outer();
}

module riser_interface_in_outer(screws=riser_interface_screws) {
    tx(screws) {
        tz(7)
        xzy() linear_extrude(8) {
            circle(d=8);
            squarex(xc=0, xs=8, y1=-4, y2=0);
        }
    }
}

module riser_interface_in_inner(screws=riser_interface_screws) {
    tx(screws) {
        tz(7)
        xzy() countersink_m2(8.2 + 5, head_extra = 5);
    }
}

module riser_interface_out_inner(screws=riser_interface_screws) {
    tx(screws) {
        tz(7)
        xzy() {
            cylx(-insert_length_m2_5, eps, d=insert_dia_m2_5);
            cylx(-12-eps, eps, d=screw_diameter_m2);
        }
    }
}

module module_interface(fc=0) {

    for (xx = module_interface_xpos) {
        x1 = xx - module_interface_width/2;
        x2 = xx + module_interface_width/2;
        tz(-eps)
        linear_extrude(4+eps2)
        polygon([
            [x1-fc + 6, -eps],
            [x1-2*fc  , 6+fc],
            [x2+2*fc  , 6+fc],
            [x2+fc - 6, -eps]
        ]);
    }
}

module module_interface_up(feet=true, module_interface_screws=[module_interface_screws[0]]) {
    x1 = module_interface_xpos[0] - module_interface_width/2;
    x2 = module_interface_xpos[1] + module_interface_width/2;

    z1 = module_rise - 12;
    z2 = module_rise - 4;
    difference() {
        yzx() tz(x1) linear_extrude(x2-x1, convexity=4)
        polygon([[-8, z1],

            [-8, 0],
            [0, 0],
            [0, z1-2],
            [8, z2],
            [8, module_rise],
            [0, module_rise],
            [0, z2+2],
        ]);

        tx(module_interface_screws)
        ty(5) {
            cylinder(h=module_rise+eps, d=screw_diameter_m2);
            linehull([[0, 0], [0, 8]])
            tz(module_rise - 4)
            rz(30)
            hexnut_hole(h=2.5, d=nut_diameter_m2);
        }
        *cubex(
            x1=-50, x2=28,
            y1=0, y2=8,
            z1=0, z2=module_rise
        );

        riser_interface_out_inner();
    }
    tx(module_interface_screws)
    ty(5) tz(module_rise - 4 + 2.5)
    bridge_support(5, h=.3);

    color("green")
    tz(module_rise) module_interface();

}

module module_interface_down() {
    module_interface(.2);
    tx(module_interface_screws[0])
    ty(5)
    tz(4 - 10)
    countersink_m2(10, expand=1, nut_length=2.5);
}

module pill_slide_xform() {
    tz(slide_tube_dia/2)
    ty(dispbase_y1)
    multmatrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, module_rise / (dispbase_y2 - dispbase_y1), 1, 0],
        [0, 0, 0, 1]
    ])
    ty(-dispbase_y1)
    children();
}

module pill_slide_cut(fc=0, mult=1) {
    pill_slide_xform()
    xzy() {
        dd = (slide_tube_dia - 4) / 2 - fc;
        tz(dispbase_y1 - eps)
        linear_extrude(mult * (dispbase_y2 - dispbase_y1) + eps2, convexity=10) {
            polygon([[0, -dd],
                [-dd, 0],
                //[0, dd],
                [0, dd],
                [dd, 0],
            ]);

        }
    }
}

module slide_insert(xform=!$preview, slide_insert_length=slide_insert_length, endcap=true) {
    xofs = dispbase_y2 - dispbase_y1;
    yofs = module_rise;
    leng = hypot(xofs, yofs);
    xcos = xofs / leng;

    fc = .4;

    dd = (slide_tube_dia - 4) / 2 - fc;

    ang1 = atan2(xcos, 1);

    ry((xform && !$preview) ? ang1 : 0)
    rx(xform ? -atan2(yofs, xofs) : 0)
    tz(-2)
    ty(xform ? -dispbase_y1 : 0)
    difference() {
        union() {
            pill_slide_cut(fc = fc, mult=slide_insert_length);
            if (endcap)
            pill_slide_xform() ty(dispbase_y1 + xofs * slide_insert_length) {
                tz(-dd)
                xzy() lxpoly(2, points=[
                    [0, 0],
                    [dd, dd],
                    [0, 2*dd],
                    [-dd, dd] - [2, 2],
                    [-2, -2],
                ]);

                *let(ptofs=[1, 1] * (dd/2 - 1))
                ty(2)
                tz(-dd)
                xzy() lxpoly(10, points=[
                    [dd, dd] - ptofs,
                    [0, 2*dd] - ptofs,
                    [-dd, dd] + ptofs,
                    ptofs,
                ]);

                let(ww=1.5)
                ty(2)
                tz(-dd)
                xzy() lxpoly(9, points=[
                    [0, dd],
                    [0, dd] + [-ww, -ww],
                    //[-dd, dd],
                    [-ww, ww],
                    [0, 0],
                    [ww, ww],
                    //[dd, dd],
                    [0, dd] + [ww, -ww],
                ]);
            }
        }

        endpos = xofs * slide_insert_length + (endcap ? -10 : 10);

        pill_slide_xform() ty(dispbase_y1) xzy() {
            cubex(
                xc=0, xs = slide_tube_dia,
                y1 = 0, y2 = dd + 2,
                z1=-10, z2=endpos,
            );
            cylx(-10, endpos, d=((slide_tube_dia - 4) / sqrt(2) - 4));

        }
    }
}

module basemodule(flip=false) {
    top = slide_top;

    difference() {
        union() {
            bh1 = 48;
            bh2 = 64;
            bh3 = 64;

            stackable_outer(slide_top, top=false, bot=true);
            intersection() {
                basept = [dispbase_x1+4, dispbase_y1+4, 0];
                v1 = basept + vxy(rotpt([0, 45], -68));
                v2 = basept + [0, 0, bh2];

                v3 = lerp(v1, v2, 0.8);
                v4 = [v3.x, -v3.y, v3.z];

                union() {
                    rfy() cylpt(v1, v2, d=6);

                    v5 = vxy(idc_center) + [0, -6, 10];
                    cylpt(v5, v3, d=6);
                    *cylpt(vxy(v5, slide_top - 16), lerp(v5, v3, 0.6), d=6);

                    v6 = vxy(idc_center) + [0, 6, 10];
                    cylpt(v6, v4, d=6);
                    *cylpt(vxy(v6, slide_top - 16), lerp(v6, v4, 0.9), d=6);

                    cylpt(vxy(idc_center, slide_top - 24), vxy(idc_center + [26, 0]), d=6);

                }

                stackable_clipbox(slide_top);
            }

            intersection() {
                rfy()
                translate([dispbase_x2-4, dispbase_y1+4]) {
                    v1 = [-60, 0, 0];
                    v2 = [0, 0, bh2];
                    cylpt(v1, v2, d=6);

                    v3 = lerp(v1, v2, 0.85);
                    v4 = [v3.x, dispbase_y2 - 2, 0];
                    cylpt(v4, v3, d=6);
                }
                cubex(
                    x1 = 0,
                    x2 = dispbase_x2,
                    y1 = dispbase_y1,
                    y2 = dispbase_y2,
                    z1 = 0, z2 = slide_top
                );
            }

            translate(idc_center) {
                cubex(
                    xc=0, xs=idc_size.x + 4,
                    yc=0, ys=idc_size.y + 4,
                    z1=0, z2=slide_top,
                    r=1
                );
            }

            sy(flip ? -1 : 1)
            ty(dispbase_y2)
            module_interface_up();

            sy(flip ? -1 : 1)
            tz(slide_tube_height) {
                dd = (slide_tube_dia - 4) / 2;

                intersection() {
                    cubex(xc=0, xs=slide_tube_dia + 8, y1=dispbase_y1, y2=dispbase_y2, z1=-slide_tube_height, z2=top - slide_tube_height);
                    union() {
                        pill_slide_xform()
                        xzy()
                        tz(dispbase_y1)
                        linear_extrude(dispbase_y2 - dispbase_y1, convexity=10) {

                            inner_adj = 8;

                            polygon([[-dd + inner_adj, -top],
                                [-dd + inner_adj, -4 - inner_adj],
                                [-dd - 4, 0],
                                [0, dd + 4],
                                //[0, 0],
                                [dd + 4, 0],
                                [dd -  inner_adj, -4 -  inner_adj],

                                [dd -  inner_adj, -top]
                            ]);
                        }

                        pill_slide_xform()
                        cylx(0, top - slide_tube_height, d=slide_tube_dia + 4);
                    }
                }
            }

        }

        translate(idc_center) tz(slide_top) {
            cubex(
                xc=0, xs=idc_size.x,
                yc=0, ys=idc_size.y,
                z1=-idc_protrusion, z2=eps
            );
            cubex(
                xc=0, xs=12,
                yc=0, ys=13.2,
                z1=-idc_protrusion - 6, z2=eps
            );
        }

        // screw holes
        for (xx = [dispbase_x1+4, dispbase_x2-4], yy=[dispbase_y1+4, dispbase_y2-4]) tx(xx) ty(yy) {
            if (threaded_inserts) {
                cylx(z1=top-15, z2=top+eps, d=screw_diameter_m3);
                cylx(top - 6, top + eps, d=4.2);
            } else {
                cylx(z1=top-15, z2=top+eps, d=screw_diameter_m3 - .3);
            }
        }

        // slide
        sy(flip ? -1 : 1) {
            tz(slide_tube_height) {
                pill_slide_cut();

                pill_slide_xform()
                cylx(0, top, d=slide_tube_dia-4);

                ty(dispbase_y2)
                tz(-slide_tube_height)
                riser_interface_out_inner();
            }
            ty(dispbase_y1)
            module_interface_down();
        }

        stackable_inner(slide_top, bot=true);



    }
}

module basemodule_r() {
    basemodule(flip=false);
}

module basemodule_l() {
    basemodule(flip=true);
}

module preview_basemodule_r(wb_sy=1) {
    *for (xx = [0, 1, 2, 3, 4]) {
        ty(xx*(dispbase_y2 - dispbase_y1) + eps) {

            if (xx > 0)
            color("blue", 0.5)
            tz(module_rise*xx + eps)
            render(10) basemodule();

            for (yy = [-1 : xx-1]) tz(module_rise*yy)
            color("magenta", 0.5)
            render(10) riser(1);

            tz(module_rise*xx + eps)
            tz(slide_top)
            tz(dispenser_wirebox_height)
            sy(wb_sy) {

                color("green", 0.5)
                render(10)
                dispenser();
                preview_dispenser();
            }
        }
    }

}

// =======================================================================================
// riser
// =======================================================================================

module riser(h=module_rise, ifdown=true, bot=true) {
    difference() {
        union() {
            stackable(h, true, bot);
            if (ifdown) ty(dispbase_y1) riser_interface_in_outer();
            ty(dispbase_y2) riser_interface_out_outer();
        }
        if (ifdown) ty(dispbase_y1) riser_interface_in_inner();
        ty(dispbase_y2) riser_interface_out_inner();
        stackable_inner(h, bot=true);
    }
}

module riser_dbl_base(n=1, sy=1, mbox_y=0) {
    base_height = max(1, n) * module_rise;
    difference() {
        union() {
            stackable_outer(base_height);

            intersection() {
                sy(sy) {
                    brace_h1 = min(base_height - 10, 74);
                    brace_h2 = min(base_height - 10, 110);


                    stackable_brace(0, brace_h1, ang=0);
                    stackable_brace(1, brace_h1, ang=0);
                    stackable_brace(2, brace_h1, ang=0);
                    stackable_brace(3, brace_h1, ang=0);
                    stackable_brace(0, brace_h2, ang=90);
                    stackable_brace(1, brace_h2, ang=90);
                    stackable_brace(2, brace_h2, ang=90);
                    stackable_brace(3, brace_h2, ang=90);

                    ty(dispbase_y1) riser_interface_in_outer();
                    ty(dispbase_y2) riser_interface_out_outer();
                }
                stackable_clipbox(base_height);
            }

            tx(-dispbase_row_spacing) {
                stackable_outer(base_height + module_rise_rear);
                intersection() {
                    sy(sy) {

                        brace_h1 = min(module_rise_rear + base_height - 10, 74);
                        brace_h2 = min(module_rise_rear + base_height - 10, 130);

                        stackable_brace(0, brace_h1, ang=0);
                        stackable_brace(1, brace_h1, ang=0);
                        stackable_brace(2, brace_h1, ang=0);
                        stackable_brace(3, brace_h1, ang=0);
                        stackable_brace(1, brace_h2, ang=90);
                        stackable_brace(3, brace_h2, ang=90);
                        if (n > 0) {
                            stackable_brace(0, brace_h2, ang=90);
                            stackable_brace(2, brace_h2, ang=90);
                            ty(dispbase_y1) riser_interface_in_outer();
                        }
                        ty(dispbase_y2) riser_interface_out_outer();
                    }
                    stackable_clipbox(base_height + module_rise_rear);
                }
            }

            tx(dispbase_x2 - dispbase_row_spacing) {
                cubex(
                    x1=-10, x2=20,
                    y1 = dispbase_y1, y2 = dispbase_y2,
                    z1 = 0, z2 = 4
                );

                rfy() ty(dispbase_y1 + 4) {
                    linehull([[14, 0], [-4, 0]]) cylx(0, base_height, d=8);
                }
            }
        }

        sy(sy) {
            ty(dispbase_y1) riser_interface_in_inner();
            ty(dispbase_y2) riser_interface_out_inner();

            tx(-dispbase_row_spacing) {
                ty(dispbase_y1) riser_interface_in_inner();
                ty(dispbase_y2) riser_interface_out_inner();
            }
        }

        if (n == 0) {
            tx(pilltray_holder_x1 - mainbox_l - 4)
            ty(mbox_y - mainbox_xcenter) {
                wall_thick = 4.4;
                cubex(
                    x1=-wall_thick, x2=mainbox_l+wall_thick,
                    y1=-wall_thick, y2=mainbox_w+wall_thick,
                    z1=-2, z2=mainbox_h, r=4
                );

            }
        }
    }
}

module riser_dbl_r(n=1) {
    mbox_y = dispbase_y1 - pilltray_width/2;
    riser_dbl_base(n, 1, mbox_y);
    if (n == 0) {
        tx(-dispbase_row_spacing)
        ty(mbox_y - mainbox_xcenter + mainbox_w + 4.4)
        difference() {
            screws = [module_interface_center];
            riser_interface_in_outer(screws);
            riser_interface_in_inner(screws);
        }

        ty(mbox_y - mainbox_xcenter + mainbox_w - 32)
        tx(pilltray_holder_x1 - mainbox_l - 8) {
            rz(90)
            difference () {
                riser_interface_in_outer([0]);
                riser_interface_in_inner([0]);
            }

        }
    }
}

module riser_dbl_l(n=1) {
    mbox_y = dispbase_y2 + pilltray_width/2;
    riser_dbl_base(n, -1, mbox_y);
    if (n == 0) {
        tx(-dispbase_row_spacing)
        ty(mbox_y - mainbox_xcenter - 4.4) sy(-1)
        difference() {
            screws = [module_interface_center];
            riser_interface_in_outer(screws);
            riser_interface_in_inner(screws);
        }
    }
}

module idc_holder_shim() {
    thick = 2;
    thick2 = 4;

    corners = [
        [dispbase_x1+4, dispbase_y1+4],
        [dispbase_x2-4, dispbase_y1+4],
        [dispbase_x2-4, dispbase_y2-4],
        [dispbase_x1+4, dispbase_y2-4],
    ];

    idc_center = pcb_offset + autopill_dispenser_single_pos_J1 + 2.54 * [-.5, -2];

    difference() {
        union() {
            linear_extrude(thick, convexity=10)
            difference() {
                union() {
                    for (pt = corners) {
                        translate(pt) circle(d=8);
                        *linehull([pt, [0, 0]]) circle(d=4);
                    }
                    *linehull([each corners, corners[0]]) circle(d=4);

                    circle(d=slide_tube_dia + 4);

                    squarex(x1=0, x2=dispbase_x2 - 4, yc=0, ys=4);
                    squarex(y1=dispbase_y1 + 4, y2=dispbase_y2 - 4, xc=0, xs=4);
                    *squarex(y1=dispbase_y1 + 4, y2=dispbase_y2 - 4, x1=dispbase_x2 - 12, x2=dispbase_x2-4);
                    *squarex(y1=dispbase_y1 + 4, y2=dispbase_y2 - 4, x1=dispbase_x1+4, x2=idc_center.x + 5);
                }

                circle(d=slide_tube_dia - 4);
                for (pt = corners) translate(pt) circle(d=screw_diameter_m3 + .5);
            }

            tz(-thick2)
            linear_extrude(thick + thick2, convexity=10) difference() {
                squarex(y1=dispbase_y1 + 2.5, y2=dispbase_y2 - 2.5, x1=dispbase_x1+2.5, x2=dispbase_x2 - 2.5);
                squarex(y1=dispbase_y1 + 8, y2=dispbase_y2 - 8, x1=idc_center.x + idc_size.x/2 + 2, x2=dispbase_x2 - 8, r=4);
                for (pt = corners) translate(pt) circle(d=8);

            }

            translate(idc_center) {
                cubex(
                    xc=0, xs=idc_size.x + 4,
                    yc=0, ys=idc_size.y + 4,
                    z1=-idc_protrusion - 2, z2=2
                );
            }
        }

        render(4)
        translate(idc_center) {
            cubex(
                xc=0, xs=idc_size.x,
                yc=0, ys=idc_size.y,
                z1=-idc_protrusion, z2=2 + eps
            );
            cubex(
                xc=0, xs=12,
                yc=0, ys=13.2,
                z1=-idc_protrusion - 2 - eps, z2=2 + eps
            );
        }

        tz(-5) ry(45) cubex(xc=0, xs=4, zc=0, zs=4, y1=dispbase_y1, y2=dispbase_y2);
    }
}

module preview_idc_holder_shim() {
    if (preview_wirebox)
    tz(3+eps)
    color("blue", 0.4)
    render(10) dispenser_wirebox();

}
