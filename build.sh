#!/bin/sh -e
IFS=

input=$1
shift
if [ $# -eq 0 ]; then
    output=${input%.*}.stl
    if [ "$output" = "$input" ]; then
        input=${input%.*}.scad
    fi
else
    output=$1
    shift
fi

echo "building $output"
openscad --export-format binstl $input -o temp-$output "$@"
./ktpanda/stlsort.py temp-$output -o $output
rm -f temp-$output
echo "$output complete"
