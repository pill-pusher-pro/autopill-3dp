module dispenser(pcbdrill=true, pincher=pincher) {
    difference() {
        union() {

            cubex(x1=dispbase_x1, x2=dispbase_x2, y1=dispbase_y1, y2=dispbase_y2,
                z1=-4, z2=0,
            r=4);

            // main tube tower
            color("#ff0099")
            cubex(xc=0, xs=tower_w + 2*tower_wall_thick.x,
                yc=0, ys=tower_l + 2*tower_wall_thick.y,
                z1=0, z2=tower_h,
            r=4);


            rfy() ty((tower_l + .4+eps)/2 + 5)
            mount_tower(0, tower_h+eps);
        }

        // main groove
        cubex(
            xc=0, xs=tower_w + .4,
            yc=0, ys=tower_l + .4,
            z1=-eps, z2=tower_h+eps
        );

        // slider groove
        if (pincher) {
            slider_groove_base(dispbase_x1, dispbase_x2+eps, .4);

            // spring space

            cubex(x1=tower_w/2 + tower_wall_thick.x, x2=spring_x1,
                yc=0, ys=springw,
            z1=-3, z2=eps);
            *color("#1b7092")
            tz(-2)
            tx(spring_x1-eps)
            yzx() {
                spring_hole(springl + eps);
            }
        } else {
            slider_groove_base(dispbase_x1, dispenser_slide_extra_f + dispenser_pill_hole * 2.5, .4);
        }

        cubex(
            xc=0, xs=tower_w + 2*tower_wall_thick.x + eps,
            yc=0, ys=slider_width + .4,
            z1=-2, z2=tower_h+eps
        );

        // pill hole
        cylx(z1=-5 - eps, z2=0, d=14);

        // space for disptopmount to rotate
        rfy() tz(tower_h + 3) ty(tower_l/2 + tower_wall_thick.y - 5) yzx() cylinder(h=4.2, center=true, r=3.1 * sqrt(2));

        // servo hole
        translate(servo_pos) {
            servo_cutout(screws=true, wirecut=1.5, z2=4+eps);
        }


        // photodiode/led holes
        rfy() tz(photodiode_height) {
            xzy() rz(45) cubex(z1=tower_l/2 - eps, z2=tower_l/2 + 12 + eps, xc=0, yc=0, xs=4, ys=4);
        }

        // cable holes
        rfy() {
            ty(-tower_l/2 - tower_wall_thick.y) {
                tz(-6)
                led_holder_base(.2);
            }
        }

        rfx() rfy() {
            tx(tower_w/2)
            ty(-tower_l/2 - 4) {
                cablehole(-5-eps, tower_h + eps);
                tx(3) cablehole(tower_h-5, tower_h + eps);
            }


        }

        // pcb mount holes
        if (pcbdrill)
        translate(pcb_offset)
        for (pt = pcb_mount_holes) {
            if (pt.x > 10 && abs(pt.x - 52.6) > 1) translate(pt) tz(-4) countersink_m2(4+eps, nut_length=0, expand=1.1);
        }

        translate(pcb_offset) {
            dispenser_component_holes(-4 - eps2, eps);
            translate(autopill_dispenser_single_pos_SW1 + [-4.5, -6.5]/2) {
                tz(-4 - eps + .6)
                render(10)
                dispenser_button_base(.2, -.6);
            }
        }

        // mount screw holes
        corners2(x1=dispbase_x1+4, x2=dispbase_x2-4, y1=dispbase_y1+4, y2=dispbase_y2-4) {
            cubex(x1=-5.2, y1=-5.2, x2=4+eps, y2=4+eps, z1=-4-eps, z2=eps, r1=3);
            tx(-10)
            tz(-10+eps)
            countersink_m2(10, expand=1, nut_length=2.5);
        }
    }

    // spring stopper
    tz(-3)
    tx(spring_x1-eps)
    yzx() {
        spring_hole(3, .3);
    }
}

// =======================================================================================
// sliders
// =======================================================================================

module dispenser_slider() {
    end_stop_pos = dispenser_slide_extra_r - 6;
    difference() {

        slider_groove_base(0, dispenser_slider_length, 0);

        cubex(
            x1=end_stop_pos, x2=dispenser_slider_length+eps2,
            yc=0, ys=slider_width+4+eps,
            z1=-.4, z2=1
        );

        if (!pincher)
        tx(dispenser_slide_extra_r + dispenser_pill_hole/2)
        cylx(z1=-5 - eps, z2=eps2, d=dispenser_pill_hole);
    }


    end_stop_x1 = pincher ? 0 : slider_width/2 - 1;
    color("blue")
    tx(end_stop_pos-2)
    yzx() linear_extrude(2) rfx() polygon([
        [end_stop_x1, 0],
        [slider_width/2, 0],
        [slider_width/2, 1],
        [slider_width/2+2, 3],
        [slider_width/2+2, 7],
        [end_stop_x1, 7],
    ]);


    tx(servo_base.x) {
        x1 = 3.15;
        x2 = 5;

        cubex(x1=-x1, x2=x1, yc=0, ys=slider_width, z1=0, z2=servo_gear_height-6.2);

        color("blue")
        rfx() {
            cubex(x1=x1, x2=x2, yc=0, ys=slider_width, z1=0, z2=servo_gear_height);
            tz(servo_gear_height)
            tx(x1) ty(-slider_width/2)
            multmatrix([
                [1, -.6, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1],
            ])
            yzx() lxpoly(x2-x1, points=[
                [0, 0],
                [-2.5, 0],
                [-2.5, -3],
                [0, -7],
            ]);
        }
    }

    if (!pincher) {
        tx(20)
        rz(90)
        mount_tower(0, 0);
    }
}

module pincher_slider() {
    lever_x = 4;
    x2 = 8;
    difference() {
        union() {
            slider_groove_base(-4, x2, 0);
            cubex(x1=lever_x + 2, x2=x2, yc=0, ys=slider_width, z1=0, z2=springw+2);
            cubex(x1=lever_x - 2, x2=0, yc=0, ys=slider_width, z1=0, z2=4);

            // spring holder
            tz(-3) tx(x2) {
                outer_dia = springw + 4;
                cubex(x1=0, x2=springl, yc=0, ys=outer_dia, z1=1, z2=outer_dia/2);
                tz(outer_dia/2)
                yzx() cylx(z1=0, z2=springl, d=outer_dia);
            }


        }
        cubex(x1=-10, x2=0,
            yc=0, ys=slider_width+4+eps,
        z1=-.4, z2=1);

        tz(-3)
        tx(x2)
        yzx()
        spring_hole(1+springl+eps);
    }
}

module servo_arm() {
    difference() {
        union() {
            // central hub
            cylx(-6, 1, d=12);

            // main arm
            linehull([[0, 0], [servo_arm_length, 0]]) {
                cylx(-6, -1, d=6);
            }


            ll = 10.4;

            p0 = [0, ll - 5];
            p1 = p0 - [0, ll];
            p2 = rotpt(p1, -14);
            p3 = p2 + [0, ll + 5];
            p4 = rotpt(p3, 5);
            //echo(p4);
            //color("green") translate(vxy(p4, 20)) cube(1, center=true);

            // side arm
            rz(5) ty(5) ty(ll) rz(-14) ty(-ll)
            intersection() {
                multmatrix([
                    [1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 1.2, 1, 0],
                    [0, 0, 0, 1],
                ])
                {
                    linehull([[0, -6], [-2.5, 2], [0, ll-5]]) {
                        cylx(-3, 1, d=3);
                    }
                    ty(ll-5)
                    cylx(-2, 10, d=3);
                }
                cubex(xc=0, xs=10, y1=-10, y2=20, z1=-6, z2=9.6);
            }
        }

        // screw hole
        cylx(-6-eps, -1+eps, d=3);

        // space for servo gear
        cylx(-1.6-eps, 1+eps, d=4.9);


        // screw holes
        tz(-1.2) {
            for (ang = [-25, 80, 150]) rz(ang)
            xzy() cylinder(h=7, d=2.3);
        }

        // cut through for preview
        *if ($preview)
        rz(30)
        cubex(xc=0, xs=2, yc=0, ys=14, zc=0, zs=10);
    }

    tx(servo_arm_length)
    cylx(-3, 5.6, d=6);

}

module preview_dispenser(flip=false, preview_pcb=preview_pcb) {
    if (preview_lower_hopper)
    rz(flip ? 180 : 0)
    tz(-ymin2 + tower_h) rx(90) {
        tz(-total_height/2) {
            lower_hopper_base();
            preview_lower_hopper_base();
        }
    }

    servo_ang = preview_slider * 90;
    slider_ofs = -(cos(servo_ang + 45) - cos(45)) * servo_arm_length;
    ang_ofs2 = 45+90+5;
    slider_ofs2 = -(cos(servo_ang + ang_ofs2) - cos(ang_ofs2)) * 10.4 + .6;

    true_ofs = ((preview_slider <= -0 && slider_ofs2 < slider_ofs) ? slider_ofs2 : slider_ofs) + preview_slider_nudge;

    tz(eps)
    tx(slider_x1 + true_ofs) {
        //color("blue", 0.5)
        //render(10)
        dispenser_slider();

        if (!pincher)
        color("green", 0.5)
        tx(20)
        render(10)
        dispenser_slider_upper();
    }

    if (pincher) {
        psx = sin(preview_pincher_rot) * (pincher_z - 2.5);// + (1 - cos(preview_pincher_rot)) * pincher_lever_thick/2;

        tx(psx - .5 + tower_w/2 + tower_wall_thick.x) tz(eps) {
            color("#e7a100", 0.5)
            render(10)
            pincher_slider();
        }
        tz(pincher_z)
        tx(pincher_x)
        ry(-preview_pincher_rot)
        tz(-pincher_z)
        tx(-pincher_x)
        color("#8e21c7", 0.5)
        render(10)
        pincher();
    }

    translate(servo_pos)
    //tz(-4 - 2.45)
    servo_model();

    tz(servo_gear_height+eps2)
    color("green", 0.5)
    render(10)
    translate(servo_pos)
    rz(-servo_ang)
    rz(135)
    rx(180)
    servo_arm();

    rz(flip ? 180 : 0)
    if (preview_disptopmount)
    tz(tower_h)
    color("blue", 0.5)
    render(10)
    disptopmount_pg_r();

    if (preview_dispenser_tube)
    tz(eps)
    color("#666666", 0.5)
    render(10)
    dispenser_tube_lower();

    if (preview_dispenser_tube)
    tz(eps2)
    color("#006666", 0.5)
    render(10)
    dispenser_tube_upper();


    if (preview_pcb) {
        tz(-4 - 1.6 - 2.0)
        translate(pcb_offset) {
            pcb_model(preview_pcb_traces);

            *translate(autopill_dispenser_single_pos_SW1 + [-4.5, -6.5]/2)
            tz(1.6 + 2 + .6)
            render(10)
            dispenser_button();

        }
    }

    if (preview_wirebox) {
        tz(-4 - dispenser_wirebox_height) {
            color("blue", 0.5)
            render(10)
            dispenser_wirebox();
            preview_dispenser_wirebox(pcb=false, servo=false);
        }
    }
}

module servo_cutout(screws=false, wirecut=0, expand=0, xe=0, z1=-5-eps, z2=eps) {
    servo_l = servo_l1 + servo_l2;
    tx(-servo_l1) {
        cubex(ys=12.2+2*expand, x1=-expand, x2=servo_l + expand + xe, yc=0, z1=z1, z2=z2);

        if (wirecut)
        tx(servo_l)
        cubex(ys=7, x1=-eps, x2=wirecut, yc=0, z1=z1, z2=z2);

        if (screws)
        for (xx = [-2, servo_l + 2])
        tx(xx) cylx(z1=z1, z2=z2, d=1.5);
    }

}

module dispenser_component_holes(z1, z2) {
    tz(-1.6) {
        component_xform(autopill_dispenser_single_data_D3)
        tx(1.27)
        cylx(z1, z2, d=7);

        component_xform(autopill_dispenser_single_data_SW1)
        translate([6.5/2, -2.25, 0])
        cubex(
            xc = 0, yc = 0,
            xs = 8, ys = 7,
            z1 = z1, z2=z2
        );
    }

}

module dispenser_pcb_spacer(front=false) {
    x0 = 97.8;
    x1 = 79.5;
    x2 = 38;
    x3 = 21.5;
    x4 = 3.2;

    y1 = 59.3;
    y2 = 3.1;
    y3 = 25;
    y4 = y1 - 6;

    if (front) {
        tx(x0) {
            difference() {
                py1 = 10;
                py2 = 52;
                cubex(
                    x1 = -2.6, x2 = 3.0,
                    y1=y2 + 3, y2 = y1 - 3,
                    z1=-10, z2=2,
                    r2=5.8/2, r4=5.8/2
                );

                cubex(
                    x1 = -1, x2 = 1,
                    y1=py1, y2=py2,
                    z1=-10-eps, z2=-1.6
                );

                for (i = [0 : 5]) ty(lerp(py1, py2, i/5))
                cubex(
                    x1 = -3, x2 = 1,
                    yc=0, ys=1.4,
                    z1=-10-eps, z2=-1.6
                );
            }

            *tx(0.6)
            for (i = [0 : 6]) ty(lerp(15 + 2, 46 - 2, i/6)) {
                cylx(z1=-8, z2=0, d=2);
            }

        }
    } else {
        *tz(-1.6) {
            component_xform(autopill_dispenser_single_data_D1)
            tx(1.27) rz(180) ty(2) led_holder();
            component_xform(autopill_dispenser_single_data_D2)
            tx(1.27) rz(0) ty(2) led_holder();
        }
    }

    difference() {
        union() {
            linear_extrude(2.0, convexity=6) {
                offset(-5.8/2)
                offset(5.8) {
                    if (front) {
                        for (yofs = [0, 1])
                        linehull([
                            [x1, y1],
                            [x1, y1 - 6 - yofs*8],
                            [x0, y1 - 6 - yofs*8],
                            [x0, y2 + 6 + yofs*5],
                            [x1, y2 + 6 + yofs*5],
                            [x1, y2],
                        ]) circle(d=0.1, $fn=4);

                    } else {
                        linehull([
                            [x2, y2],
                            [x2, y3],
                            [x4, y3],
                            [x4, y4],
                            [x3, y4],
                            [x3, y1],

                        ]) circle(d=0.1, $fn=4);
                        *linehull([
                            //[x1, y1],
                            [x2, y1],
                            [x2, y2],
                            //[x1, y2],
                        ]) circle(d=0.1, $fn=4);
                    }
                }
            }
        }

        dispenser_component_holes(-1, 3);

        iter_drill_holes(autopill_dispenser_dual_drill, min_hole_size=0.5) {
            if ($hole_size < 3.1) {
                cylx(-eps, 2 + eps, d=max($hole_size + 0.2, 2.6), $fn=16);
            }
        }
    }
}

module dispenser_pcb_spacer_rear() {
    dispenser_pcb_spacer(front=false);
}

module dispenser_pcb_spacer_front() {
    dispenser_pcb_spacer(front=true);
}

module idc_cutout() {
    hh = 10.4;

    let(xs=6.4, ys=18, zofs=2.2, xs2=2.54 * 2 + .2, ys2=2.54 * 5 + .2)
    translate(1.27 * [1, -4]) {
        cubex(
            xc = 0, xs = xs,
            yc = 0, ys = ys,
            z1 = hh + eps, z2 = zofs
        );

        cubex(
            xc = 0, xs = xs2,
            yc = 0, ys = ys2,
            z1 = hh + eps, z2 = -eps
        );

        tz(zofs)
        yzx() lxpoly(xs, center=true, points=[
            [-ys2/2, eps],
            [-ys2/2 + zofs, -zofs-eps],
            [ys2/2 - zofs, -zofs-eps],
            [ys2/2, eps],
        ]);

        cubex(
            x1 = 0, x2=-xs/2 - 1,
            yc = 0, ys = 4.5,
            z1 = hh + eps, z2 = -eps
        );

        let(ex=2)
        tz(hh + eps - 1)
        frustum(-xs/2, -ys/2, xs/2, ys/2, 2, xe=ex, ye=ex);

        cubex(
            xc=0, xs=xs + 4,
            yc=0, ys=ys + 4,
            z1 = hh + 1, z2 = hh + 5
        );
    }

}

module dispenser_wirebox() {
    hh = dispenser_wirebox_height;
    pcb_z2 = hh - 2.0;
    pcb_z1 = pcb_z2 - 1.6;

    idc16_ofs = 3.5 * 2.54;
    idc10_ofs = 2 * 2.54;

    difference() {
        union() {
            wirebox_base(
                x1=dispbase_x1+2,
                x2=dispbase_x2-2,
                y1=dispbase_y1+2,
                y2=dispbase_y2-2,
                wt=2,
                bt=1,
                hh=hh,
                taper_ofs=20,
                ti_height=6.0
            );

            // pill pillar
            cylx(z1=0, z2=hh, d=dispenser_pcb_pill_hole - .5);

            intersection() {
                cylx(-eps, pcb_z1, d=24);
                cubex(x1=0, x2=30, yc=0, ys=30, z1=-eps, z2=pcb_z1);
            }


            ty(pcb_offset.y + autopill_dispenser_single_pos_J1.y - idc10_ofs)
            tx(dispbase_x1)
            xzy()
            linear_extrude(24, center=true, convexity=2) polygon([
                [0, 0],
                [0, pcb_z1],
                [18.5, pcb_z1],
                [18.5, 5],
                [18.5 + 5, 0],
            ]);
            *cubex(
                zc=0, zs=24,
                y1=0, y2=pcb_z1,
                x1=0, xs=18.5
                //r2=3, r1=3
            );
        }

        if (preview_cutout) {
            cubex(x1=dispbase_x2-2-eps, x2=dispbase_x2+eps, y1=dispbase_y1, y2=dispbase_y2, z1=0, z2=hh+eps);
            cubex(x1=dispbase_x1-eps, x2=dispbase_x1+2+eps, y1=dispbase_y1, y2=dispbase_y2, z1=0, z2=hh+eps);
            cubex(x1=dispbase_x1, x2=dispbase_x2, y1=dispbase_y1-eps, y2=dispbase_y1+10+eps, z1=0, z2=hh+eps);
            cubex(x1=dispbase_x1, x2=dispbase_x2, y1=dispbase_y2-10-eps, y2=dispbase_y2+eps, z1=0, z2=hh+eps);
        }

        // pill hole
        cylx(z1=-4-eps, z2=hh+eps, d=dispenser_pill_hole+2);

        // mounting holes
        corners2(x1=dispbase_x1+4, x2=dispbase_x2-4, y1=dispbase_y1+4, y2=dispbase_y2-4) {
            cylx(z1=-4-eps, z2=hh+4+eps, d=screw_diameter_m3+.5, ext=eps);
            cylx(z1=hh+1, z2=hh+4+eps, d=6.1, ext=eps);
        }

        // sacraficial cut for seam
        tx(dispbase_x1)
        cubex(xc=0, yc=0, xs=1, ys=.5, z1=-4-eps, z2=hh + eps);

        // connector cutout
        if (dispenser_connector_vertical) {
            tz(pcb_z1)
            translate(pcb_offset + autopill_dispenser_single_pos_J1)
            ry(180)
            idc_cutout();

            translate(pcb_offset + autopill_dispenser_single_pos_R5)
            cubex(
                xc=0, xs=4,
                z1=pcb_z1 - 1.4, z2=pcb_z1+eps,
                yc=0, ys=2.5
            );
        } else {
            ty(pcb_offset.y + autopill_dispenser_single_pos_J1.y - idc16_ofs)
            tx(dispbase_x1)
            yzx() {
                cubex(
                    xc=0, xs=14,
                    y1=pcb_z1 - 6.4, y2=pcb_z1+eps,
                    z1=-eps, zs=17.5+eps2
                );
                cubex(
                    xc=0, xs=18,
                    y1=pcb_z1 - 6.4, y2=pcb_z1+eps,
                    z1=-eps, zs=8.4+eps2
                );
                cubex(
                    xc=0, xs=4.5,
                    y1=pcb_z1 - 7.4, y2=pcb_z1+eps,
                    z1=-eps, zs=8.4
                );

                // space for that one damn resistor i forgot about
                cubex(
                    x1=7-eps, x2=10,
                    y1=pcb_z1 - 1.4, y2=pcb_z1+eps,
                    z1=11, z2=16
                );
            }
        }
    }
}

module preview_dispenser_wirebox(pcb=preview_pcb, servo=true) {
    if (pcb) {
        tz(dispenser_wirebox_height - 2.0 - 1.6 + eps)
        translate(pcb_offset)
        pcb_model(preview_pcb_traces);
    }

    if (servo) {
        translate(servo_pos)
        tz(dispenser_wirebox_height + 4)
        servo_model();
    }
}

module cablehole(z1, z2) {
    cubex(x1=-1, x2=2.5, ys=6.2, yc=0, z1=z1, z2=z2);
}

module led_pinhole(z1, z2) {
    spc = 2.54 / 2;
    rfy() ty(spc) cylx(z1=z1, z2=z2, d=1.2);
}

module mount_tower(z1, z2, inv=false) {
    difference() {
        cubex(
            xc=0, xs=14,
            yc=0, ys=10,
            z1=z1, z2=z2+7, r3=2, r4=2
        );
        cubex(
            xc=0, xs=4.2,
            yc=0, ys=10+eps,
            z1=z2, z2=z2+7+eps
        );
        tz(z2+3) yzx() tz(-7) {
            countersink_m2(14, expand=1, nut_length=0, inv=inv);
            tz(1)
            linehull([[0, 0], [0, 5]])
            rz(30)
            hexnut_hole(h=2.5, d=nut_diameter_m2);
        }
    }
}

module slider_groove_base(sgroove_x1, sgroove_x2, fc) {
    sgroove_y1 = (slider_width + fc) / 2;
    sgroove_y2 = sgroove_y1 + 1.5;
    yzx() tz(sgroove_x1-eps) linear_extrude(sgroove_x2 - sgroove_x1 + eps)
    polygon([[-sgroove_y1, eps],
        [sgroove_y1, eps],
        [sgroove_y2, -2],
        [-sgroove_y2, -2],
    ]);

}

module interface_spacer(r=2, n=5, hh=0.8) {
    h2 = hh + 2.4 + 2.54*(r-1);
    difference() {
        union() {
            // bottom support piece
            cubex(
                x1=-1.27, x2=1.27 + 2.54*(n-1),
                y1=-2.54, y2=3.8 + 2.54*(r-1),
                z1=0, z2=hh
            );

            // top support piece
            cubex(
                x1=-1.27, x2=1.27 + 2.54*(n-1),
                y1=-2.54, y2=1.8 + 2.54*(r-1),
                z1=0, z2=h2
            );
        }

        pl = 1.6;
        pw = 1.1;

        for (yy = [0 : r-1], xx=[0 : n-1]) {
            ty(yy*2.54) tx(xx * 2.54) {
                // vertical pin space
                cubex(z1=-1, z2=h2 + 1, xc=0, yc=0, xs=pw, ys=pl);

                // horiz pin space
                cubex(z1=.2 + hh + (r-1-yy) * 2.54, z2=h2+1, xc=0, xs=pw, y1=0, y2=10);
            }
        }
    }
}

module spring_hole(length, fc=0) {
    sz2 = springw * sqrt(2)/2 - fc;

    sw2 = springw/2 - fc;

    linear_extrude(length)
    polygon([
        [-sw2, 0],
        [sw2, 0],

        [sw2, sz2],
        [0, sz2 + sw2],
        [-sw2, sz2],
    ]);
}

module led_holder_base(fc=0) {
    xs = 7.2 + 2*fc;
    led_h = 11.4;

    xzy() tz(-fc) lxpoly(8 + 2*fc, points=[
        [-xs/2, 0],
        [-xs/2, led_h + 2.5 + fc],
        [0, led_h + 2.5 + xs/2 + fc],
        [xs/2, led_h + 2.5 + fc],
        [xs/2, 0],
    ]);
    cubex(
        xc=0, xs=xs,
        y1=fc, y2=-5.8 - fc,
        z1=-fc, z2=6 + fc

    );
}
module led_holder() {
    xs = 7.2;
    led_h = 11.4;
    difference() {
        led_holder_base();

        tz(led_h) xzy() cylx(-eps, 8+eps, d=5.5);

        ty(-2)
        rfx() tx(1.27) cubex(z1=-eps, z2=led_h, xc=0, y1=-10, xs=1, y2=.6);
    }
}

module md_spacer(h=1, straight=true) {
    expand = 1.2;
    size = [25.1, 21.0];
    origin = [29.4, 29.0] - [1.7, 2];
    shift = [.6, -.5];

    difference() {
        cubex(
            x1=-expand, x2=size.x+expand,
            y1=-expand, y2=size.y+expand,
            z1=0, z2=h+1
        );
        cubex(
            x1=0, x2=size.x,
            y1=0, y2=size.y,
            z1=h-eps, z2=h+1+eps
        );
        tz(h-eps) corners2(0, 0, size.x, size.y) {
            rz(45) cubex(
                x1=-2, x2=5,
                yc=0, ys=2,
                z1=0, z2=1+eps2
            );
        }
        for (i = [0 : 9]) {
            pt2 = md_spacer_layer_2[i] - origin;
            pt1 = straight ? pt2 : md_spacer_layer_1[i] - origin + shift;
            pt1f = [pt1.x, size.y - pt1.y];
            pt2f = [pt2.x, size.y - pt2.y];
            color("green")
            translate(pt1f) skew_by(pt2f-pt1f, h)
            cubex(
                xc=0, yc=0,
                xs=1.4, ys=1.4,
                z1=-eps, z2=h+eps
            );
            *cylx(-eps, h+eps, d=1.2);
        }
    }
}

module wire_holder_l() {
    difference() {
        union() {
            cubex(
                xc=0, y1=-3.5,
                xs=7, y2=3.5,
                z1=0, z2=3,
                r1=3.5, r2=3.5, r4=3.5
            );

            cubex(
                x1=-3.5, x2=0,
                y1=3.5, y2=16.9,
                z1=0, z2=8,
            );
        }

        for (yy = [5.6 : 3.0 : 30]) ty(yy) {
            cubex(
                xc=0, xs=7 + eps,
                yc=0, ys=1.4,
                z1=2, z2=8 + eps,
            );

        }


        tz(1.0)
        hexnut_hole(d=nut_diameter_m2, h=3);
        cylx(-eps, 3, d=screw_diameter_m2);
    }
}

module wire_holder_r() {
    sy(-1) wire_holder_l();
}

module dispenser_button_base(fc=0, z1=0, z2=0) {
    d1 = 11;
    cylx(z2, 5, d=d1 + 2*fc);
    cylx(z2, z2+1, d1=d1 + 2 + 2*fc, d2=d1 + 2*fc);
    if (z1 != z2) cylx(z1, z2, d=d1 + 2 + 2*fc);
}

module dispenser_button() {
    zofs = -2;
    difference() {
        union() {
            dispenser_button_base(z1=1.2, z2=1.2, fc=-.2);
            *tz(zofs) {
                cubex(
                    xc=0, yc=0,
                    xs = 10, ys=10,
                    z1=1, z2=-zofs,
                    r=1
                );
            }
        }
        tz(zofs) {
            cubex(
                xc=0, yc=0,
                xs = 7.6, ys=7.6,
                z1=0, z2=pushbutton_h,
                r=1.75
            );
        }

    }
}
