module pillwheel() {
    wr = wheel_rad;
    a = .4;

    difference() {
        union() {
            color("gold") {
                difference() {
                    cylx(z1=4 + .8, z2=total_height, r=wr);
                    cylx(z1=4, z2=bearing_top_z + 3, r=wr-4);

                }
            }

            // shaft
            cylx(z1=bearing_top_z, zs=3, d=bearing_inner + 2);
            cylx(z1=bearing_top_z - bearing_thick, z2=bearing_top_z, d=bearing_inner);

            tz(bearing_top_z - bearing_thick - beltwheel_bearing_spacing + .4)
            hexnut_hole(h=beltwheel_bearing_spacing-.4, d=bearing_inner*.6);
        }

        cwidth = 4;

        difference() {
            pillwheel_flex_cover(z1 = 4, inner_rad = wheel_rad - 1.0, outer_rad = wheel_rad + 1, gpi=0.45);
            *cylx(z1 - 2, z1, r1 = wheel_rad, r2 = wheel_rad - 1.8);
        }


        //total_pw_height = pri_height + lid_thick + pri_depth + 6 + eps2;
        cylx(bearing_top_z - bearing_thick - beltwheel_bearing_spacing - eps, total_height + eps, d=screw_diameter_m2);
        tz(4) {
            hexnut_hole(h=total_height+eps, d=nut_diameter_m2);
        }
    }


    tz(4-.2)
    bridge_support(6);
}

module preview_pillwheel() {
    if (preview_pillwheel) {
        tz(bearing_top_z - bearing_thick - beltwheel_bearing_spacing)
        color("blue", 0.5)
        render(10)
        beltwheel();
    }

    tz(bearing_top_z - bearing_thick)
    color("#444488", 0.5)
    render(10)
    bearing_model();

    color("#222222")
    tz(total_height)
    render(10)
    wheel_spiral();

    color("#222222")
    render(10)
    pillwheel_flex_cover();

}

module pillwheel_flex_cover(
    inner_rad = wheel_rad - 1, outer_rad = wheel_rad,
    z1 = 5, z2 = total_height - 1, gpi=0.55
) {
    let()
    difference() {
        tz(z1) linear_extrude(z2 - z1, convexity=10) difference() {
            polygon(squaregear(outer_rad, outer_rad + .3, 96, 0.5));
            polygon(squaregear(inner_rad - 1, inner_rad, 48, gpi));
        }
    }
}

module pillwheel_poker() {
    wr = wheel_rad;
    a = .4;

    inner_z = total_height/2 - poker_height/2 + 1;

    difference() {
        union() {
            color("gold") {
                difference() {
                    cylx(z1=4 + .8, z2=total_height, r=wr);
                    cylx(z1=4, z2=inner_z, r=wr-4);

                }
            }

            // shaft
            z4 = inner_z;
            z3 = bearing_top_z;
            z2 = bearing_top_z - bearing_thick + 1;
            z1 = bearing_top_z - bearing_thick - beltwheel_bearing_spacing;


            *tz(z1) hexnut_hole(h=z2-z1, d=bearing_inner*.6);
            cylx(z1=z1, z2=z2, d=bearing_inner - 2);
            cylx(z1=z2, z2=z3, d=bearing_inner);
            cylx(z1=z3, z2=z4, d=bearing_inner + 1);
            tz(z1) rx(180) tz(-.2) planetgear_interface(.2);
        }

        // poker holes
        for (i = [0 : poker_spokes - 1]) rz(i * 360 / poker_spokes) {
            cubex(
                x1 = bearing_inner/2 + 1, x2 = wheel_rad + 1,
                yc = 0, ys = poker_width + .3,
                zc = total_height/2, zs = poker_height
            );
        }

        tz(inner_z - 1 - eps)
        linear_extrude(poker_height + eps, convexity=2) difference() {
            circle(d=bearing_inner + 6);
            circle(d=bearing_inner + 1);
        }


        cwidth = 4;

        //total_pw_height = pri_height + lid_thick + pri_depth + 6 + eps2;
        cylx(bearing_top_z - bearing_thick - beltwheel_bearing_spacing - 6 - eps, total_height + eps, d=screw_diameter_m2);
        tz(4) {
            hexnut_hole(h=total_height+eps, d=nut_diameter_m2);
        }
    }


    tz(4-.2)
    bridge_support(6);
}

module preview_pillwheel_poker() {
    tz(total_height/2 + poker_height/2) {
        //tx(bearing_outer/2 + 1)
        tx(cam_rad_base)
        rx(180)
        render(10)
        pillwheel_poker_cam_follower();


        color("#777777", 0.5)
        rx(180)
        render(10)
        pillwheel_insert();

    }

    *tz(bearing_top_z - bearing_thick)
    color("#444488", 0.5)
    render(10)
    bearing_model();


}

module pillwheel_insert() {
    slh = total_height/2 + poker_height/2 - bearing_top_z;

    render(10)
    difference() {
        union() {
            cylinder(h=slh, d=bearing_inner + 3.2);
            linear_extrude(poker_height, convexity=2) {
                circle(d=bearing_inner + 5.6);
                for (i = [0 : poker_spokes - 1]) rz(i * 360 / poker_spokes) {
                    hull() {
                        circle(d=poker_width);
                        tx(cam_rad_base - poker_width) circle(d=poker_width);
                    }
                }

            }

        }
        cylinder(h=slh, d=bearing_inner + 1.5);
    }
}

module pillwheel_poker_cam_follower() {
    linear_extrude(5.2) {
        linehull([[-1 + poker_width/2 + .1 , 0], [1 - poker_width/2 - .1, 0]]) circle(d=poker_width + .12);
    }
    linear_extrude(poker_height - .4) {
        linehull([[0, 0], [wheel_rad - cam_rad_base - poker_width/2, 0]]) circle(d=poker_width + .12);
    }
}

function rectcoords(pts) = [for (v=pts) [cos(v.y), sin(v.y)] * v.x];

module wheel_spiral() {
    polar_pts_inner = [for(v = [0 : .1 : 30]) [v+nut_diameter_m3/2-1, v*75]];
    polar_pts_outer = [for(v = polar_pts_inner) v + [1, 0]];
    intersection() {
        difference() {
            cylinder(1, r=wheel_rad, center=true);
            tz(-eps)
            hexnut_hole(h=1, d=nut_diameter_m3);
        }
        linear_extrude(.2, convexity=10)
        polygon([
            each rectcoords(polar_pts_inner),
            each reverse(rectcoords(polar_pts_outer))
        ]);

    }
}

module beltwheel_base(r, h=5, xh=0, s=5/4) {
    r2 = r + (h - xh) / 2 / s;
    tz(h/2) rfz() {
        cylx(z1=-h/2, z2=-xh/2, r1=r2, r2=r);
        cylx(z1=0, z2=xh/2, r=r);
    }


    *cylx(z1=1.6, z2=2, r1=r+2, r2=r);
    *cylx(z1=1.6, z2=3, r=r);
    *cylx(z1=3, z2=5, r1=r, r2=r + 2);
}

module beltwheel(ext=0) {
    difference() {
        union() {
            tz(-5)
            beltwheel_base(beltwheel_rad, xh=1.3);
            cylx(0, beltwheel_bearing_spacing + ext + .2, d=bearing_inner + 2);
        }
        tz(ext-eps)
        hexnut_hole(h=beltwheel_bearing_spacing + .2 + eps2, d=bearing_inner*.6 + .4);
        tz(-5)
        countersink_m2(100, expand=1.1, inv=1, head_extra=ext, nut_length=2.5);
    }
}

module motorbeltwheel() {
    difference() {
        union() {
            beltwheel_base(2.4, xh=1.3);
            //cylx(0, 1.2, r=3);
            cylx(0, 7, r=2.4);
        }
        cylx(-eps, 10, r=1.3);
    }
}


module pillwheel_holder_screws(d, angs, rad) {
    r1 = bearing_outer/2 + 1;
    tz(2)
    rz(poker_cam_ang) {
        for (ang = angs) rz(ang) {

            yzx() {
                if (is_undef(d)) {
                    tz(r1) countersink_m2(rad - r1 + 2, head_extra=2);
                } else {
                    cylx(r1, rad, d=d);
                }
            }
            //
        }
    }
}

module pillwheel_holder_outer(fc=0) {
    z2 = total_height/2 - poker_height/2 - .6;
    main_r = wheel_rad - 4.6;
    bevel = 2;

    rz(poker_cam_ang) {
        cylx(0, bevel, r1 = main_r - bevel, r2 = main_r);
        cylx(bevel, z2, r = main_r);
        for (ang = [0, 180, 45, -45, 90, -90]) rz(ang) {
            cubex(
                x1 = wheel_rad - 12, x2 = wheel_rad - 3 + fc,
                yc = 0, ys = 5 + 2*fc,
                z1 = 0, z2 = 4
            );
        }
    }
}

module pillwheel_holder() {
    z2 = total_height/2 - poker_height/2 - .2;
    difference() {
        pillwheel_holder_outer();
        cylx(1, z2 + eps, d=bearing_outer);
        cylx(bearing_thick, z2 + eps, d=bearing_outer + .8);

        rz(poker_cam_ang) {
            tz(z2 - 3) linear_extrude(3 + eps, convexity=2)
            linehull([for(i = [0 : 1 : 360]) rotpt([1, 0] * (cam_rad(i)), i) ]) circle(r=1.2);
        }

        // shaft passthrough
        tz(-eps) bearing_cutout(1+eps2, bearing_outer);

        // small cut to ensure the seam is on the backside
        rz(-45)
        cubex(
            x1 = 0, x2 = wheel_rad,
            yc = 0, ys = .1,
            z1 = z2 - 3, z2 = z2 + eps
        );

        pillwheel_holder_screws(2.3, [0, 45, -45, 90, -90, 180], wheel_rad + 20);
    }
}

module preview_pillwheel_holder() {
    *tz(1+eps)
    color("#444488", 0.5)
    render(10)
    bearing_model();
}
