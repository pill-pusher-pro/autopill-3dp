module crossbar_base(tw) {
    barh = 7;
    mainh = 20;

    cubex(
        xc=0, xs=4,
        y1=-tw, y2=tw,
        z1=0, z2=barh
    );

    rfy() {
        ty(tw)
        difference() {
            yzx() linear_extrude(4, center=true)
            polygon([[0, 0], [6, 0], [8, 2.5], [8, 7], [0, 7]]);
            *cubex(
                xc=0, xs=4,
                y1=0, y2=8,
                z1=0, z2=7
            );
            tz(3) ty(5) yzx() cylinder(h=10, center=true, d=screw_diameter_m2);
        }
    }
}

module motormount_base(motorh=15, r1=0, r2=0, r3=0, r4=0, sidesplit=0, splitw=5) {
    difference() {
        cubex(
            x1=0, x2=motormount_l,
            yc=0, ys=motormount_w,
            z1=0, z2=motorh,
            r1=r1, r2=r2, r3=r3, r4=r4
        );
        tz(-eps)
        tx(12) {
            intersection() {
                cylinder(h=motorh-2, d=20);
                cubex(
                    xc=0, xs=15,
                    yc=0, ys=20,
                    z1=0, z2=motorh-2
                );

            }
            cylx(z1=motorh-2-eps2, z2=motorh+eps2, d=8.6);
            //if (sidesplit != 0) {
            intersection() {
                rz(sidesplit ? 90 : 0)
                cubex(xc=0, yc=0, xs=15, ys=6.6, z1=motorh-2-eps2, z2=motorh+eps2);
                rz(45)
                cubex(xc=0, yc=0, xs=8.6, ys=8.6, z1=motorh-2-eps2, z2=motorh+eps2);
            }
            //}

        }
        if (sidesplit) {
            cubex(
                y1=-5, y2=-24,
                xc=12, xs=splitw,
                z1=-eps, z2=motorh-2
            );

        } else {
            cubex(
                x1=12, x2=24,
                yc=0, ys=splitw,
                z1=-eps, z2=motorh-2
            );
        }

    }
}

module motormount() {
    motorh = 15;
    difference() {
        motormount_base(motorh, r4=4);
        tz(motorh/2)
        yzx() {
            sz(-1)
            tz(-4.5-eps)
            countersink_m2(10, nut_length=2.5);
            //cylx(z1=12, z2=30, d=4);
        }
    }
}

module disptopmount_base(motors=[0, 1], mofs=[beltwheel_offset, 0], msize = [16, 16]) {
    barh = 0;
    hh = motormount_w + 4;
    mainh = hh - barh;

    tw = tower_l;
    difference() {
        union() {
            crossbar_base(tower_l/2);

            // outer fitting
            cubex(
                xc=0, xs=28,
                yc=0, ys=24,
                z1=0, z2=mainh,
                r1=2, r2=2
            );
        }

        // fitting for hopper
        cubex(
            xc=0, yc=0,
            ys=total_height + .4,
            xs=total_height + .4,
            z1=-eps, z2=mainh+eps
        );

        // space for left belt wheel

        cubex(
            x1=-26,
            x2=26,
            y1=total_height/2,
            ys=5,
            z1=hh - 12, z2=hh+eps
        );

        // main pill tube
        //cylx(z1=0, z2=2, d=dispenser_pill_hole, ext=eps);

    }
    for (xx = motors) sx(xx ? -1 : 1) {
        mh = msize[xx];
        zofs = 0;
        ty(mofs[xx])
        tx(24/2)
        tz(motormount_w/2) {
            rx(-90)
            tz(-mh + total_height/2) {
                motormount_base(mh, r1=0, r2=0, r3=0, r4=0, sidesplit=1, splitw=10);
            }
        }

    }
}

module disptopmount() {
    disptopmount_base([0, 1]);
}

module disptopmount_pg_l() {
    sx(-1) disptopmount_pg_r();
}

module disptopmount_pg_r() {
    difference() {
        disptopmount_base([0], [beltwheel_offset + 10], [26]);
        cubex(
            x1 = 12 + motormount_l/2 - 4.5, xs = motormount_l,
            y1=-total_height/2, y2=total_height/2,
            z1=-1, z2=motormount_w+eps
        );
    }
}
