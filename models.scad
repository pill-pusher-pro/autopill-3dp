module pill(d=pill_width) {
    ll=pill_len - d;
    tz(pill_len/2)
    color("blue") {
        cylx(-ll/2, ll/2, d=d);
        tz(ll/2) sphere(d=d);
        tz(-ll/2) sphere(d=d);
    }
}

module bearing_model() {
    difference() {
        cylx(0, bearing_thick, d=bearing_outer - .4);
        cylx(-eps, bearing_thick + eps, d=bearing_inner + .4);
    }
}

module pcb_model(traces=false) {
    if (dual_dispenser_pcb) {
        autopill_dispenser_dual_pcb_model(traces=traces);
    } else {
        autopill_dispenser_single_pcb_model(traces=traces);

    }
    if ($preview) {
        if (preview_pcb_spacer) {
            tz(1.6)
            color("#444444")
            render(10)
            dispenser_pcb_spacer();

            tz(1.6)
            color("#555555")
            render(10)
            dispenser_pcb_spacer(front=true);
        }

        dispenser_pcb_components(idc_plug=false);
    }
}

module pcb_model_main(traces=preview_pcb_traces) {
    autopill_main_pcb_model(traces=traces);
    if ($preview) {
        iter_objects(autopill_main_objects) {
            if ($ref == "U1") {
                // DNP
            } else if ($ref == "C1") {
                // DNP
            } else if ($ref == "C2") {
                // DNPx
            } else if ($ref == "D1") {
                // DNP
            } else if ($ref == "A1") {
                *tz(-1.6) sz(-1) {
                    translate(2.54 * [11, 1])
                    rz(90)
                    pinheader_vert(n=8, r=1);


                    translate(2.54 * [20, 1])
                    rz(90)
                    pinheader_vert(n=8, r=1);

                    translate(2.54 * [29, 1])
                    rz(90)
                    pinheader_vert(n=8, r=1);

                    translate(2.54 * [38, 3])
                    rz(180)
                    pinheader_vert(n=14, r=2);

                    translate(2.54 * [7.4, 20])
                    rz(90)
                    pinheader_vert(n=10, r=1);

                    translate(2.54 * [18, 20])
                    rz(90)
                    pinheader_vert(n=8, r=1);

                    translate(2.54 * [27, 20])
                    rz(90)
                    pinheader_vert(n=6, r=1);
                }

            } else {
                autopill_kicad_component();
            }
        }
    }
}

module pcb_model_led_split() {
    pts = [
        [20, -30],
        [49.5, -30],
        [49.5, -58],
        [64.75, -58],
        [64.75, -67],
        [20, -67],
    ];
    translate(-autopill_led_buttons_origin) {
        polygon(pts);
    }
}

module pcb_model_led_buttons(traces=false, components=true) {
    autopill_led_buttons_pcb_model(preview_pcb_traces, $fn=15, min_hole_size=$preview ? 0 : 1.0, hole_size_offset = $preview ? 0 : 0.5);
    if ($preview && components) {
        iter_objects(autopill_led_buttons_objects) {
            if ($cpackage == "LED_D5.0mm") {
                $component_zofs = 0;
                $component_bend = 0;
                if ($ref == "D1") {
                    led_5mm("red");
                } else if ($ref == "D2") {
                    led_5mm("#cccc00");
                } else if ($ref == "D3") {
                    led_5mm("#00aa00");
                } else if ($ref == "D4") {
                    led_5mm("#0000aa");
                } else {
                    led_5mm();
                }
            } else {
                kicad_component();
            }
        }
    }
}

module pcb_model_led(traces=false, components=true) {
    autopill_led_pcb_model(preview_pcb_traces, $fn=15, min_hole_size=$preview ? 0 : 1.0, hole_size_offset = $preview ? 0 : 0.5);
    if ($preview && components) {
        iter_objects(autopill_led_buttons_objects) {
            if ($cpackage == "LED_D5.0mm") {
                $component_zofs = 0;
                $component_bend = 0;
                if ($ref == "D1") {
                    led_5mm("red");
                } else if ($ref == "D2") {
                    led_5mm("#cccc00");
                } else if ($ref == "D3") {
                    led_5mm("#00aa00");
                } else if ($ref == "D4") {
                    led_5mm("#0000aa");
                } else {
                    led_5mm();
                }
            } else if ($cpackage == "SW_PUSH_6mm") {
            } else if ($ref == "J3") {
            } else {
                kicad_component();
            }
        }
    }
}

module pcb_model_buttons(traces=false, components=true) {
    autopill_button_pcb_model(preview_pcb_traces, $fn=15, min_hole_size=$preview ? 0 : 1.0, hole_size_offset = $preview ? 0 : 0.5);
    if ($preview && components) {
        iter_objects(autopill_led_buttons_objects) {
            if ($cpackage == "LED_D5.0mm") {
            } else if ($cpackage == "smd_r") {
            } else if ($ref == "J1") {
            } else if ($ref == "J2") {
            } else {
                kicad_component();
            }
        }
    }
}

$component_zofs = 0;
module led_5mm(color="green") {
    tz($component_zofs)
    rx($component_bend) {
        tx(1.27)
        color(color, 0.5)
        tz(1)
        render(2)
        intersection() {
            union() {
                cylx(0, 0.4, d=5.8);
                cylx(0, 6, d=5);
                tz(6) sphere(d=5);
            }
            cubex(x1=-3, y1=-3, x2=2.6, y2=3, z1=0, z2=20);
        }
        for (p = [0 : 1]) tx(p*2.54) {
            cylx(0, 1, d=0.4);
            sphere(d=0.4);
        }
    }
    for (p = [0 : 1]) tx(p*2.54) {
        cylx(-2, $component_zofs, d=0.4);

    }
}

module autopill_kicad_component() {
    if ($cpackage == "BarrelJack_Horizontal_2GND") {
        barrel_jack();
    } else {
        kicad_component();
    }
}

module dispenser_pcb_components(idc_plug=preview_idc, idc_w=5) {
    iter_objects(autopill_dispenser_objects) {
        $mosfet_bend = -50;
        if ($cpackage == "LED_D5.0mm") {
            if ($ref == "D1") {
                $component_zofs = 11.4;
                $component_bend = 90;
                color("#333333")
                tx(1.27) rz(180) ty(2) led_holder();
                led_5mm("#bbbbbb");
            } else if ($ref == "D2") {
                $component_zofs = 11.4;
                $component_bend = -90;
                color("#333333")
                tx(1.27) ty(2) led_holder();
                led_5mm("#222222");
            } else {
                $component_zofs = 0;
                $component_bend = 0;
                led_5mm();
            }
        } else {
            if ($ref == "J1") {
                if (dispenser_connector_vertical) {
                    if (idc_plug)
                    tx(1.27)
                    ty(-2.54 * (idc_w - 1)/2)
                    tz(2.8)
                    rz(-90)
                    idc_model(idc_w);

                    pinheader_vert(n=idc_w, r=2);
                } else {
                    if (idc_plug)
                    tx(6.9)
                    ty(-2.54 * (idc_w - 1)/2)
                    tz(3.34)
                    ry(90)
                    rz(-90)
                    idc_model(idc_w);

                    tz(.8) pinheader_horiz(n=idc_w, r=2);

                    rz(-90)
                    color("#555555", 1.0)
                    render(10)
                    interface_spacer(n=5, r=2);
                }
            } else if ($ref == "C1") {
            } else if ($ref == "C2") {
                $component_bend = 90;
                kicad_component();
            } else if ($ref == "SW1") {
                translate([6.5, -4.5]/2)
                tz(2) dispenser_button();
                kicad_component();
            } else {
                kicad_component();
            }
        }
    }
}

module servo_wire() {
    yzx() cylx(0, 1.5, d=1.14);
    tx(1.5) {
        sphere(d=1.14);
        cylx(0, 5, d=1.14);
    }
}

module servo_model() {
    servo_l = servo_l1 + servo_l2;

    // main body
    color("#000088")
    cubex(ys=servo_w, x1=-servo_l1, x2=servo_l2, yc=0, z1=-15.72, z2=6.6);

    // mount tab
    color("#000077")
    difference() {
        cubex(ys=servo_w, x1=-servo_l1 - 4.7, x2=servo_l2 + 4.7, yc=0, z1=0, z2=2.45);
        tx(servo_l/2 - servo_l1) rfx() tx(servo_l/2 + 2) {
            cylx(-eps, 3, d=2);
            cubex(x1=0, x2=5, yc=0, ys=1.11, z1=-eps, z2=3);
        }

    }

    color("#000099")
    tz(6.6) {
        cylx(0, 4.5, d=servo_w);
    }

    tz(6.6 + 4.5) {
        color("#eeeeee")
        cylx(0, 2.9, d=4.8);
    }
    tz(-11.5) tx(servo_l2) {

        ty(-1.14) color("#553300") servo_wire();
        color("#ff0000") servo_wire();
        ty(1.14) color("#ffaa00") servo_wire();
    }
}

module idc_model(w=5) {
    color("#333333")
    difference() {
        union() {
            xzy()
            cubex(
                xc=0, xs=4.9 + 2.54 * w,
                zc=0, zs=6,
                y1=0, y2=14.5,//10.35,
                r1=1, r2=1
            );

            cubex(
                xc=0, xs=4,
                y1=-3.8, y2=-3,
                z1=0, z2=6.85
            );
        }
        tx(-2.54 * (w - 1) / 2)
        ty(-1.27)
        for (row = [0, 1], col=[0 : w - 1]) tx(col*2.54) ty(row*2.54) {
            cubex(xc=0, yc=0, xs=2, ys=2, z1=-eps, z2=20);
        }
        //tx()
    }
}

module autopill_led_pcb_model(traces=false, thick=1.6, trace_thick=0.05, convexity=10, pcb_color="#00bb00", trace_color="#00cc00", mask_color="#cccccc", silk_color="#f8f8f8", min_hole_size=0, hole_size_offset=0, stl=false) {
    color(pcb_color)
    render(convexity)
    if (stl) {
        intersection() {
            import("autopill_led_buttons-pcb-board.stl", convexity=convexity);
            pcb_model_led_split();
        }
    } else {
        linear_extrude(thick)
        difference() {
            intersection() {
                translate(-autopill_led_buttons_origin) import("autopill-led-buttons-pcb-board.dxf");
                pcb_model_led_split();
            }
            pcb_drill(autopill_led_buttons_drill, min_hole_size, hole_size_offset);
        }
    }

    if (traces) {
        mask_thick = trace_thick + .01;
        silk_thick = mask_thick + 0.02;
        color(trace_color, 0.5) render(convexity)
        intersection() {
            autopill_led_buttons_pcb_layer(trace_color, 0.5, "autopill-led-buttons-pcb-trace-b", -trace_thick-0.001, trace_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
        color(trace_color, 0.5) render(convexity)
        intersection() {
            autopill_led_buttons_pcb_layer(trace_color, 0.5, "autopill-led-buttons-pcb-trace-f", thick+0.001, trace_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
        color(mask_color, 0.5) render(convexity)
        intersection() {
            autopill_led_buttons_pcb_layer(mask_color, 1.0, "autopill-led-buttons-pcb-mask-b", -mask_thick-0.001, mask_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
        color(mask_color, 0.5) render(convexity)
        intersection() {
            autopill_led_buttons_pcb_layer(mask_color, 1.0, "autopill-led-buttons-pcb-mask-f", thick+0.001, mask_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
    }
}

module autopill_button_pcb_model(traces=false, thick=1.6, trace_thick=0.05, convexity=10, pcb_color="#00bb00", trace_color="#00cc00", mask_color="#cccccc", silk_color="#f8f8f8", min_hole_size=0, hole_size_offset=0, stl=false) {
    color(pcb_color)
    render(convexity)
    if (stl) {
        difference() {
            import("autopill_led_buttons-pcb-board.stl", convexity=convexity);
            pcb_model_led_split();
        }
    } else {
        linear_extrude(thick)
        difference() {
            difference() {
                translate(-autopill_led_buttons_origin) import("autopill-led-buttons-pcb-board.dxf");
                pcb_model_led_split();
            }
            pcb_drill(autopill_led_buttons_drill, min_hole_size, hole_size_offset);
        }
    }

    if (traces) {
        mask_thick = trace_thick + .01;
        silk_thick = mask_thick + 0.02;
        color(trace_color, 0.5) render(convexity)
        difference() {
            autopill_led_buttons_pcb_layer(trace_color, 0.5, "autopill-led-buttons-pcb-trace-b", -trace_thick-0.001, trace_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
        color(trace_color, 0.5) render(convexity)
        difference() {
            autopill_led_buttons_pcb_layer(trace_color, 0.5, "autopill-led-buttons-pcb-trace-f", thick+0.001, trace_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
        color(mask_color, 0.5) render(convexity)
        difference() {
            autopill_led_buttons_pcb_layer(mask_color, 1.0, "autopill-led-buttons-pcb-mask-b", -mask_thick-0.001, mask_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
        color(mask_color, 0.5) render(convexity)
        difference() {
            autopill_led_buttons_pcb_layer(mask_color, 1.0, "autopill-led-buttons-pcb-mask-f", thick+0.001, mask_thick, convexity, min_hole_size, hole_size_offset, stl);
            linear_extrude(6, center=true) pcb_model_led_split();
        }
    }
}

module complete_preview_dispenser(flip=false) {
    tz(slide_top) {
        idc_holder_shim();
        tz(3) {
            *color("#ffffff")
            render(10)
            dispenser_wirebox();

            *preview_dispenser_wirebox(pcb=false);

            *tz(dispenser_wirebox_height + 4) {
                color("#202020")
                render(10)
                dispenser();

                preview_dispenser(flip=flip, preview_pcb=false);
            }
        }
    }

}

module complete() {
    // Center parts

    color("#363636")
    render(10)
    pilltray_holder();

    color("#4a4a4a")
    render(10)
    pilltray_holder_top();

    color("#4a4a4a")
    render(10)
    pilltray_holder_rear_slide();

    color("#666666")
    render(10)
    pilltray();


    // mainbox
    ty(-mainbox_xcenter)
    tz(4) tx(pilltray_holder_x1 - 4 - eps)
    rz(90) {
        color("#202020")
        render(10)
        mainbox();

        tz(mainbox_h) {
            color("#404040")
            render(10)
            mainbox_lid();

            preview_mainbox_lid();
        }

    }

    // pi mount

    tz(pilltray_holder_pi_mount_z - 60)
    tx(pilltray_pi_x1 + 64)
    ry(45)
    tx(2)
    tz(4+eps)
    {

        color("#f8f8f8")
        render(10)
        pi_mount();

        rz(90)
        preview_pi_mount(preview_lid=false, preview_dpad=false);

        tz(pi_mount_h) {
            color("#202020")
            render(10)
            pi_mount_lid();

            preview_pi_mount_lid();
        }
    }

    //risers
    for (n = [1 : 4]) {
        ty(pilltray_width/2 + dispbase_y2 + (n - 1) * (.1 + dispbase_y2 - dispbase_y1)) {
            color("#2020c0")
            render(10)
            riser_dbl_r(n);

            tz(module_rise_initial + (n - 1) * module_rise) {
                color("#707070")
                render(10)
                basemodule();

                complete_preview_dispenser();

                tz(module_rise_rear)
                tx(-dispbase_row_spacing + dispbase_x2 + dispbase_x1) {
                    sx(-1)
                    color("#707070")
                    render(10)
                    basemodule();

                    rz(180)
                    complete_preview_dispenser(flip=true);
                }
            }


        }

        ty(-pilltray_width/2 + dispbase_y1 + -(n - 1) * (.1 + dispbase_y2 - dispbase_y1)) {
            color("#2020c0")
            render(10)
            riser_dbl_l(n);

            tz(module_rise_initial + (n - 1) * module_rise) {
                sy(-1)
                color("#707070")
                render(10)
                basemodule();

                complete_preview_dispenser(flip=true);

                tz(module_rise_rear)
                tx(-dispbase_row_spacing + dispbase_x2 + dispbase_x1) {
                    sy(-1)
                    sx(-1)
                    color("#707070")
                    render(10)
                    basemodule();

                    rz(180)
                    complete_preview_dispenser();
                }
            }
        }
    }
}

anim_frames = 60;
anim_frame = $t * anim_frames;

module gear_animate() {
    rotate = 360 * anim_frame / 120;

    baserot = rotate * pg_ring_gear_teeth;
    s_rot = -baserot / pg_sun_gear_teeth;

    planetgear(rotate=rotate);

    tz(-2)
    rz(s_rot)
    color("gold") render(10) planetgear_beltwheel();


    *rz(-rotate) color("red")
    tz(15) cubex(x1=0, x2=50, yc=0, ys=1, z1=0, z2=1);


    rz(-rotate * 4 / pg2_ring_gear_teeth + 180 / pg2_ring_gear_teeth) {
        color("red")
        tz(13.5) cubex(xc=0, xs=55, yc=0, ys=1, z1=0, z2=1);
        color("gold", 0.95) tz(17 + .2) render(10) difference() {
            tz(-2)
            pillwheel_gear_r();
            cylx(z1=-4, z2=1, d=100);
        }
    }
}

module dispenser_animate_cutout() {
    cubex(
        xc = -pill_width_fc/2, xs = 50,
        y1 = dispbase_y1, y2 = -pill_width_fc/2,
        z1=0, z2 = 100
    );
    cubex(
        x1 = pill_width_fc/2, x2 = 50,
        y1 = -pill_width_fc/2, y2 = -pincher_lever_thick/2,
        z1=0, z2 = 100
    );
}

module dispenser_animate_slider_cutout() {
    cubex(
        x1 = 8, x2 = 30,
        yc = 0, ys = slider_width + 2,
        z1 = -4, z2 = 12
    );

}

module spring_model(h) {
    n = 12;
    loops = 6;
    helix_extrude(angle=360 * loops, ofs=[5.5/2, 0], cofs=[0, h/loops], points = [for (ang = [0 : n - 1]) rotpt([0, .4], 360 * ang / n)]);
}

module dispenser_animate(spring=true) {
    echo(animtime=$ctime);
    echo(pincher_stop=$pincher_stop);
    echo(servo_ang=$servo_ang);


    pincher_pos = [pincher_x, 0, pincher_z];


    servo_end = servo_pos + rotpt([servo_arm_length, 0], 45 + $servo_ang);
    servo_end_2 = servo_pos + rotpt([-2.12437, -10.403], 45 + $servo_ang);


    slider_base_x = servo_end.x - 6;
    slider_pos = servo_end.y < -slider_width/2 ? min(slider_base_x, servo_end_2.x - 26.5) : slider_base_x;

    pincher_stopped_x = 14.4;

    pincher_min_x = $pincher_stop ? pincher_stopped_x : 12;
    pincher_slider_x = max(pincher_min_x, slider_pos + dispenser_slider_length + 4.1);

    pincher_rot = -atan2(pincher_slider_x + 4 - pincher_x, pincher_z - 2.5);
    pincher_stopped_rot = -atan2(pincher_stopped_x + 4 - pincher_x, pincher_z - 2.5);

    rz1 = pincher_z - pincher_ring_z1;
    ring_rad1 = hypot(pincher_x - pill_width_fc/2, rz1);

    ptx = -pincher_x + pill_width/2;
    pty = -sqrt(ring_rad1^2 - ptx ^ 2);

    pincher_pt = [pincher_x, pincher_z] + rotpt([ptx, pty], -5 - pincher_rot);
    pincher_stopped_pt = [pincher_x, pincher_z] + rotpt([ptx, pty], -5 - pincher_stopped_rot);

    spring_x1 = pincher_slider_x + 8;
    spring_x2 = dispbase_x2 - 3;

    //translate(pincher_pt) ty(-10) color("red") cube(1.1, center=true);

    dispenser_color = "#333333";
    dispenser_tube_lower_color = "#50a0ff";
    dispenser_tube_upper_color = "#0088bb";

    slider_color = "#80c000";
    pincher_slider_color = "#00c060";

    servo_arm_color = "#00b000";

    color(dispenser_color) render(10) difference() {
        dispenser();
        dispenser_animate_cutout();
    }

    color(dispenser_tube_lower_color) render(10) difference() {
        dispenser_tube_lower();
        dispenser_animate_cutout();
    }

    color(dispenser_tube_upper_color) render(10) difference() {
        dispenser_tube_upper();
        dispenser_animate_cutout();
    }

    translate(servo_pos)
    servo_model();

    tz(servo_gear_height+eps2)
    translate(servo_pos)
    rz($servo_ang)
    rz(45)
    rx(180)
    color(servo_arm_color)
    render(10)
    servo_arm();


    tx(slider_pos)
    color(slider_color)
    render(10)
    dispenser_slider();

    tx(pincher_slider_x)
    color(pincher_slider_color)
    render(10) difference() {
        pincher_slider();
        if (spring)
            dispenser_animate_slider_cutout();
    }

    spring_true_h = spring_x2 - spring_x1;
    //spring_h = floor(spring_true_h);
    //sx((spring_x2 - spring_x1)  / 12)
    if (spring)
    tx(spring_x1) yzx() {
        spring_model(spring_true_h);
    }

    //tx(pincher_x + lever_x) ty(-4) tz(2.5) color("red") cube(1, center=true);

    let(xp=pincher_pos)
    translate(xp) ry(pincher_rot) translate(-xp)
    color("#ffff00") render(10) {
        pincher();
    }
    //translate(vxy(servo_end_2, 15)) cube(1, center=true);

    pinch_xofs = [
        50,
        pincher_pt.x,
        pincher_stopped_pt.x,
    ];

    for(pill = $pills) {
        pos = pill[0];
        pinched = pill[1];
        yrot = pill[2];

        zpos = pos < 0 ? -((pos/10)^2)*10 : pos;

        if (zpos > -500) {
            tx(min(0, pinch_xofs[pinched] - pill_width/2 + .5))
            tz(zpos)
            ry(yrot)
            color(pill[3]) pill(d=pill_width-1);
        }
    }

    if ($opacity > 0) {
        color(dispenser_color, $opacity) render(10) intersection() {
            dispenser();
            dispenser_animate_cutout();
        }

        color(dispenser_tube_lower_color, $opacity) render(10) intersection() {
            dispenser_tube_lower();
            dispenser_animate_cutout();
        }

        color(dispenser_tube_upper_color, $opacity) render(10) intersection() {
            dispenser_tube_upper();
            dispenser_animate_cutout();
        }

        if (spring)
        tz(eps)
        tx(pincher_slider_x)
        color(pincher_slider_color, $opacity)
        render(10) {
            intersection() {
                pincher_slider();
                dispenser_animate_slider_cutout();
            }
        }
    }
}
