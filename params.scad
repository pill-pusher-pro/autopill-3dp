module_rise = 25;
module_rise_initial = 25;
module_rise_rear = 37;

pill_width_fc = pill_width + (min(1, pill_width / 4));
pill_thick_fc = pill_thick + (min(1, pill_thick / 3));

// CNC Kitchen threaded inserts
// M2.5 standard
//insert_dia_m2_5 = 3.6;
insert_dia_m2_5 = 4.2;
insert_length_m2_5 = 4.0;

// M3 short
insert_dia_m3 = 4.1;
insert_length_m3 = 4.0;

// pillwheel

wheel_rad = 28;
wheel_spacing = pill_width;
wheel_fit_clearance = 0.3;
wheel_fit_clearance_2 = 0.8;
wheel_shaft_rad = 2;
wheel_shaft_outer_rad = 12;
wheel_hub_height = 3;

poker_height = 2.8;
poker_width = 1.8;
poker_spokes = 6;

poker_cam_ang = -45;
//poker_cam_ang = 180;
poker_cam_ang_size = 73;

// hopper

hopper_length = 120;
hopper_width = 58;
hopper_height = 20;
hopper_funnel_height = 30;
hopper_wall_thick = 4;

hinge_size = 8.1;

// dispenser

dispbase_x1 = -68;
dispbase_x2 = 38;

dispbase_y2 = 35;
dispbase_y1 = -dispbase_y2;

dispbase_row_spacing = (dispbase_x2 - dispbase_x1) + 10;

pilltray_x1 = -100;
pilltray_holder_x1 = -100;
pilltray_pi_x1 = -80;
pilltray_holder_pi_mount_z = 120;

photodiode_height = 5.4;

top_mount_z = pill_len + 5;

tan_offset = 6;

gear_fc = .08;

wheel_tx = wheel_rad + wheel_fit_clearance + wheel_spacing/2;

// lower hopper funnel

pri_height = pill_thick_fc;

total_height = 19;

pri_depth = (total_height - pri_height) / 2;
lid_thick = pri_depth;

motormount_l = 23;
motormount_w = 25;

lid_y_cut = 0;

tangent_ang = 40;

ymax = wheel_rad + 90;
ymin = -wheel_rad - 4;
ymin2 = ymin - motormount_w - 4;

tan_pt_vec = [-cos(tangent_ang), sin(tangent_ang)];
tan_vec = [tan_pt_vec.y, -tan_pt_vec.x];

tangent_pt = [wheel_tx, 0] + tan_pt_vec * (wheel_rad - tan_offset);

dia_text = pill_width == pill_thick ? str("D", pill_width) : str("W", pill_width, " / T", pill_thick);

// bearing (lower_hopper, pillwheel, bearing)

bearing_thick = 6;
bearing_outer = 24;
bearing_inner = 12;
bearing_top_z = 1 + bearing_thick;

// beltwheel

beltwheel_rad = wheel_rad + 14;
beltwheel_bearing_spacing = 3;
beltwheel_offset = 8;

pillwheel_gear_teeth = 50;

pillwheel_gear_rad = wheel_tx - .2;
pillwheel_gear_thick = 5;
pillwheel_gear_tooth_size = pill_width >= 12 ? 0.8 : 1.0;

spoke_r1 = bearing_inner/2 + 2;
spoke_r2 = pillwheel_gear_rad - 5;
spoke_w = 2.5;

// planet gear

pg_fit_clearance = 0.06;

pg_hole = 5;

pg_gear_size = 1.1;

pg_planet_carrier_teeth = 20;
pg_planet_gear_teeth = 10;
pg_num_planets = 5;
pg_scale = planetgear_hgr ? 0.9 : 1.0;


pg_ring_gear_teeth = pg_planet_carrier_teeth + pg_planet_gear_teeth;
pg_sun_gear_teeth = pg_planet_carrier_teeth - pg_planet_gear_teeth;

pg_gear_ratio = pg_planet_carrier_teeth * 2 / pg_sun_gear_teeth;

pg_rotate_print = 90;
pg_fudge_rotate = 0; //-.01/6 * 360;

pg2_sun_gear_teeth = pg_sun_gear_teeth + 2;
pg2_planet_gear_teeth = pg_planet_gear_teeth - 2;
pg2_ring_gear_teeth = pg_ring_gear_teeth - 2;

echo(str("gear ratio = ", pg_gear_ratio, " : 1"));
echo(str("ring size = ", pg_ring_gear_teeth));
echo(str("planet size = ", pg_planet_gear_teeth));

pg_sun_gear_size = pg_sun_gear_teeth * pg_scale;
pg_planet_gear_size = pg_planet_gear_teeth * pg_scale;
pg_carrier_gear_size = (pg_sun_gear_teeth + pg_planet_gear_teeth) * pg_scale;
pg_ring_gear_size = pg_ring_gear_teeth * pg_scale;

pg2_planet_gear_size = pg2_planet_gear_teeth * pg_scale;
pg2_ring_gear_size = pg2_ring_gear_teeth * pg_scale;

pg_outer_size = pg_ring_gear_size + 5;

pg_shroud_rad = wheel_rad + 13;
pg_mounts = [
    [-50, wheel_rad + 11.5],
    [50, wheel_rad + 11.5],
];

pg_shroud_inner = pg_shroud_rad - 5;

pg_z = bearing_top_z - bearing_thick - beltwheel_bearing_spacing - 10 - 6;
echo(str("planet mount size = ", -pg_z));


// basemodule

slide_tube_height = 12;
slide_tube_dia = 30;
slide_top = module_rise + slide_tube_dia + slide_tube_height + 5;

// pilltray

pilltray_width = 120;
//pilltray_holder_height = module_rise + slide_tube_height + slide_tube_dia/2 - 4;
pilltray_height = module_rise + slide_tube_height - 2;
pilltray_holder_height = pilltray_height + 2;

// dispenser-wirebox

dispenser_wirebox_height = np(13);

pcb_offset = [-65.5, -31.2] +[0,0];

pcb_mount_holes = autopill_dispenser_single_drill_2_7_npth;

autopill_dispenser_objects = dual_dispenser_pcb ? autopill_dispenser_dual_objects : autopill_dispenser_single_objects;

idc_center = pcb_offset + autopill_dispenser_single_pos_J1 + 2.54 * [-.5, -2];
idc_size = [6.4, 18];
idc_protrusion = 6.6;

// dispenser

dispenser_pill_hole = 12;
dispenser_pcb_pill_hole = 19;

dispenser_slide_distance = 2 * dispenser_pill_hole;
dispenser_slide_extra_f = pincher ? -2 : 4;
dispenser_slide_extra_r = 31;
dispenser_slider_length = (pincher ? 2 : 3) * dispenser_pill_hole + dispenser_slide_extra_f + dispenser_slide_extra_r;

servo_base = [ 6, -5 ];
servo_arm_length = 12 * sqrt(2);

slider_x1 = -dispenser_slide_extra_r - dispenser_pill_hole * 2.5;
slider_x2 = -dispenser_slide_extra_r - dispenser_pill_hole/2;
slider_width = 16;

servo_pos = [slider_x1, 0] + servo_base + [ 12, -12 ];
servo_l1 = 17;
servo_l2 = 6;
servo_w = 12.2;

springw = 6.5;
springl = 16;

spring_x1 = dispbase_x2 - 3;

// total height = 30, height to mounts = 18
servo_gear_height = np(12.45);

tower_l = 30;
tower_w = 16;
tower_h = pincher ? 60 : 40;
tower_wall_thick = [4, 10.2];


// pincher

pincher_lever_thick = 3;
pincher_w = ceil(min(pill_thick * .5, 6)/0.2)*0.2;

pincher_x = tower_w/2 + tower_wall_thick.x + 3.5;
pincher_z = tower_h - 4;
pincher_ring_z1 = pill_len + .2;
pincher_ring_z2 = min(pincher_z - 16, pill_len * 1.8 - 2);
pincher_ring_l = pincher_ring_z2 - pincher_ring_z1;

// pi mount

pi_mount_h = 23;
pi_mount_l = 85+12;
pi_w = 56;

pi_display_w = 55.6;
pi_display_l = 85.6;
pi_display_z = 2 + 23.8;

pi_xoffset = -15;
pi_led_pcb_pos = [-61.1, -25.7, 14];
pi_button_pcb_pos = [-35, 35, 9] - vxy(autopill_led_buttons_pos_J3 - [2.54 * 3, 0]);

pi_buttons = [
    autopill_led_buttons_data_SW1,
    autopill_led_buttons_data_SW2,
    autopill_led_buttons_data_SW3,
    autopill_led_buttons_data_SW4,
    autopill_led_buttons_data_SW5,
    autopill_led_buttons_data_SW6,
];

pi_button_pos = [for (obj = pi_buttons) [obj[1].x, obj[1].y] + [4.5, 6.5]/2];

pushbutton_h = 5.2;

button_z = pi_button_pcb_pos.z + 1.6 + pushbutton_h;
button_h = 5;

dpad_rad = pi_button_pos[4].x - pi_button_pos[0].x;

// mainbox

mainbox_w = 178;
mainbox_l = 64;
mainbox_h = 24;

mega_mount_z = 3.2;
pcb_top_z = mega_mount_z + 14.2;

mega_pos = [3.5, 6.4];
main_pcb_pos = mega_pos - autopill_main_pos_A1;

converter_5v_pos = mega_pos + [116, 2];
converter_5v_size = [26.7, 46.2, 13.87];

converter_6v_pos = mega_pos + [148, 8];
converter_6v_size = [25.14, 34.5, 20.27];


mainbox_xcenter = main_pcb_pos.x + (autopill_main_pos_J8.x + autopill_main_pos_J1.x - 2.54) / 2;
//mainbox_xcenter = main_pcb_pos.x + autopill_main_pos_J1.x;

mainbox_ports = [

    autopill_main_pos_J9,
    autopill_main_pos_J10,
    autopill_main_pos_J11,
    autopill_main_pos_J12,
    autopill_main_pos_J13,
    autopill_main_pos_J14,
    autopill_main_pos_J15,
    autopill_main_pos_J16,


    autopill_main_pos_J1,
    autopill_main_pos_J2,
    autopill_main_pos_J3,
    autopill_main_pos_J4,
    autopill_main_pos_J5,
    autopill_main_pos_J6,
    autopill_main_pos_J7,
    autopill_main_pos_J8,
];

mega_mount_holes = [
    [13.95, 2.54],
    [15.25, 50.8],
    [66.04, 7.6],
    [66.04, 35.55],
    [96.5, 2.54],
    [90.15, 50.8],
];

mount_holes_m2 = [
    each [for (pt = mega_mount_holes) mega_pos + pt],
];
mount_holes_m3 = [
    converter_6v_pos + [converter_6v_size.x/2, -3],
    converter_6v_pos + [converter_6v_size.x/2, 40-3],
    converter_5v_pos + [converter_5v_size.x/2, -3],
    converter_5v_pos + [converter_5v_size.x/2, 53-3],
];

md_spacer_layer_1 = [
    [29.4, 32.2],
    [29.4, 29.0],
    [33, 46.6],
    [33, 43.7],
    [33, 40.3],
    [33, 37.5],
    [47.6, 46.6],
    [47.6, 43.7],
    [47.6, 40.3],
    [47.6, 37.5],
];

md_spacer_layer_2 = [
    [29.4, 31.55],
    [29.4, 29.0],
    [33.55, 45.5],
    [33.55, 42.95],
    [33.55, 40.4],
    [33.55, 37.9],
    [48.8, 45.41],
    [48.8, 42.83],
    [48.81, 40.33],
    [48.81, 37.76],
];

cam_rad_base = (bearing_outer/2 + wheel_rad - 6) / 2 - 2.5;
