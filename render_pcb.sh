#!/bin/bash
OPENSCAD=$HOME/lsrctree/git/openscad/build/openscad
render() {
    part=$1
    outb=$2
    outf=$outb.png
    cam=$3
    shift
    shift
    shift
    $OPENSCAD autopill.scad -D"part=\"$part\"" -Dhighres=1 -Dpreview_pcb_components=1 -Dpreview_pcb_traces=1 --imgsize=2000,1600 --colorscheme=Metallic -o x$outf --camera $cam --projection o "$@"
    convert x$outf  -transparent \#aaaaff -trim -resize 600x -bordercolor none -border 20 -define png:exclude-chunks=date,time $outf
    rm -f x$outf
}

render pcb-model dispenser-single-pcb-top 42,21,10,51,0,319,220 -Ddual_dispenser_pcb=0 -Ddispenser_connector_vertical=1 &
render pcb-model dispenser-single-pcb-bot 42,21,5,235,0,319,220 -Ddual_dispenser_pcb=0 -Ddispenser_connector_vertical=1 &
render pcb-model dispenser-dual-pcb-top 42,21,10,51,0,319,220 -Ddual_dispenser_pcb=1 -Ddispenser_connector_vertical=1 &
render pcb-model dispenser-dual-pcb-bot 42,21,5,235,0,319,220 -Ddual_dispenser_pcb=1 -Ddispenser_connector_vertical=1 &
render pcb-model-main main-pcb-top 50,26,10,51,0,319,230 &
render pcb-model-main main-pcb-bot 50,26,5,235,0,319,230 &
render pcb-model-led led-pcb-top 50,31,10,51,0,319,230 &
render pcb-model-led led-pcb-bot 50,31,5,235,0,319,230 &
#render pcb-j2-mech-support 32,32,0,0,0,0,140 -Dsingle_button=1 -Dsingle_relay=1 &
wait
