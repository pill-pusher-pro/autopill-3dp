include <common.scad>

include <autopill-main-pcb-model.scad>
include <autopill-dispenser-dual-pcb-model.scad>
include <autopill-dispenser-single-pcb-model.scad>
include <autopill-led-buttons-pcb-model.scad>

/* [Common] */
part = "dispenser"; // [complete, gear-animate, lower-hopper-base-test, lower-hopper-base, lower-hopper-base-shim, lower-hopper-lid, dispenser-tube-lower, dispenser-tube-upper, dispenser-slider-upper, pincher, beltwheel, beltwheel-ext, pillwheel-gear-l, pillwheel-gear-r, planetgear, planetgear-sun, planetgear-planet, planetgear-ring, planetgear-carrier, planetgear-shim, planetgear-beltwheel, pillwheel, pillwheel-holder, motorbeltwheel, pillwheel-flex-cover, pillwheel-poker, pillwheel-poker-cam-follower, pillwheel-insert, pillwheel-gear-spacer, disptopmount, disptopmount-pg-l, disptopmount-pg-r, dispenser-wirebox, dispenser, dispenser-slider, dispenser-pcb-spacer-front, dispenser-pcb-spacer-rear, dispenser-button, led-holder, pincher-slider, servo-arm, riser, riser-dbl-r, riser-dbl-l, stackable-spacer, basemodule-r, basemodule-l, slide-insert, idc-holder-shim, pilltray, pilltray-holder, pilltray-holder-top, pilltray-holder-top-aligntool-1, pilltray-holder-top-aligntool-2, pilltray-holder-rear-slide, hopper, hopper-lid, spiral-l, spiral-r, pcb-model, pcb-model-main, pcb-model-led, mainbox, mainbox-pcb-spacer, mainbox-lid, mainbox-lid-inlay-front, mainbox-lid-inlay-back, pi-mount, pi-mount-lid, pi-mount-lid-inlay-led, pi-mount-lid-inlay-front, pi-mount-lid-inlay-back, pi-button-dpad, pi-button-dpad-inlay, button-pcb-cover, display-spacer, interface-spacer, motor-pin-spacer, md-spacer, md-solder-jig, wire-holder-r, wire-holder-l]
highres = false;
$fn = (!highres && $preview) ? 30 : 128;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-100 : .25 : 200]

/* [Preview parts] */

preview_lid = false;
preview_pillwheel = false;
preview_pillwheel_holder = false;
preview_pillwheel_spokes = false;
preview_pillgear = false;
preview_planetgear = false;
preview_beltwheel = false;
preview_bearing = false;

preview_pilltray_holder = false;

preview_pcb = false;
preview_pcb_traces = false;
preview_pcb_spacer = false;
preview_hopper = false;
preview_lower_hopper = false;
preview_wirebox = false;
preview_disptopmount = false;
preview_dispenser_tube = false;

preview_pilltray = true;

pg_flat = false;

preview_pg_rot = 0; // [0 : 0.5 : 720]
preview_pincher_rot = 0; // [0 : 0.25 : 14]
preview_slider = 0; // [-1 : 0.05 : 1]
preview_slider_nudge = 0; // [-2 : 0.05 : 2]

preview_wheel_rot = 0; // [0 : 0.5 : 360]

preview_cutout = false;

preview_idc = true;

dual_dispenser_pcb = false;

/* [Parameters] */
pincher = false;
pill_width = 10; // [3 : .1 : 13]
pill_thick = 10; // [2 : .1 : 11]
pill_len = 24.6;
planetgear_hgr = false;
pillgear_old_interface = false;
riser_height = 1;
slide_insert_length = 1;
threaded_inserts = true;
dispenser_connector_vertical = false;
hopper_window = true;
module __nc__() {} // end of customizer variables

$component_bend = 0;
$pin_length = 2;

include <params.scad>
include <gear.scad>
include <pillwheel.scad>
include <lower-hopper.scad>
include <upper-hopper.scad>
include <motormount.scad>
include <basemodule.scad>
include <wirebox.scad>
include <mainbox.scad>
include <pi-mount.scad>
include <dispenser.scad>
include <dispenser-tube.scad>
include <pilltray.scad>

include <tools.scad>

include <models.scad>

module skew_by(pt, z) {
    multmatrix([
        [1, 0, pt.x/z, 0],
        [0, 1, pt.y/z, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ])
    children();
}

// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "complete") {
        if ($preview) {
            complete();
        } else {
            complete();
        }
    }
    else if (part == "gear-animate") {
        if ($preview) {
            gear_animate();
        } else {
            gear_animate();
        }
    }
    else if (part == "lower-hopper-base-test") {
        if ($preview) {
            lower_hopper_base_test();
        } else {
            lower_hopper_base_test();
        }
    }
    else if (part == "lower-hopper-base") {
        if ($preview) {
            lower_hopper_base();
            preview_lower_hopper_base();
        } else {
            rz(-90)
            lower_hopper_base();
        }
    }
    else if (part == "lower-hopper-base-shim") {
        if ($preview) {
            lower_hopper_base_shim();
        } else {
            lower_hopper_base_shim();
        }
    }
    else if (part == "lower-hopper-lid") {
        if ($preview) {
            lower_hopper_lid();
        } else {
            rz(90)
            lower_hopper_lid();
        }
    }
    else if (part == "dispenser-tube-lower") {
        if ($preview) {
            dispenser_tube_lower();
        } else {
            ry(pincher ? -90 : 0)
            dispenser_tube_lower();
        }
    }
    else if (part == "dispenser-tube-upper") {
        if ($preview) {
            dispenser_tube_upper();
        } else {
            ry(pincher ? -90 : 180)
            dispenser_tube_upper();
        }
    }
    else if (part == "dispenser-slider-upper") {
        if ($preview) {
            dispenser_slider_upper();
        } else {
            ry(180)
            dispenser_slider_upper();
        }
    }
    else if (part == "pincher") {
        if ($preview) {
            pincher();
        } else {
            rx(90)
            pincher();
        }
    }
    else if (part == "beltwheel") {
        if ($preview) {
            beltwheel();
        } else {
            beltwheel();
        }
    }
    else if (part == "beltwheel-ext") {
        if ($preview) {
            beltwheel(beltwheel_offset);
        } else {
            beltwheel(beltwheel_offset);
        }
    }
    else if (part == "pillwheel-gear-l") {
        if ($preview) {
            pillwheel_gear_l();
        } else {
            rx(planetgear_hgr ? 180 : 0)
            pillwheel_gear_l();
        }
    }
    else if (part == "pillwheel-gear-r") {
        if ($preview) {
            pillwheel_gear_r();
            preview_pillwheel_gear_r();
        } else {
            rx(planetgear_hgr ? 180 : 0)
            pillwheel_gear_r();
        }
    }
    else if (part == "planetgear") {
        if ($preview) {
            planetgear();
            preview_planetgear();
        } else {
            planetgear();
        }
    }
    else if (part == "planetgear-sun") {
        if ($preview) {
            planetgear_sun();
        } else {
            planetgear_sun();
        }
    }
    else if (part == "planetgear-planet") {
        if ($preview) {
            planetgear_planet();
        } else {
            planetgear_planet();
        }
    }
    else if (part == "planetgear-ring") {
        if ($preview) {
            planetgear_ring();
        } else {
            planetgear_ring();
        }
    }
    else if (part == "planetgear-carrier") {
        if ($preview) {
            planetgear_carrier();
        } else {
            planetgear_carrier();
        }
    }
    else if (part == "planetgear-shim") {
        if ($preview) {
            planetgear_shim();
        } else {
            planetgear_shim();
        }
    }
    else if (part == "planetgear-beltwheel") {
        if ($preview) {
            planetgear_beltwheel();
        } else {
            planetgear_beltwheel();
        }
    }
    else if (part == "pillwheel") {
        if ($preview) {
            pillwheel();
            preview_pillwheel();
        } else {
            rx(180)
            pillwheel();
        }
    }
    else if (part == "pillwheel-holder") {
        if ($preview) {
            pillwheel_holder();
            preview_pillwheel_holder();
        } else {
            pillwheel_holder();
        }
    }
    else if (part == "motorbeltwheel") {
        if ($preview) {
            motorbeltwheel();
        } else {
            motorbeltwheel();
        }
    }
    else if (part == "pillwheel-flex-cover") {
        if ($preview) {
            pillwheel_flex_cover();
        } else {
            pillwheel_flex_cover();
        }
    }
    else if (part == "pillwheel-poker") {
        if ($preview) {
            pillwheel_poker();
            preview_pillwheel_poker();
        } else {
            rx(180)
            pillwheel_poker();
        }
    }
    else if (part == "pillwheel-poker-cam-follower") {
        if ($preview) {
            pillwheel_poker_cam_follower();
        } else {
            pillwheel_poker_cam_follower();
        }
    }
    else if (part == "pillwheel-insert") {
        if ($preview) {
            pillwheel_insert();
        } else {
            pillwheel_insert();
        }
    }
    else if (part == "pillwheel-gear-spacer") {
        if ($preview) {
            pillwheel_gear_spacer();
        } else {
            rx(180)
            pillwheel_gear_spacer();
        }
    }
    else if (part == "disptopmount") {
        if ($preview) {
            ty(20) tz(3) rx(-preview_wheel_rot) ty(-20) tz(-3)
            disptopmount();
        } else {
            disptopmount();
        }
    }
    else if (part == "disptopmount-pg-l") {
        if ($preview) {
            disptopmount_pg_l();
        } else {
            disptopmount_pg_l();
        }
    }
    else if (part == "disptopmount-pg-r") {
        if ($preview) {
            disptopmount_pg_r();
        } else {
            disptopmount_pg_r();
        }
    }
    else if (part == "dispenser-wirebox") {
        if ($preview) {
            dispenser_wirebox();
            preview_dispenser_wirebox();
        } else {
            dispenser_wirebox();
        }
    }
    else if (part == "dispenser") {
        if ($preview) {
            dispenser();
            preview_dispenser();
        } else {
            dispenser();
        }
    }
    else if (part == "dispenser-slider") {
        if ($preview) {
            dispenser_slider();
        } else {
            dispenser_slider();
        }
    }
    else if (part == "dispenser-pcb-spacer-front") {
        if ($preview) {
            dispenser_pcb_spacer_front();
        } else {
            rx(180)
            dispenser_pcb_spacer_front();
        }
    }
    else if (part == "dispenser-pcb-spacer-rear") {
        if ($preview) {
            dispenser_pcb_spacer_rear();
        } else {
            dispenser_pcb_spacer_rear();
        }
    }
    else if (part == "dispenser-button") {
        if ($preview) {
            dispenser_button();
        } else {
            rx(180)
            dispenser_button();
        }
    }
    else if (part == "led-holder") {
        if ($preview) {
            led_holder();
        } else {
            rx(-90)
            led_holder();
        }
    }
    else if (part == "pincher-slider") {
        if ($preview) {
            pincher_slider();
        } else {
            pincher_slider();
        }
    }
    else if (part == "servo-arm") {
        if ($preview) {
            servo_arm();
        } else {
            servo_arm();
        }
    }
    else if (part == "riser") {
        if ($preview) {
            riser(riser_height * module_rise);
        } else {
            riser(riser_height * module_rise);
        }
    }
    else if (part == "riser-dbl-r") {
        if ($preview) {
            riser_dbl_r(riser_height);
        } else {
            riser_dbl_r(riser_height);
        }
    }
    else if (part == "riser-dbl-l") {
        if ($preview) {
            riser_dbl_l(riser_height);
        } else {
            riser_dbl_l(riser_height);
        }
    }
    else if (part == "stackable-spacer") {
        if ($preview) {
            stackable_spacer();
        } else {
            stackable_spacer();
        }
    }
    else if (part == "basemodule-r") {
        if ($preview) {
            basemodule_r();
            preview_basemodule_r();
        } else {
            basemodule_r();
        }
    }
    else if (part == "basemodule-l") {
        if ($preview) {
            basemodule_l();
        } else {
            basemodule_l();
        }
    }
    else if (part == "slide-insert") {
        if ($preview) {
            slide_insert();
        } else {
            slide_insert();
        }
    }
    else if (part == "idc-holder-shim") {
        if ($preview) {
            idc_holder_shim();
            preview_idc_holder_shim();
        } else {
            rx(180)
            idc_holder_shim();
        }
    }
    else if (part == "pilltray") {
        if ($preview) {
            pilltray();
            preview_pilltray();
        } else {
            pilltray();
        }
    }
    else if (part == "pilltray-holder") {
        if ($preview) {
            pilltray_holder();
            preview_pilltray_holder();
        } else {
            pilltray_holder();
        }
    }
    else if (part == "pilltray-holder-top") {
        if ($preview) {
            pilltray_holder_top();
            preview_pilltray_holder_top();
        } else {
            pilltray_holder_top();
        }
    }
    else if (part == "pilltray-holder-top-aligntool-1") {
        if ($preview) {
            pilltray_holder_top_aligntool_1();
            preview_pilltray_holder_top_aligntool_1();
        } else {
            pilltray_holder_top_aligntool_1();
        }
    }
    else if (part == "pilltray-holder-top-aligntool-2") {
        if ($preview) {
            pilltray_holder_top_aligntool_2();
            preview_pilltray_holder_top_aligntool_2();
        } else {
            pilltray_holder_top_aligntool_2();
        }
    }
    else if (part == "pilltray-holder-rear-slide") {
        if ($preview) {
            pilltray_holder_rear_slide();
        } else {
            pilltray_holder_rear_slide_xform()
            pilltray_holder_rear_slide();
        }
    }
    else if (part == "hopper") {
        if ($preview) {
            hopper();
            preview_hopper();
        } else {
            ry(180)
            hopper();
        }
    }
    else if (part == "hopper-lid") {
        if ($preview) {
            hopper_lid();
        } else {
            ry(180)
            hopper_lid();
        }
    }
    else if (part == "spiral-l") {
        if ($preview) {
            wheel_spiral();
        } else {
            wheel_spiral();
        }
    }
    else if (part == "spiral-r") {
        if ($preview) {
            wheel_spiral();
        } else {
            sx(-1)
            wheel_spiral();
        }
    }
    else if (part == "pcb-model") {
        if ($preview) {
            pcb_model(traces=preview_pcb_traces);
        } else {
            pcb_model(traces=preview_pcb_traces);
        }
    }
    else if (part == "pcb-model-main") {
        if ($preview) {
            pcb_model_main(traces=preview_pcb_traces);
        } else {
            pcb_model_main(traces=preview_pcb_traces);
        }
    }
    else if (part == "pcb-model-led") {
        if ($preview) {
            pcb_model_led(traces=preview_pcb_traces);
        } else {
            pcb_model_led(traces=preview_pcb_traces);
        }
    }
    else if (part == "mainbox") {
        if ($preview) {
            mainbox();
            preview_mainbox();
        } else {
            mainbox();
        }
    }
    else if (part == "mainbox-pcb-spacer") {
        if ($preview) {
            mainbox_pcb_spacer();
        } else {
            mainbox_pcb_spacer();
        }
    }
    else if (part == "mainbox-lid") {
        if ($preview) {
            mainbox_lid();
            preview_mainbox_lid();
        } else {
            rx(180)
            mainbox_lid();
        }
    }
    else if (part == "mainbox-lid-inlay-front") {
        if ($preview) {
            mainbox_lid_inlay_front();
        } else {
            rx(180)
            mainbox_lid_inlay_front();
        }
    }
    else if (part == "mainbox-lid-inlay-back") {
        if ($preview) {
            mainbox_lid_inlay_back();
        } else {
            rx(180)
            mainbox_lid_inlay_back();
        }
    }
    else if (part == "pi-mount") {
        if ($preview) {
            rz(-90)
            pi_mount();
            preview_pi_mount();
        } else {
            pi_mount();
        }
    }
    else if (part == "pi-mount-lid") {
        if ($preview) {
            pi_mount_lid();
            preview_pi_mount_lid();
        } else {
            rx(180)
            pi_mount_lid();
        }
    }
    else if (part == "pi-mount-lid-inlay-led") {
        if ($preview) {
            pi_mount_lid_inlay_led();
        } else {
            rx(180)
            pi_mount_lid_inlay_led();
        }
    }
    else if (part == "pi-mount-lid-inlay-front") {
        if ($preview) {
            pi_mount_lid_inlay_front();
        } else {
            rx(180)
            pi_mount_lid_inlay_front();
        }
    }
    else if (part == "pi-mount-lid-inlay-back") {
        if ($preview) {
            pi_mount_lid_inlay_back();
        } else {
            rx(180)
            pi_mount_lid_inlay_back();
        }
    }
    else if (part == "pi-button-dpad") {
        if ($preview) {
            pi_button_dpad();
            preview_pi_button_dpad();
        } else {
            rx(180)
            pi_button_dpad();
        }
    }
    else if (part == "pi-button-dpad-inlay") {
        if ($preview) {
            pi_button_dpad_inlay();
        } else {
            rx(180)
            pi_button_dpad_inlay();
        }
    }
    else if (part == "button-pcb-cover") {
        if ($preview) {
            button_pcb_cover();
        } else {
            rx(180)
            button_pcb_cover();
        }
    }
    else if (part == "display-spacer") {
        if ($preview) {
            display_spacer();
        } else {
            rx(180)
            display_spacer();
        }
    }
    else if (part == "interface-spacer") {
        if ($preview) {
            interface_spacer();
        } else {
            interface_spacer();
        }
    }
    else if (part == "motor-pin-spacer") {
        if ($preview) {
            interface_spacer(n=4, r=1, hh=1.2);
        } else {
            interface_spacer(n=4, r=1, hh=1.2);
        }
    }
    else if (part == "md-spacer") {
        if ($preview) {
            md_spacer();
        } else {
            md_spacer();
        }
    }
    else if (part == "md-solder-jig") {
        if ($preview) {
            md_solder_jig();
        } else {
            md_solder_jig();
        }
    }
    else if (part == "wire-holder-r") {
        if ($preview) {
            wire_holder_r();
        } else {
            wire_holder_r();
        }
    }
    else if (part == "wire-holder-l") {
        if ($preview) {
            wire_holder_l();
        } else {
            wire_holder_l();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
