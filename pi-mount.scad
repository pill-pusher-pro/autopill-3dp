module pi_mount() {
    button_pcb_points_raw = [
        [50.1, 44.1],
        [63.9, 44.1],
        [63.9, 32],
        [73, 32],
        [73, 26.5],
        [91.5, 26.5],
        [91.5, 55.7],
        [80, 55.7],
        [80, 66.5],
        [65.2, 66.5],
        [65.2, 57.6],
        [50.1, 57.6],
    ];

    //-
    org = -autopill_led_buttons_origin;
    button_pcb_points = [for (pt = button_pcb_points_raw) [pt.x, -pt.y] + org];

    difference() {
        union() {
            wirebox_base(hh=pi_mount_h, x1=-pi_mount_l+4, x2=-4, y1=-pilltray_width/2+4, y2=pilltray_width/2-4, bt=4);
            ty(pi_xoffset) {
                rfy() ty(24.5) tx(-4) tx([-4, -62]) {
                    cylx(0, 2, d=6);
                }
            }
            ty(pi_xoffset)
            translate(pi_button_pcb_pos) {

                pi_button_xform() {
                    cylx(-pi_button_pcb_pos.z, 0, d=5);
                }

                for (pt = autopill_led_buttons_drill_2_7_npth) translate(pt) {
                    cylx(-pi_button_pcb_pos.z, 0, d=6.6);
                }

                *difference() {
                    tz(-pi_button_pcb_pos.z)
                    linear_extrude(pi_button_pcb_pos.z, convexity=4) polygon(button_pcb_points);

                    pi_button_xform() {
                        cubex(z1=-2, z2=eps, xc=0, yc=0, xs=8.6, ys=7);
                    }

                    *iter_drill_holes(autopill_led_buttons_drill, min_hole_size=0.5) {
                        if ($hole_size < 2.6) {
                            hs = max($hole_size + 0.2, 4.5);
                            cylx(-2, eps, d=hs, $fn=16);
                            *cubex(z1=-2, z2=eps, xc=0, yc=0, xs=hs, ys=hs);
                        }
                    }
                }
            }
            //
        }


        ty(pi_xoffset) {
            // pi ports
            cubex(yc=0, ys=pi_w, x1=-pi_mount_l-eps, x2=-pi_mount_l+4+eps, z1=0, z2=pi_mount_h+eps);

            // sd card
            *cubex(yc=0, ys=16, x1=-1, x2=-15, z1=-2, z2=pi_mount_h+eps);
            cubex(yc=0, ys=16, x1=1, x2=-15, z1=-2, z2=5);

            // pi screw holes
            rfy() ty(24.5) tx(-4) tx([-4, -62]) {
                if (threaded_inserts) {
                    cylx(-3-eps, 2+eps, d=insert_dia_m2_5);
                } else {
                    cylx(-4-eps, 2+eps, d=screw_diameter_m2);
                    tz(-4-eps)
                    hexnut_hole(h=3, d=nut_diameter_m2);
                }
            }
            translate(pi_button_pcb_pos) {
                for (pt = autopill_led_buttons_drill_2_7_npth) translate(pt) {
                    cylx(-insert_length_m2_5, eps, d=insert_dia_m2_5);
                    cylx(-pi_button_pcb_pos.z, eps, d=screw_diameter_m2);
                }
            }
        }


        // mount hole cut through button pcb mount
        ty(pilltray_width/2-7) tx(-48) {
            cylx(0, pi_button_pcb_pos.z + eps, d=6);
        }

        // power port
        tx(-15)
        ty(pilltray_width/2-4-eps)
        xzy() linear_extrude(4+eps2)
        squarex(x1=0, xs=8, y1=0, ys=12);
        *polygon([[-6, 1], [6, 1], [6, 7], [.5, 12], [-.5, 12], [-6, 7]]);

        rfy() ty(pilltray_width/2-7) tx([-22, -56]) {
            tz(-10)
            countersink_m2(10, expand=1);
        }
    }

    if (!threaded_inserts)
    ty(pi_xoffset) {
        rfy() ty(24.5) tx(-4) tx([-4, -62]) {
            tz(-4+3-eps) bridge_support();
        }
    }
}

module preview_pi_mount(preview_lid=preview_lid, preview_dpad=true) {
    rz(-90) {
        ty(pi_xoffset) {
            tz(2)
            rz(180)
            ty(-29.6)
            tx(1.7)
            tz(-1.8)
            color("green", 0.5)
            import("rpi3b.stl", convexity=10);

            color("#333366")
            render(5)
            tx(-8)
            tz(3.4)
            ty(24.5) tx([0, -58])
            rz(90)
            display_spacer();


            *color("blue", 0.5)
            tx(-5) tz(pi_display_z)
            cubex(x1=0, x2=-pi_display_l, yc=0, ys=pi_display_w, z1=0, z2=-6.06);


            if (preview_pcb) {
                translate(pi_led_pcb_pos)
                translate(-autopill_led_buttons_pos_J1)
                pcb_model_led(traces=true, components=true);

                translate(pi_button_pcb_pos) {
                    pcb_model_buttons(traces=true, components=true);
                    if(preview_dpad)
                    translate(pi_button_pos[4]) tz(pi_mount_h - pi_button_pcb_pos.z) {
                        pi_button_dpad();
                        preview_pi_button_dpad();
                    }
                    tz(1.6-eps2) color("blue", 0.4) render(10) button_pcb_cover();
                    *pi_button_xform() {
                        tz(pi_mount_h - pi_button_pcb_pos.z)
                        color("#cccccc")
                        pi_button();
                    }
                }
            }
        }

        if (preview_lid)
        tz(pi_mount_h) color("#4595b6", 0.5) render(10) pi_mount_lid();
    }
}

module led_xform() {
    translate(vxy(pi_led_pcb_pos))
    translate(-autopill_led_buttons_pos_J1) {
        for (pt = [
            autopill_led_buttons_pos_D1,
            autopill_led_buttons_pos_D2,
            autopill_led_buttons_pos_D3,
            autopill_led_buttons_pos_D4,
        ]) translate(pt) {
            ty(-1.27)
            children();
        }
    }

}

module pi_mount_lid() {
    difference() {
        color("#333333")
        wirebox_lid_base(x1=-pi_mount_l, x2=0, y1=-pilltray_width/2, y2=pilltray_width/2);

        ty(pi_xoffset) {
            tx(-5) {
                cubex(x1=0, x2=-pi_display_l-2, yc=0, ys=pi_display_w+4, z1=-eps, z2=3);
                cubex(x1=-1, x2=-pi_display_l+6, y1=-pi_display_w/2+2.2, y2=pi_display_w/2-1, z1=-eps, z2=4+eps);
            }

            // leds
            *ty(-34) tx([for (x=[-68 : 2.54*4 : -30]) x])  {
                cylx(-eps, 4+eps, d=5.2);
            }

            led_xform() {
                cylx(-eps, 4+eps, d=5.2);
            }
            //buttons
            translate(pi_button_pcb_pos + pi_button_pos[4])
            pi_button_dpad_base(.4, zfc=0.8);


            *let(d=8)
            translate([pi_button_pcb_pos.x, pi_button_pcb_pos.y]) {
                pi_button_xform() {
                    cylx(-eps2, 2+eps, d1=d+2, d2=d);
                    cylx(2, 4+eps, d=d);
                }
            }

        }
    }
}

module preview_pi_mount_lid() {
    tz(eps) pi_mount_lid_inlay_front();

    ty(pi_xoffset)
    translate(pi_button_pcb_pos + pi_button_pos[4]) {
        color("#eeeeee") render(10) pi_button_dpad();
        color("#333333") tz(eps) render(10) pi_button_dpad_inlay();
    }

    *color("red", 0.3)
    pi_mount_lid_inlay_led();
}

module pi_mount_lid_inlay_led() {
    ty(pi_xoffset) {
        led_xform() {
            cylx(4 - .4, 4+eps, d=5.2);
        }
    }
}

module pi_button_dpad() {
    //pi_button_dpad_base(-.1, 0, [90, 270]);
    pi_button_dpad_base(-.1, 0, [], bridge=false);
}

module preview_pi_button_dpad() {
    color("#333333")
    tz(eps2) render(10) pi_button_dpad_inlay();
}

module pi_button_xform() {
    for (pt = pi_button_pos) translate(pt) {
        children();
    }
}

module pi_button_dpad_base(fc=0, zfc=0, center_bridge_ang=[0, 90, 180, 270], bridge=true) {
    base = pi_button_pos[4];
    rad = dpad_rad;
    rofs1 = 3.5;
    rofs2 = 4;

    if_z = 0.6 + zfc;
    if_z2 = if_z - .2;

    difference() {
        union() {
            *cylx(0, 1, r1=rad + rofs1 + 1 + fc, r2 = rad + rofs1 + fc);
            cylx(0, button_h, r = rad + rofs1 + fc);
        }
        *cylx(-eps, 1, r1=rad - rofs2 - 1 - fc, r2 = rad - rofs2 - fc);
        cylx(-eps, button_h+eps, r = rad - rofs2 - fc);

        for (ang = [0, 90, 180, 270]) rz(45 + ang) {
            cubex(
                x1 = rad - rofs2 - 4, x2 = rad + rofs1 + 2,
                yc=0, ys=3.5 - 2*fc,
                z1=bridge ? if_z : -eps, z2 = button_h+eps
            );
            cubex(
                x1 = rad - rofs2 - 4, x2 = rad,
                yc=0, ys=3.5 - 2*fc,
                z1=-eps, z2 = button_h+eps
            );
            cubex(
                x1 = rad + rofs2 - 1.5, xs = 5,
                yc=0, ys=3.5 - 2*fc,
                z1=-eps, z2 = button_h+eps
            );
        }
    }

    for (ang = center_bridge_ang) rz(ang) {
        cubex(
            x1 = rad - rofs2 - 4, x2 = rad - rofs2 + 2,
            yc=0, ys=2.6 + 2*fc,
            z1=0, z2=if_z2,
        );
    }

    if (bridge)
    translate(-base)
    let(pt1=pi_button_pos[5])
    translate(pt1)
    //for (pt2 = [/*pi_button_pos[1] + [0, 1],*/ pi_button_pos[2] + [1.6, 0]]) {
    let(pt2 = base + [10, 10]) {
        ofs = pt2 - pt1;
        len = hypot(ofs.x, ofs.y);

        rz(atan2(ofs.y, ofs.x))
        cubex(x1=0, x2=hypot(ofs.x, ofs.y), yc=0, ys=2.6 + 2*fc, z1=0, z2=if_z2);
    }


    let(r1 = rad - rofs2 - 3)
    for (i = [4, 5]) translate(pi_button_pos[i] - base)
    difference() {
        union() {
            cylx(0, 1, r1=r1 + 1 + fc, r2 = r1 + fc);
            cylx(1-eps, button_h, r = r1 + fc);
        }
    }

    for (pt = pi_button_pos) translate(pt - pi_button_pos[4]) {
        cubex(xc=0, yc=0, xs=3.6, ys=3.6, z1 = -pi_mount_h + button_z + .4, z2=1);
    }
}

module button_pcb_cover() {
    hh = pi_mount_h - 1.6 - pi_button_pcb_pos.z - 1;

    cover_pts_raw = [
        [53.2, 43.2],
        [65.2, 43.2],
        [65.2, 31.2],
        [82.1, 31.2],
        [82.1, 26.4],
        [91.6, 26.4],
        [91.6, 26.4],
        [91.6, 37.7],
        [86.7, 37.7],
        [86.7, 54.7],
        [74.7, 54.7],
        [74.7, 66.5],
        [65.2, 66.5],
        [65.2, 54.7],
        [53.2, 54.7],
    ];

    cover_pcb_points = [for (pt = cover_pts_raw) [pt.x, -pt.y] - autopill_led_buttons_origin];

    btn_z = button_z - pi_button_pcb_pos.z - 1.6;

    //slice(axis="x", vc=47.5, vs=1)
    render(10)
    difference() {
        union() {
            color("blue")
            linear_extrude(hh, convexity=10) polygon(cover_pcb_points);
            linehull(autopill_led_buttons_drill_2_7_npth) cylx(0, hh, d=7);

        }
        *for (pt = autopill_led_buttons_drill_2_7_npth) translate(pt) {
            cylx(1, 20, d=6);
        }
        pi_button_xform() {
            cubex(
                xc=0, yc=0, xs=7, ys=9,
                z1 = -eps, z2=btn_z
            );
            cubex(
                xc=0, yc=0, xs=4, ys=4,
                z1 = -eps, z2=hh + eps
            );

            *intersection_for(ang = [0, 90]) rz(ang)
            //cylx(-1, 20, d=3.5);
            xzy() lxpoly(8, center=true, convexity=4, points=[
                [-4, -8],
                [-4, 4],
                [-2, 6],
                [-2, 20],
                [2, 20],
                [2, 6],
                [4, 4],
                [4, -8],

            ]);
        }

        for (pt = autopill_led_buttons_drill_2_7_npth) translate(pt) {
            countersink_m2(hh, head_extra=hh - 3);
            *cylx(1, 20, d=6);
            cylx(-eps, 1+eps, d=screw_diameter_m2);
        }
    }

}

module pi_mount_lid_inlay_2d() {
    tx(-92)
    rz(90)
    scale([.6, .6])
    ty(12)
    logo(0);
}

module pi_mount_lid_inlay_front() {
    color("gold")
    tz(4-.4) {
        linear_extrude(.4+eps) {
            pi_mount_lid_inlay_2d();
        }
    }
}

module pi_mount_lid_inlay_back() {
    color("white")
    tz(4-.6) {
        linear_extrude(.2) {
            offset(-15)
            offset(16)
            pi_mount_lid_inlay_2d();
        }
    }
}

module pi_button_inlay(btn, r=0) {
    translate(pi_button_pos[btn] - pi_button_pos[4])
    tz(button_h - .2) linear_extrude(.2, convexity=10) {
        rz(r) children();
    }
}

module pi_button_inlay_text(xx, yy, txt, r=0) {
    pi_button_inlay(xx, yy, r) text(txt, 5, "DejaVu Serif:style=Bold", valign="center", halign="center");
}

module pi_button_arrow(btn, r) {
    pi_button_inlay(btn, r) offset(.4) {
        tx(-2) {
            linehull([[0, 0], [4, 0]]) circle(d=.01);
            linehull([[0, 0], [2, 2]]) circle(d=.01);
            linehull([[0, 0], [2, -2]]) circle(d=.01);
        }
    }

}

module pi_button_dpad_inlay() {
    pi_button_arrow(0, 0);
    pi_button_arrow(1, 270);
    pi_button_arrow(2, 180);
    pi_button_arrow(3, 90);
    pi_button_inlay(4, 0) {
        difference() {
            circle(d=5.2);
            circle(d=3.2);
        }
    }
    pi_button_inlay(5, 90) offset(.4) {
        ty(-.5)
        tx(-2) {
            translate([3, 1]) linehull([[-3, -1], each arcpts(1, -90, 90, 10), [-1, 1]]) circle(d=.01);
            linehull([[0, 0], [1, 1]]) circle(d=.01);
            linehull([[0, 0], [1, -1]]) circle(d=.01);
        }

    }
}

module display_spacer() {
    difference() {
        union() {
            *cubex(x1=-1, x2=1, y1=3, y2=12, z1=0, z2=16.2);
            *cubex(x1=-9, x2=1, yc=2, ys=2, z1=0, z2=16.2);

            cylx(0, 4, d=screw_head_diameter_m2);
            cylx(4, 8, d1=screw_head_diameter_m2, d2=7.8);
            cylx(8, 16.2, d=7.8);

            *cubex(x1=0, x2=6, y1=0, y2=6, z1=0, z2=2);
        }
        *ty(7)
        rx(-45)
        cubex(xc=0, xs=10, y1=0, y2=10, zc=0, zs=50);
        tz(-2) countersink_m2(30, head_extra=20, expand=1.2, nut_length=2.5);
    }
}
