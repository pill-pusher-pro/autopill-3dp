include <ktpanda/consts.scad>
use <ktpanda/funcs.scad>
use <ktpanda/partbin.scad>
use <ktpanda/shapes.scad>
use <kicad-models/kicad.scad>
use <kicad-models/kicad-models.scad>

function sqr(x) = x * x;
function vxy(v, z=0) = [v.x, v.y, z];

module marker_text(text, size=4, halign=undef, valign=undef) {
    offset(.1, $fn=15)
    text(text, size, "Quicksand:style=Bold", spacing=1.2, halign=halign, valign=valign);
}

fit_clearance = 0.2;
