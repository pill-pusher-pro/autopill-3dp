highres = True

imgsize = [1080, 1080]

camera_dist = 400
camera_pos = [-12, 0, 24]
camera_rot = [65, 0, 13]

pill_length = 10
pill_startz = 120

pill_pincher_z = pill_length * 1.75

defvar('opacity', 1.0, speed=.8)

defvar('servo_ang', 40, speed=170)
defvar('pincher_stop')


pills = []

opacity.move(ev=0.2)

pill_colors = ["#0000ff", "#00ff00", "#00ffff", "#ff0000", "#ff00ff", "#cccccc", "#ffff00", "#ff9900"]

for i, clr in enumerate(pill_colors):
    pill = defvar(f'pill{i+1}', pill_startz + i * pill_length, speed=80)
    pill.yrot = defvar(f'pill{i+1}_yrot', 0)
    pill.pinched = 0 if i == 0 else defvar(f'pill{i+1}_pinched', 0)
    pill.color = clr
    pills.append(pill)

allpills = list(pills)

dist = pill_pincher_z - pill_startz

for p in pills:
    p.move(dv=dist, speed=130)

wait(pill1)

wait(.5)

cpill = 0
while pills:
    cpill += 1

    slow = cpill < 4
    servo_ang.speed = 150 if slow else 450

    # move servo to point where pincher releases
    wait(servo_ang.move(ev=22))

    # pills fall to bottom
    for i, p in enumerate(pills):
        p.move(ev=i * pill_length)

    # lowest pill starts to rotate
    pills[0].yrot.move(ev=8 if pills[0].pinched == 0 else 12, dt=.2)

    # continue moving servo all the way in
    wait(servo_ang.move(ev=0))

    if slow:
        wait(.3)
    else:
        wait(.2)

    # wait for pills to fall
    wait(pills[0])

    if len(pills) > 1:
        # set second pill to be pushed by pincher
        pills[1].pinched.snap(1)
        pincher_stop.snap(1)
    else:
        pincher_stop.snap(0)

    # move servo to neutral position
    wait(servo_ang.move(ev=40))

    if len(pills) > 1:
        # set second pill to stay in pushed position
        pills[1].pinched.snap(2)

    if slow:
        wait(.3)

    # move servo to point where pill will start to fall
    wait(servo_ang.move(ev=70))

    # start pill moving down off the screen
    pills[0].move(ev=-100)

    # continue moving servo all the way out
    wait(servo_ang.move(ev=90))

    # last pill, start fading cutout back in
    if len(pills) == 1:
        opacity.move(ev=1.0)

    wait(.4)

    # move back to neutral
    wait(servo_ang.move(ev=40))

    if slow:
        wait(.5)

    del pills[0]


wait(.8)

scad(r'''
include <common.scad>

include <autopill-main-pcb-model.scad>
include <autopill-dispenser-dual-pcb-model.scad>
include <autopill-dispenser-single-pcb-model.scad>
include <autopill-led-buttons-pcb-model.scad>
''')

scad(f'highres = {toscad(highres)};\n')

scad(r'''
$fn = (!highres && $preview) ? 30 : 128;

planetgear_hgr = true;
pill_width = 6;
pill_thick = 6;
pill_len = 10;
pincher = true;
dispenser_connector_vertical = true;
pillgear_old_interface = false;
riser_height = 1;
slide_insert_length = 1;
threaded_inserts = true;
hopper_window = true;
dual_dispenser_pcb = true;

preview_spring = true;

include <params.scad>

include <wirebox.scad>
include <dispenser.scad>
include <dispenser-tube.scad>
include <models.scad>
''')

generate(xdefs=[
    f'$pills = {toscad([[p, p.pinched, p.yrot, p.color] for p in allpills])};',
])

scad(r'''

dispenser_animate(spring=preview_spring);

''')
