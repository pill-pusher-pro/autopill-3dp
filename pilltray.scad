pilltray_curve_pts = [
    //[[pilltray_x1 + 4 - eps, 16], [0, 0], [0, 0]],
    [[pilltray_x1 + 8 - eps, 16], [5, -3]],
    [[pilltray_x1 + 25, 10], [6, -1]],
    [[(pilltray_x1 + dispbase_x2) / 2, 8], [10, 0]],
    [[dispbase_x2 - 30, 10], [10, 2]],
    [[dispbase_x2 + 2, 25], [4, 5]],
];


module pilltray_outline(x2ofs=0) {
    dx1 = pilltray_x1 + 4;
    dx2 = dispbase_x2 + 4;
    pw = (pilltray_width - 8.4)/2;
    corner_ofs = 40;
    polygon([[dx1+corner_ofs, -pw],
        [dx1, -pw+corner_ofs],
        [dx1, pw-corner_ofs],
        [dx1+corner_ofs, pw],
        [dx2+x2ofs, pw],
        [dx2+x2ofs, -pw],
    ]);
}

module bezier_dbg(cpts, npts, scl=1) {
    for (pt = cpts) {
        curvept = pt[0];
        cp1 = pt[1];
        cp2 = is_undef(pt[2]) ? pt[1] : pt[2];
        color("blue") translate(curvept) cylx(0, 1, d=1*scl);
        color("green") translate(curvept + cp1) cylx(0, 2, d=.5*scl);
        color("green") translate(curvept - cp2) cylx(0, 2, d=.5*scl);
    }
    for (pt = bezierpts(cpts, npts)) {
        color("red") translate(pt) cylx(0, 3, d=.2*scl);

    }
}

module pilltray() {
    sfact = 0.6;
    difference() {
        tz(4) linear_extrude(pilltray_height-4) offset(4) offset(-4) pilltray_outline();

        intersection() {
            union() {
                tz(4) linear_extrude(pilltray_height-4+eps) offset(2) offset(-6) pilltray_outline(10);
                cubex(
                    x1=pilltray_x1 + 4 - eps, xs = 4 + 3*eps,
                    yc = 0, ys = 18,
                    z1 = 0, zs = pilltray_height + eps
                );
            }

            xzy() linear_extrude(pilltray_width, center=true) polygon([
                [pilltray_x1 + 4 - eps, pilltray_height + eps],
                [pilltray_x1 + 4 - eps, 16],
                each bezierpts(pilltray_curve_pts, 30),
                [dispbase_x2 + 4 + eps, 25],
                [dispbase_x2 + 4 + eps, pilltray_height + eps],
            ]);

            *tz(6)
            sz(sfact)
            xzy()

            cubex(x1=pilltray_x1+8, x2=dispbase_x2 + 6,
                zc=0, zs=pilltray_width-12,
                y1=0, y2=pilltray_height*2+eps,
            r2=60, r1=40);
        }

        dx1 = pilltray_x1 + 4 - eps;
        dx2 = 4.9;
        ofs = 30;
        yy = 10;

        mat1 = [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [-.2, 0, 1, 0],
            [0, 0, 0, 1]
        ];

        mat2 = [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [-.9, 0, 1, 0],
            [0, 0, 0, 1]
        ];

        tx(dx1) {
            *multmatrix(mat1)
            tz(16) linear_extrude(pilltray_height, convexity=4)
            polygon([[0, yy],
                [0, -yy],
                [dx2, -yy],
                [dx2+ofs, -yy-ofs],
                [dx2+ofs, yy+ofs],
            [dx2, yy]]);

            multmatrix(mat2)
            tz(14)
            cubex(x1=0, x2=dx2+ofs, yc=0, ys=pilltray_width, z1=-pilltray_height, z2=0);
        }

        // debug
        *color("pink")
        cubex(xc=0, zc=0, xs=200, zs=200, y1=0, y2=-100);
    }
}

module preview_pilltray() {
    ty(-pilltray_width/2)
    xzy()
    bezier_dbg(pilltray_curve_pts, 20, 1);
}

module slide_curve_tri(ofs = -4) {
    dd = (slide_tube_dia + ofs) / 2;
    pts = [
        [0, -dd],
        [-dd, 0],
        //[0, dd],
        [0, dd],
        [dd, 0]
    ];
    slide_curve_base(pts);
}

module slide_curve_round(ofs = -4) {
    dd = (slide_tube_dia + ofs) / 2;
    pts = [
        [0, dd],
        [-dd, 0],
        each (arcpts(dd / sqrt(2), -90 - 45, -45)),
        [dd, 0],
    ];
    slide_curve_base(pts);
}

module slide_curve_extbot(ofs = -4) {
    dd = (slide_tube_dia + ofs) / 2;
    pts = [
        [dd, -dd - 40],
        [-dd, -dd - 40],
        [-dd, 0],
        //[0, dd],
        [0, dd],
        [dd, 0]
    ];
    slide_curve_base(pts, false);
}

module slide_curve_base(pts, full=true) {
    rad = 46;
    rad2 = 50;

    offset = module_rise;


    render(10)
    tz(module_rise_rear + module_rise_initial) tx(-dispbase_row_spacing + dispbase_x2 + dispbase_x1)
    tz(slide_tube_dia/2 + slide_tube_height)
    rfy() {
        ty(-pilltray_width/2)
        tx(rad)
        sx(-1)
        helix_extrude(pts, ofs=[rad, 0], angle=90, cofs=[0, -offset*4], $fn=highres ? 300 : 50);

        tx(rad)
        ty(-pilltray_width/2 + rad)
        tz(-rad2 - offset)
        multmatrix([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [-.3, 0, 1, 0],
            [0, 0, 0, 1],
        ])

        if (full)
        xzy() rz(90) sy(-1)
        helix_extrude([for (pt = pts) rotpt(pt, -90)], ofs=[rad2, 0], angle=90, $fn=highres ? 300 : 50);

    }
}

module pilltray_holder_split(fc=0) {
    cubex(
        x1 = pilltray_holder_x1 - 80, x2 = dispbase_x2,
        yc = 0, ys = pilltray_width + 30,
        z1=pilltray_holder_height, z2 = pilltray_holder_pi_mount_z
    );
}

module pilltray_holder_interface_tab(inner=false, fc=0) {
    lxpoly(4 + (inner ? eps : 0), points= [
        [-4 - fc, -eps],
        [-4 - fc, 6 + fc],
        [0, 10 + fc],
        [4 + fc, 6 + fc],
        [4 + fc, -eps],
    ]);
}

module pilltray_holder_interface(inner=false) {
    fc = inner ? .2 : 0;

    tz(pilltray_holder_height)
    rfy()
    ty(pilltray_width/2 - 4)
    tx([pilltray_holder_x1 + 22, pilltray_pi_x1 + 64 - 10])
    xzy() {
        difference() {
            pilltray_holder_interface_tab(inner, fc);
            if (!inner) ty(4) countersink_m2(4.4);
        }
        if (inner) ty(4) {
            cylx(-insert_length_m2_5, eps, d=insert_dia_m2_5);
            cylx(-10, eps, d=screw_diameter_m2);
        }
    }
}


pilltray_holder_rear_slide_t1 = [0, 0, -4];
pilltray_holder_rear_slide_ry = -19;
pilltray_holder_rear_slide_t2 = [-pilltray_holder_x1, 0, -module_rise_rear - 10];
pilltray_holder_rear_slide_screw = -pilltray_holder_rear_slide_t2 + [0, 0, 30];

module pilltray_holder_rear_slide_xform(dir=1) {
    if (dir == 1) {
        translate(pilltray_holder_rear_slide_t1)
        ry(pilltray_holder_rear_slide_ry)
        translate(pilltray_holder_rear_slide_t2)
        children();
    } else if (dir == -1) {
        translate(-pilltray_holder_rear_slide_t2)
        ry(-pilltray_holder_rear_slide_ry)
        translate(-pilltray_holder_rear_slide_t1)
        children();
    } else {
        children();
    }
}

module pilltray_holder_rear_slide_base() {
    intersection() {
        slide_curve_extbot(4);
        cubex(
            x1 = pilltray_holder_x1, xs = -100,
            yc = 0, ys = pilltray_width + 10,
            z1 = 0, z2 = 200
        );
    }
}

module pilltray_holder_rear_slide() {
    difference() {
        intersection() {
            union() {
                pilltray_holder_rear_slide_base();
                translate(pilltray_holder_rear_slide_screw)
                yzx() {
                    linehull([[0, 0], [0, -20]])
                    cylx(-5, 0, d=10);
                }
            }
            pilltray_holder_rear_slide_xform(-1)
            yzx() tz(-100) linear_extrude(100)
            let(bev=10)
            polygon([
                [-pilltray_width/2 + bev, 0],
                [-pilltray_width/2, bev],
                [-pilltray_width/2, 200],
                [pilltray_width/2, 200],
                [pilltray_width/2, bev],
                [pilltray_width/2 - bev, 0],
            ]);

        }
        slide_curve_round();

        translate(pilltray_holder_rear_slide_screw) yzx() sz(-1) {
            countersink_m2(5);
            ty(-18) countersink_m2(35, head_extra=23, expand=1.2);
        }

    }
    *color("blue")
    linear_extrude(.1)
    union() {
        projection(cut=false)
        pilltray_holder_rear_slide_xform()
        pilltray_holder_rear_slide_base();
        squarex(yc=0, ys = 62, x1 = -15, x2 = 0);
    }
}
module pilltray_holder(screws=true) {
    difference() {
        pilltray_holder_base();
        pilltray_holder_split();
        cubex(
            x1=pilltray_holder_x1+4, x2=dispbase_x2+4, yc=0, ys=pilltray_width-8,
            z1=4, z2=pilltray_holder_height + eps,
            r1=4, r3=4
        );
        cubex(
            x1=pilltray_pi_x1 + 64, x2=dispbase_x2+4, yc=0, ys=pilltray_width-8,
            z1=pilltray_holder_height - eps, z2=pilltray_holder_pi_mount_z+eps,
        );

        if (screws) tz(4) tx(pilltray_holder_x1 - 6 + eps) yzx() mainbox_mount();
    }
    pilltray_holder_interface();
}

module pilltray_holder_top() {
    difference() {
        intersection() {
            union() {
                pilltray_holder_base(pilltray_holder_x1);
                *slide_curve_tri(4);

            }
            pilltray_holder_split();
        }
        pilltray_holder_interface(true);

        tz(pilltray_holder_pi_mount_z - 60)
        tx(pilltray_pi_x1 + 64) ry(45) {

            rfy() ty(pilltray_width/2-4)
            tx(-14) tx([-6, -40]) {
                ty(-3) {
                    cylx(-20, eps, d=screw_diameter_m2+.3);
                    cylx(-insert_length_m2_5, eps, d=insert_dia_m2_5);
                }

            }
        }
        slide_curve_round(-2.8);

        translate(pilltray_holder_rear_slide_screw) yzx() {
            ty([0, -18]) {
                cylx(-eps, insert_length_m2_5, d=insert_dia_m2_5);
                cylx(-eps, 8, d=screw_diameter_m2);
            }
        }
    }

    rear_x = pilltray_holder_x1 + dispbase_row_spacing;
    module_base_x = rear_x - 22;

    tx(-dispbase_row_spacing)
    rfy() ty(pilltray_width/2) {
        cubex(
            y1=-8, y2=0,
            x1=rear_x, x2=module_base_x,
            z1=pilltray_holder_height,
            z2=module_rise_rear + module_rise_initial
        );


        tz(module_rise_rear)
        intersection() {
            union() {
                cubex(
                    y1=0, y2=20,
                    x1=module_base_x, x2=dispbase_x2,
                    z1=pilltray_holder_height - module_rise_rear,
                    z2=module_rise + 20
                );
            }
            module_interface_up(false, module_interface_screws=[module_interface_screws[2]]);
        }
    }
}

module preview_pilltray_holder_top() {
    color("#01ea26", 0.5)
    render(10)
    pilltray_holder_rear_slide();
}

module pilltray_holder_top_aligntool_1() {
    difference() {
        cubex(
            yc=0, ys=35,
            xc=0, xs=16,
            z1=0, z2=22
        );
        tz(16) ry(-45)
        cubex(
            yc=0, ys=40,
            x1=0, xs=16,
            z1=0, z2=22
        );
    }
}

module preview_pilltray_holder_top_aligntool_1() {
    tz(16)
    ry(-45)
    translate([-pilltray_holder_x1, 0, -pilltray_holder_height])
    color("blue", 0.4)
    render(10)
    pilltray_holder_top();
}

module pilltray_holder_top_aligntool_2() {
    cubex(
        y1=-30, y2=0,
        x1=15, xs=66,
        z1=0, z2=8
    );

    translate([-pilltray_holder_x1, 0, 8 + 4])
    rx(180)
    tx([pilltray_holder_x1 + 22, pilltray_pi_x1 + 64 - 10])
    pilltray_holder_interface_tab();
}

module preview_pilltray_holder_top_aligntool_2() {
    tz(pilltray_width/2 + 8)
    rx(90)
    translate([-pilltray_holder_x1, 0, -pilltray_holder_height])
    color("blue", 0.4)
    render(10)
    pilltray_holder_top();
}

module pilltray_holder_base(x1=pilltray_holder_x1) {
    difference() {
        union() {
            cubex(
                x1=x1, x2=dispbase_x2, yc=0, ys=pilltray_width,
                z1=0, z2=pilltray_holder_pi_mount_z - 18,
                r1=0, r2=4, r3=0, r4=4
            );

            rfy() ty(pilltray_width/2) {
                module_interface_up(true);
            }

        }

        rfy() ty(pilltray_width/2) riser_interface_out_inner();

        z1 = pilltray_holder_pi_mount_z - pilltray_holder_height;
        tz(pilltray_holder_height)
        xzy() linear_extrude(pilltray_width+eps, center=true)
        polygon([[pilltray_pi_x1 + 64, 0],
            [pilltray_pi_x1 + 64, z1-60],

            [pilltray_pi_x1 + 4, z1+eps],
            [dispbase_x2, z1+eps],
            [dispbase_x2, 0],
        ]);

        tz(module_rise_initial)
        rfy() {
            intersection() {
                ty(pilltray_width/2 - dispbase_y2)
                tz(-module_rise + slide_tube_height)
                pill_slide_cut();

                cubex(xc=0, xs=50,
                    y1=pilltray_width/2 - 4 - eps, y2=pilltray_width/2 + eps,
                z1=0, z2=pilltray_holder_height+eps);
            }

        }


    }
}

module preview_pilltray_holder() {
    if (preview_pilltray)
    color("green", 0.5)
    render(10)
    tz(eps)
    pilltray();

    ty(-mainbox_xcenter)
    tz(4) tx(pilltray_holder_x1 - 4 - eps) //ry(-90) rz(-90)
    rz(90) {
        color("#0126ea", 0.5)
        render(10)
        mainbox();

        if (preview_lid)
        tz(mainbox_h)
        color("#8a00ed", 0.5)
        render(10)
        mainbox_lid();

        translate(main_pcb_pos + autopill_main_pos_J8 + 2.54 * [-.5, 2]) {
            tz(mainbox_h+ 4)
            rz(90)
            idc_model();
        }
    }

    color("#0126ea", 0.5)
    render(10)
    pilltray_holder_top();

    color("#01ea26", 0.5)
    render(10)
    pilltray_holder_rear_slide();


    ty(-(pilltray_width/2 - dispbase_y1 + eps)) {
        color("#0066ff", 0.5)
        render(10)
        riser_dbl_l(1);

        ty(dispbase_y1 - dispbase_y2)
        color("#0066ff", 0.5)
        render(10)
        riser_dbl_l(2);
    }

    ty(pilltray_width/2 - dispbase_y1 + eps)
    color("#0066ff", 0.5)
    render(10)
    riser_dbl_r(1);

    tz(module_rise_initial)
    for (flip = [0, 1]) sy(flip ? -1 : 1){
        tz(eps)
        ty(pilltray_width/2 - dispbase_y1 + eps) {
            color("blue", 0.5)
            render(10) basemodule();
            *preview_basemodule(flip ? -1 : 1);


        }
        tz(module_rise_rear + eps) tx(-dispbase_row_spacing + dispbase_x2 + dispbase_x1)
        ty(pilltray_width/2 - dispbase_y1 + eps) {
            tz(slide_tube_dia/2)
            slide_insert(xform=false, endcap = false);

            color("blue", 0.5)
            sx(-1) render(10) basemodule();
            *preview_basemodule(flip ? -1 : 1);

        }
    }

    tz(pilltray_holder_pi_mount_z - 60)
    tx(pilltray_pi_x1 + 64) ry(45) {

        color("#8a00ed", 0.5)
        render(10)
        tx(2)
        tz(4+eps)
        pi_mount();
    }
}

module pilltray_holder_mainbox() {
    pilltray_holder(false);
    ty(-mainbox_xcenter)
    tz(4) tx(pilltray_holder_x1 - 4)
    rz(90) {
        mainbox(false);

    }
}

module preview_pilltray_holder_mainbox() {
    ty(-pilltray_width/2 - dispbase_y2)
    color("green", .5)
    riser_dbl_l(1);

    ty(pilltray_width/2 + dispbase_y2)
    color("green", .5)
    riser_dbl_r(1);
}
