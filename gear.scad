function polarpt(r, t) = [r * cos(t), r * sin(t)];

function _triwave(t) = 2*(t < 1 ? t : 2 - t) - 1;
function triwave(t) = _triwave(2*(t - floor(t)));
function gearpts(s, gs, n, phase=0) = [
    for (t=[0:360/(n * (highres ? 20 : 10)):360]) polarpt(s  + gs * triwave(t*n/360), t + phase)
];


module gear(s, gs, n) {
    //linear_extrude(t)
    polygon(gearpts(s, gs, n));
    //circle(r=s);
}


module ringgear(is, igs, in, os, ogs, on, iphase=0, ophase=0) {
    opts = is_undef(ogs) ? [
        [-os, -os],
        [-os, os],
        [os, os],
        [os, -os],
    ] :gearpts(os, ogs, on, ophase);

    ipts = gearpts(is, igs, in, iphase);
    points = [ each opts, each ipts ];
    paths = [
        [ for(i=[0 : 1 : len(opts)-1]) i ],
        [ for(i=[0 : 1 : len(ipts)-1]) i+len(opts) ]
    ];
    polygon(points, paths);
}

function _squaregear(ang1, ang2, ang3, ra, rb) = [
    rotpt([0, rb], ang1),
    rotpt([0, rb], ang2),
    rotpt([0, ra], ang2),
    rotpt([0, ra], ang3),
];

function squaregear(ra, rb, ng, gp) = flatten([
    for (i = [0 : ng - 1]) _squaregear(i*360/ng, (i + gp) * 360/ng, (i + 1) * 360/ng, ra, rb)
]);

module planet_transform(ang) {
    rz(ang)
    tx(pg_carrier_gear_size)
    //rz(180 + ang * pg_sun_gear_teeth / pg_planet_gear_teeth)
    rz(-ang)
    children();
}

module planet_transform_2(ang) {
    rz(ang)
    tx(pg_carrier_gear_size)
    //rz(180 + ang * pg2_sun_gear_teeth / pg2_planet_gear_teeth)
    rz(-ang)
    children();
}

module pg_sun_gear(h, twist, slices) {
    color("blue")
    rfz(h/2)
    linear_extrude(h/2, twist=twist*pg_planet_gear_teeth/pg_sun_gear_teeth, convexity = 3, slices=slices)
    gear(pg_sun_gear_size - pg_fit_clearance, pg_gear_size * pg_scale, pg_sun_gear_teeth);

}

module pg_planet_gear(h, twist, slices) {
    rfz(h/2)
    color("yellow")
    linear_extrude(h/2, twist=-twist, convexity = 3, slices=slices)
    gear(pg_planet_gear_size - pg_fit_clearance, pg_gear_size * pg_scale, pg_planet_gear_teeth);

}

module pg2_planet_gear(h, twist, slices, rotate) {
    render(3) intersection() {
        let(br = pg_planet_gear_size - 1.2, zs=2)
        union() {
            cylx(0, zs, r1=br, r2=br + zs);
            cylx(zs, 3.2, r=br + zs);
        }
        linear_extrude(3.6, convexity = 3)
        gear(pg2_planet_gear_size - pg_fit_clearance, pg_gear_size * pg_scale, pg2_planet_gear_teeth);
    }
}

module pg_ring_gear(h, twist, slices) {
    color("green")
    rz((360 / pg_ring_gear_teeth / 2) * (1 + pg_planet_gear_teeth))
    render(6) intersection() {
        rfz(h/2)
        linear_extrude(h/2, twist=-twist*pg_planet_gear_teeth/pg_ring_gear_teeth, convexity = 5, slices=slices)
        ringgear(pg_ring_gear_size + pg_fit_clearance, pg_gear_size * pg_scale, pg_ring_gear_teeth, pg_outer_size + 10, undef, undef);

        cylinder(h=h, r=pg_outer_size, $fn = $fn * 6);
    }
}

module pg_shim_arc(rad, ang1=-45, ang2=45, xofs1=6, xofs2=6) {
    polygon([
        rotpt([5, -xofs1], ang1),
        rotpt([rad, -xofs1], ang1),
        each arcpts(rad, ang1, ang2, ceil($fn * 135/360)),
        rotpt([rad, xofs2], ang2),
        rotpt([5, xofs2], ang2),

    ]);

}

module planetgear_interface(fc=0, cap=false) {
    for (ang = [0, 120, -120]) rz(ang) {
        xzy() lxpoly(2.4 - 2*fc, center=true, points=[
            [0, 0],
            [0, 3.2],
            [1.8 - fc, 3.2],
            [4.8 - fc, 0],
        ]);
    }

    cylx(0, 3.2, d=4.5 - 2*fc);
    if (cap)
    cylx(3.2, 5, d1=4.5 - 2*fc, d2 = 2 - 2*fc);
}

module planetgear_sun(h=10, twist=30) {
    slices = pg_flat ? 1 : 5;
    twist = pg_flat ? 0 : twist;

    rz(pg_rotate_print / pg_sun_gear_teeth)
    difference() {
        pg_sun_gear(h, twist, slices);

        tz(-eps)
        planetgear_interface(fc=0, cap=true);

        cylx(-eps, h+eps, d=screw_diameter_m2);

        cylx(h - 6, h+eps, d=insert_dia_m2_5);
    }
}

module planetgear_planet(h=10, twist=30) {
    slices = pg_flat ? 1 : 5;
    twist = pg_flat ? 0 : twist;

    rz(-pg_rotate_print / pg_planet_gear_teeth)
    difference() {
        union() {
            rz(180 + 180/pg_planet_gear_teeth)
            pg_planet_gear(h, twist, slices);
            if (!planetgear_hgr) {
                tz(h)
                cylinder(d=pg_hole, h=6);
            }

            if (planetgear_hgr)
            tz(h)
            pg2_planet_gear(h, twist, slices);
        }
        if (planetgear_hgr) {
            cylx(-eps, h + 5 + eps, d=pg_hole);
        }
    }
}

module planetgear_ring(h=10, twist=30, rotate=0) {
    slices = pg_flat ? 1 : 5;
    twist = pg_flat ? 0 : twist;

    rz(-pg_rotate_print / pg_ring_gear_teeth)
    pg_ring_gear(h, twist, slices);

    render(10)
    linear_extrude(h) {
        difference() {
            pg_shim_arc(pg_shroud_rad, xofs1=6, xofs2=6);
            circle(r=pg_outer_size - .5);
        }
    }

    difference() {
        tz(h)
        linear_extrude(-pg_z - h + 6, convexity=10) {
            difference() {
                pg_shim_arc(pg_shroud_rad, xofs1=6, xofs2=6);
                pg_shim_arc(pg_shroud_inner, xofs1=7, xofs2=7);
            }
        }
        tz(-pg_z)
        pillwheel_holder_screws(undef, [0, 45, -45, 90], pg_shroud_rad);
    }

    let(d=8)
    for (m=pg_mounts) let(ang=m[0], xx=m[1]) rz(ang+180) {
        difference() {
            union() {
                cubex(
                    x1 = pg_outer_size - .5, x2 = xx,
                    yc = 0, ys=d,
                    z1=0, z2=h
                );
                tx(xx) {
                    cylx(0, -pg_z, d=d);
                    *cylx(-pg_z, -pg_z + 2.4, d=3);
                }
            }
            tx(xx)
            tz(-eps)
            countersink_m2(-pg_z + eps2, inv=1);
        }
    }
}

module planetgear(h=10, twist=30, rotate=preview_pg_rot) {
    slices = pg_flat ? 1 : 5;
    twist = pg_flat ? 0 : twist;

    baserot = rotate * pg_ring_gear_teeth;
    s_rot = -baserot / pg_sun_gear_teeth;
    p_rot = baserot / pg_planet_gear_teeth;
    r_rot = baserot / pg_ring_gear_teeth;

    rz(s_rot - r_rot)
    planetgear_sun(h, twist);

    for (i = [0 : pg_num_planets - 1]) {
        txpt = rotpt([pg_carrier_gear_size, 0], i * 360 / pg_num_planets - r_rot);
        echo("planet", i, txpt);
        translate(txpt) {
            rz(p_rot - r_rot)
            planetgear_planet(h, twist);
        }
    }

    planetgear_ring(h, twist);
}

module preview_planetgear(rotate=preview_pg_rot) {
    *rz(.5)
    tz(15)
    color("aqua", 0.95)
    render(10)
    pillwheel_gear_r();

    tz(-2) planetgear_carrier();
}

module planetgear_carrier() {
    dia = pg_hole - .4;

    color("DeepSkyBlue")
    linear_extrude(1.6, convexity=10) {
        difference() {
            circle(r=pg_carrier_gear_size + dia/2);

            rz(-28)
            pillgear_spoke_cutout(pg_num_planets, spoke_r1=9, spoke_r2 = pg_carrier_gear_size - dia/2, holes=false);

            circle(d=13.4);
        }
    }

    color("aqua")
    tz(1.6)
    for (i = [0 : pg_num_planets - 1]) {
        rz(i * 360 / pg_num_planets) tx(pg_carrier_gear_size)
        cylx(0, 12, d=dia);
    }

}

module planetgear_shim() {
    difference() {
        tz(-1)
        linear_extrude(6, convexity=10) {
            difference() {
                union() {
                    pg_shim_arc(pg_shroud_inner, -45, 45, 6, 6);
                    pg_shim_arc(pg_shroud_inner - 1, -90, 55, 6, 0);
                }
                pg_shim_arc(wheel_rad + 4, -90, 45, 7, 7);
            }

        }
        pillwheel_holder_screws(undef, [0, 45, 90], pg_shroud_inner);
        pillwheel_holder_screws(undef, [-45], pg_shroud_inner - 1);
    }
}


module planetgear_beltwheel() {
    difference() {
        union() {
            tz(-5)
            beltwheel_base(beltwheel_rad, xh=1.3);

            tz(2) planetgear_interface(.2);

            cylx(0, 2, d=13);
        }
        tz(-5)
        countersink_m2(100, expand=1.1, inv=1, head_extra=0, nut_length=2.5);

        tz(-5-eps)
        linear_extrude(5 + eps2, convexity=10)
        pillgear_spoke_cutout(6, spoke_r2 = beltwheel_rad - 5, holes=false);
    }

}

module pg_spoke_transform(nspokes=pg_num_planets) {
    rz(90 + 10)
    for (i = [0 : nspokes - 1]) rz(i * 360 / nspokes)
    tx(pg_carrier_gear_size) children();
}

module pillwheel_gear_spacer_o(ext=0) {
    z2 = bearing_top_z - bearing_thick;
    z0 = bearing_top_z - bearing_thick - beltwheel_bearing_spacing + 1.4;
    z1 = z0 + .4;

    difference() {
        union() {
            cylx(z0, z2, d=bearing_inner + 2);

            *tz(z0) rx(180) {
                pillwheel_gear_interface(.2, cut=true);
            }
        }
        *tz(z1)
        hexnut_hole(h=z2 - z1, d=bearing_inner*.6 + .4);

        *tz(-10 - ext)
        countersink_m2(100, expand=1.1, inv=1, head_extra=ext, nut_length=2.5);
    }

    *tz(z1 - .2)
    bridge_support();
}

module pillwheel_gear_spacer(ext=0) {
    z2 = bearing_top_z - bearing_thick + 1;
    z0 = bearing_top_z - bearing_thick - beltwheel_bearing_spacing;

    tz(z0)
    linear_extrude(z2 - z0)
    difference() {
        circle(d=bearing_inner + 2);
        circle(d=bearing_inner - 2 + .3);
        *cylx(z0, z2, d=bearing_inner + 2);

        *tz(z0 - eps)
        hexnut_hole(h=z2 - z0 + eps2, d=bearing_inner*.6 + .3);

    }
}

module pillwheel_gear_outer(mirror=false) {
    tz(-pillwheel_gear_thick) {
        render(5) {
            difference() {

                rfz(pillwheel_gear_thick/2)
                linear_extrude(pillwheel_gear_thick/2, twist=(mirror ? -1 : 1) * (pg_flat ? 0 : 3.6), slices=2, convexity=10) {
                    polygon(gearpts(pillwheel_gear_rad, pillwheel_gear_tooth_size, pillwheel_gear_teeth));
                }

                if (planetgear_hgr)
                intersection() {
                    linear_extrude(pillwheel_gear_thick - (preview_cutout ? 0 : 1), convexity=10) {
                        difference() {
                            polygon(gearpts(pg2_ring_gear_size + pg_fit_clearance, pg_gear_size * pg_scale, pg2_ring_gear_teeth, pg_outer_size + 10));
                            circle(r=pg_sun_gear_size - 2);
                        }
                    }
                    let(br = pg2_ring_gear_size + pg_gear_size - .8, zs=2)
                    tz(pillwheel_gear_thick - 1) {
                        cylx(-zs, 0, r1=br + zs, r2 = br);
                        cylx(-pillwheel_gear_thick, -zs, r = br + zs);

                    }
                }
            }
        }
    }


    if (pillgear_old_interface)
    cylx(0, beltwheel_bearing_spacing + .2, d=bearing_inner + 2);
}

function nspokes(n, spc, ofs, ofs5=0) = (n == 0) ? [] : [for (i = [0 : n-1]) spc * i + ofs + ofs5 * floor(i/5)];

module pillwheel_gear_inner(ext=0) {
    tz(-5.2)
    countersink_m2(100, expand=1.1, inv=1, head_extra=ext, nut_length=2.5);

    angspc  = planetgear_hgr ? 15   : 4;
    mainofs = planetgear_hgr ? 145  : -360/5;
    ys1     = planetgear_hgr ? 3.6  : 1.6;
    ys2     = planetgear_hgr ? 1.6  : 1.6;
    xs      = planetgear_hgr ? 1.4  : 0.8;
    y1      = planetgear_hgr ? pg_sun_gear_size + 4 : spoke_r2 - .5;

    linear_extrude(20 + eps, center=true, convexity=10) {
        rz(10)
        for (ang = nspokes(floor(pill_width), -angspc, 0, -angspc*.75)) rz(ang) {
            squarex(y1 = y1, ys=ys1, xc=0, xs=xs);
        }

        for (ang = nspokes(floor(pill_width * 10) % 10, -angspc, mainofs, -angspc*.75)) rz(ang) {
            squarex(y1 = y1, ys=ys2, xc=0, xs=xs);
        }
    }

    if (pillgear_old_interface) {
        hexnut_hole(h=beltwheel_bearing_spacing + .2 + eps2, d=bearing_inner*.6 + .3);
    } else {
        rx(180)
        tz(-.2)
        planetgear_interface(0, true);
    }


}

module pillgear_spoke_cutout(nspokes = pg_num_planets, spoke_r1=spoke_r1, spoke_r2=spoke_r2, holes=true) {
    spoke_cofs = 0;
    spoke_len = spoke_r2 - spoke_r1;
    chord_len = spoke_len * 2;

    a = 20;
    c = chord_len/2;
    b = (c*c/a - a) / 2;
    r = a + b;

    difference() {
        circle(r=spoke_r2);
        circle(r=spoke_r1);
        for (ang = [0 : nspokes - 1]) rz(ang * (360/nspokes))
        intersection() {
            ty(spoke_r1)
            tx(-a)
            difference() {
                tx(-b) circle(r=r + spoke_w/2);
                tx(-b) circle(r=r - spoke_w/2);
            }
            squarex(y1=spoke_r1 - 1, y2 = spoke_r2 + 2, x1=-a-spoke_w - 10, x2=a+spoke_w + 10);
        }

        if (holes)
        pg_spoke_transform(nspokes) circle(d=pg_hole + 5);
    }

    if (holes)
    pg_spoke_transform(nspokes) circle(d=pg_hole + .75);
}

module pillwheel_gear(mirror=false) {
    //pillgear_spoke_cutout();
    difference() {
        pillwheel_gear_outer(mirror);
        pillwheel_gear_inner();

        if (!planetgear_hgr)
        tz(-5 - eps)
        linear_extrude(20, convexity=10) pillgear_spoke_cutout();
    }

    *if (planetgear_hgr)
    rx(180)
    bridge_support(h=0.32);
}

module pillwheel_gear_r() {
    pillwheel_gear(false);
}

module pillwheel_gear_l() {
    pillwheel_gear(true);
}

module pillwheel_gear_l_belt() {
    difference() {
        union() {
            pillwheel_gear_outer();

            *cylx(-8, -5, r=pillwheel_gear_rad + .8);

            tz(-8)
            linear_extrude(3, convexity=10) {
                polygon(gearpts(pillwheel_gear_rad, pillwheel_gear_tooth_size, pillwheel_gear_teeth));
            }

            if (!preview_cutout)
            tz(-8 - 5)
            beltwheel_base(beltwheel_rad, xh=1.3);
        }
        pillwheel_gear_inner(8);
        tz(-5 - 8 - eps)
        linear_extrude(20, convexity=10) pillgear_spoke_cutout();
    }
}

module preview_pillwheel_gear_r() {
    if (preview_pillgear) {
        tx(wheel_tx*2)
        rz(360/pillwheel_gear_teeth/2)
        color("blue", 0.5)
        render(10)
        pillwheel_gear_l();
    }
}
