module wirebox_base(hh, x1, y1, x2, y2, bt=4, wt=4, outer_rad=4, inner_rad=3, screw_pos=[-12, -2], taper_ofs=4, hole_rot=0, ti_height=insert_length_m2_5) {
    difference() {
        union() {
            difference() {
                cubex(
                    x1=x1-wt, x2=x2+wt, y1=y1-wt, y2=y2+wt,
                    z1=-bt, z2=hh,
                    r=outer_rad
                );
                cubex(
                    x1=x1, x2=x2, y1=y1, y2=y2,
                    z1=0, z2=hh+eps
                );
            }

            // platforms for mounting holes
            corners2(x1=x1, x2=x2, y1=y1, y2=y2) {
                intersection() {
                    if (threaded_inserts) {
                        cubex(x1=-7, y1=-7, x2=0, y2=0, z1=-eps, z2=hh, r1=inner_rad);
                        cubex(x1=-15.8, y1=-5.5, x2=0, y2=0, z1=-eps, z2=hh, r1=inner_rad);

                    } else {
                        cubex(x1=-18, y1=-7, x2=0, y2=0, z1=-eps, z2=hh, r1=inner_rad);
                    }

                    yzx() tz(-18) lxpoly(18, points=[
                        [-7, hh],
                        [0, hh],
                        [0, hh - taper_ofs - 7],
                        [-7, hh - taper_ofs],

                    ]);

                    *ty(-3)
                    tz(hh-8)
                    rx(-45)
                    cubex(x1=-20, y1=-10, x2=10, y2=30, z1=-50, z2=0);
                }
                cubex(x1=-7, y1=-7, x2=wt, y2=wt, z1=hh, z2=hh+4, r1=inner_rad, r4=outer_rad);
            }
        }

        //m2.5 screw holes
        corners2(x1=x1, x2=x2, y1=y1, y2=y2) {
            translate(screw_pos) {
                if (threaded_inserts) {
                    cylx(z1=0, z2=hh+eps, d=screw_diameter_m2);
                    cylx(z1=hh - ti_height, z2=hh+eps, d=insert_dia_m2_5);
                } else {
                    cylx(z1=0, z2=hh+eps, d=2.3);
                }
            }
        }
    }
}

module wirebox_lid_base(x1, y1, x2, y2, lt=4, wt=4, inner_rad=3) {
    difference() {
        cubex(x1=x1, x2=x2,
            y1=y1, y2=y2,
        z1=0, z2=lt);

        corners2(x1=x1+wt, y1=y1+wt, x2=x2-wt, y2=y2-wt) {
            cubex(x1=-7.2, y1=-7.2, x2=wt+eps, y2=wt+eps, z1=-eps, z2=lt+eps, r1=inner_rad);
            tx(-12)
            ty(-2)
            tz(-6+eps)
            countersink_m2(10, nut_length=2.5);
        }
    }

}
