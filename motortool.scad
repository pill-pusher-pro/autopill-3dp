include <common.scad>

$fn = 80;

module lh() {
    hull() {
        children();
        tx(5) children();
    }
}

module beltwheel_base(r, h=5, xh=0, s=5/4, ll=0) {
    r2 = r + (h - xh) / 2 / s;
    tz(h/2) rfz() {
        linehull([[0, 0], [ll, 0]])
        cylx(z1=-eps, z2=xh/2+eps, r=r);
        linehull([[0, 0], [ll, 0]])
        cylx(z1=xh/2-eps, z2=h/2, r1=r, r2=r2);
    }
}

sz(-1)
difference() {
    ll = 16;
    cubex(x1=0, x2=32, yc=0, ys=12, z1=0, z2=10);

    linehull([[0, 0], [ll, 0]]) cylx(5-eps2, 9.5, r=3.4);
    tz(-eps)
    beltwheel_base(1.4, xh=1, s=1, ll=ll);
}

fc = 0.35;

*ry(90)
difference() {
    ll = 16;
    cubex(x1=0, x2=10, yc=0, ys=10, z1=-4, z2=5);

    rfz() tz(1.2/2){
        lh() cylx(-eps, 2.5+fc, r=1.8+fc);
        lh() cylx(-eps, 1.2+fc, r=3+fc);
    }
    lh() cylx(-5-eps, 5+eps, r=1.2);
}
