module pincher(fc1=0, fc2=0, fch=0) {
    rz1 = pincher_z - (pincher_ring_z1 - fc1);
    rz2 = pincher_z - (pincher_ring_z2 + fc2);
    ring_rad1 = hypot(pincher_x - pill_width_fc/2, rz1);
    ring_rad2 = hypot(pincher_x - pill_width_fc/2, rz2);

    thick = pincher_w + 2*fch;
    z2 = 2.5;
    tx(pincher_x) {
        difference() {
            union() {
                tz(pincher_z)
                xzy() {
                    cylinder(h=thick, center=true, d=6.2);
                    intersection() {
                        difference() {
                            cylinder(h=thick, center=true, r=ring_rad1, $fn=100);
                            if (rz2 > 0)
                            cylinder(h=thick+eps2, center=true, r=ring_rad2, $fn=100);

                        }
                        for (ang = [-5, 0])
                        rz(ang)
                        cubex(x1=-pincher_x + pill_width/2, x2=0, zc=0, zs=thick, y1=-pincher_z, y2=0);

                    }
                }
                cubex(xc=0, xs=pincher_lever_thick, yc=0, ys=thick, z1=z2, z2=pincher_z);
                tz(z2) xzy() cylinder(h=thick, center=true, d=4);

            }

            tz(pincher_z) xzy()
            cylinder(h=thick+eps2, center=true, d=screw_diameter_m2);
        }

    }
}

module dispenser_tube() {
    difference() {
        union() {
            cubex(xc=0, yc=0, xs=tower_w, ys=tower_l, z1=0, z2=tower_h);
            cubex(xc=0, yc=0, xs=tower_w + 2*tower_wall_thick.x, ys=slider_width, z1=0, z2=tower_h);
        }
        if (pill_width == pill_thick && !pincher) {
            dia_diff = 6;
            cylx(-eps, tower_h+eps, d=pill_width_fc);
            intersection() {
                cylx(tower_h - dia_diff, tower_h + eps, d1=pill_width_fc, d2=pill_width_fc + dia_diff);
                cubex(z1=-eps, z2=tower_h+eps, xc=0, yc=0, xs=pill_width_fc, ys=pill_thick_fc);
            }
        } else {
            cubex(z1=-eps, z2=tower_h+eps, xc=0, yc=0, xs=pill_width_fc, ys=pill_thick_fc);
        }

        if (!pincher) {
            cubex(xc=0, xs=tower_w + 2*tower_wall_thick.x,
                yc=0, ys=slider_width,
            z1=pill_len, zs=2);
        }

        if (pincher)
        pincher(.6, .6, .2);

        // photodiode/led holes
        tz(photodiode_height) {
            if (pincher) {
                xzy() rz(45) cubex(zc=0, zs=tower_l+eps, xs=2, ys=2, xc=0, yc=0);
            } else {
                xzy() intersection() {
                    rz(45) cubex(zc=0, zs=tower_l+eps, xs=4, ys=4, xc=0, yc=0);
                    cubex(zc=0, zs=tower_l+eps, xs=3, ys=10, xc=0, yc=0);
                }
            }
        }
    }
    if (pincher) difference() {
        y1 = pincher_w/2 + .2;
        y2 = y1 + 4;
        z1 = pincher_ring_z2 - pincher_ring_z1;
        tx(tower_w/2 + tower_wall_thick.x)
        rfy() {
            cubex(x1=0, xs=7,
            y1=y1, y2=y2, z1=tower_h - 8, z2=tower_h);
            tz(pincher_ring_z1)
            ty(y1) xzy() linear_extrude(y2-y1)
            polygon([[0, 0], [7, 0], [7, z1], [0, z1]]);
        }
        tz(tower_h-4)
        tx(pincher_x)
        xzy() tz(-y2) countersink_m2(2*y2, expand=1, nut_length=2.5);
    }

}

module dispenser_tube_text() {
    linear_extrude(.4 + eps, convexity=10) {
        ty(tower_l/2 - 2)
        marker_text(dia_text, 4, halign="left", valign="top");
        ty(-tower_l/2 + 2)
        marker_text(str("L", pill_len), 4, halign="left", valign="bottom");
    }
}

module dispenser_tube_lower() {
    if (pincher) {
        difference() {
            intersection() {
                dispenser_tube();
                cubex(x1=-tower_w/2, x2=pill_width_fc/2-eps, yc=0, ys=50, z1=0, z2=tower_h);
            }

            tz(tower_h - 10)
            tx(-tower_w/2)
            ry(90)
            sy(-1)
            dispenser_tube_text();
        }
    } else {
        intersection() {
            dispenser_tube();
            cubex(xc=0, yc=0, xs=50, ys=50, z1=0, z2=pill_len + 2 - eps);
        }
    }
}

module dispenser_tube_upper() {
    if (pincher) {
        difference() {
            intersection() {
                dispenser_tube();
                cubex(x1=pill_width_fc/2, yc=0, xs=50, ys=50, z1=0, z2=tower_h);
            }

            tz(tower_h - 10)
            tx(tower_w/2)
            ry(90)
            sz(-1)
            dispenser_tube_text();
        }

    } else {
        intersection() {
            dispenser_tube();
            cubex(xc=0, yc=0, xs=50, ys=50, z1=pill_len + 2, z2=tower_h);
        }
    }

}

module dispenser_slider_upper() {
    thick = .4;
    cover_end = 5 + tower_wall_thick.x + 2*dispenser_pill_hole + 2;
    difference() {
        union() {
            cubex(x1=-5, x2=5,
                yc=0, ys=4,
            z1=0, zs=7);

            if (pill_len + thick > 7)
            cubex(xc=0, xs=10,
                yc=0, ys=12,
            z1=7, z2=pill_len+thick, r1=2, r3=2);

            cubex(x1=5, xs=1,
                yc=0, ys=12,
            z1=0, z2=pill_len+thick);

            cubex(x1=-5, x2=cover_end,
                yc=0, ys=slider_width - .6,
                z1=pill_len, z2=pill_len+thick,
            r1=2, r3=2);
        }

        // cut off anything above pill_len+1
        cubex(x1=-5-eps, x2=cover_end+eps,
            yc=0, ys=slider_width,
        z1=pill_len+thick+eps, zs=10);

        // cut off anything that intersects with the mount tower
        rfy()
        cubex(x1=-5-eps, x2=5+eps,
            y1=2+eps, y2=slider_width/2 + 1,
        z1=0, z2=7+eps);


        tz(3) xzy() cylinder(4+eps, center=true, d=screw_diameter_m2);
        tx(cover_end)
        tz(pill_len)
        xzy() linear_extrude(slider_width+eps, center=true)
        polygon([
            [eps, eps],
            [eps, 1+eps],
            [-1.2, 1+eps]
        ]);
    }
}
